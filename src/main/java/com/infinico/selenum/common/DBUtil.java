package com.infinico.selenum.common;

import java.sql.Connection; 
import java.sql.Statement;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.sql.ResultSet; 
import java.sql.DriverManager; 
import java.sql.SQLException;

public class DBUtil {
	
	


	public static String nams;

	public static void main(String[] args) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
		//Right click on mysql workbench -> Select "Copy JDBC connection string...add database name"
		String url="jdbc:mysql://localhost:3306/testsql";
		String uname = "root";
		String pass = "9618696711";
		
		//Select column name and table name
		String query = "select PersonID from persons where PersonID = 4;";
		Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		Connection con = DriverManager.getConnection(url,uname,pass);
		Statement st = con.createStatement();
		ResultSet rs= st.executeQuery(query);
		while (rs.next()) {
		String nams = rs.getString("PersonID");
		System.out.print(nams);
		}
		st.close();
		con.close();
		
	}
	
	  //Get data from DATABASE
    public ArrayList<String> getDBdetails() throws Exception {
		ArrayList<String> users= new ArrayList<String>();
		//Right click on mysql workbench -> Select "Copy JDBC connection string...add database name"
		String url="jdbc:mysql://localhost:3306/testsql";
		String uname = "root";
		String pass = "9618696711";
		
		//Select column name and table name
		String query = "select PersonID,LastName from persons";
		Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		Connection con = DriverManager.getConnection(url,uname,pass);
		Statement st = con.createStatement();
		ResultSet rs= st.executeQuery(query);
		while (rs.next()) {
		String Personid = rs.getString("PersonID");
		String lastname = rs.getString("LastName");
		
		users.add(Personid);
		users.add(lastname);
		//System.out.print(Personid);
		}
		st.close();
		con.close();
		return users;
	}
	
    //DB CALLING METHOD IN ANOTHER CLASS
/*	 DBUtil base = new DBUtil();
		ArrayList<String> users=base.getDBdetails();
		for (String temp : users) {
			System.out.println(temp);
		}

	 System.out.println("DB " +users);
	 System.out.println("DB " +users.get(1));
	*/
	
	
	
}




/*
 package com.infinico.selenum.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection; 
import java.sql.Statement;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.sql.ResultSet; 
import java.sql.DriverManager; 
import java.sql.SQLException;

public class DBUtil {
	
	


	public static void main(String[] args) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
		//Right click on mysql workbench -> Select "Copy JDBC connection string...add database name"
		String url="jdbc:mysql://localhost:3306/testsql";
		String uname = "root";
		String pass = "9618696711";
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("database.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			System.out.println(prop.getProperty("url"));
			System.out.println(prop.getProperty("uname"));
			System.out.println(prop.getProperty("pass"));

			//Select column name and table name
			String query = "select PersonID from persons where PersonID = 4;";
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			String url=prop.getProperty("url");
			String uname=prop.getProperty("uname");
			String pass=prop.getProperty("pass");
			
			Connection con = DriverManager.getConnection(url,uname,pass);
			Statement st = con.createStatement();
			ResultSet rs= st.executeQuery(query);
			while (rs.next()) {
			String nams = rs.getString("PersonID");
			System.out.print(nams);
			}
			st.close();
			con.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		
		
		
		
		
		
		
	}
	
	
	
}

 
 
 */
