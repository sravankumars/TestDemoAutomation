package com.infinico.selenum.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class CommonUtil {

	public final static short TYPE_UPPER_ONLY = 1;
	public final static short TYPE_LOWER_ONLY = 2;
	public final static Random rnd = new Random();

	static final char[] alphas = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
			'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
			'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
			'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
			'w', 'x', 'y', 'z' };

	public static String getUniquenumber() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String time = sdf.format(cal.getTime()) + System.currentTimeMillis();
		return time;
	}

	public static String readID(String strQuery) throws Exception {
		Connection connect = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//connect = DBUtil.getConnection();
			statement = connect.createStatement();
			resultSet = statement.executeQuery(strQuery);
			resultSet.next();
			String s = resultSet.getString(1);
			return (s);

		}

		catch (Exception e) {
			throw e;

		} finally {

			try {
				if (resultSet != null) {
					resultSet.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connect != null) {
					connect.close();
				}
			} catch (Exception e) {

			}

		}

	}

	/**
	 * Generate a random string of characters at the specified length. length
	 * hard coded to 5
	 * 
	 * @return generated string of lower case
	 */
	public static String generateRandomString() {
		short type = 2;
		int min = type == TYPE_LOWER_ONLY ? 26 : 0;
		int max = type == TYPE_UPPER_ONLY ? 26 : alphas.length;
		String generated = "";
		for (int i = 0; i < 5; i++) {
			int random = rnd.nextInt(max - min) + min;
			generated += alphas[random];
		}
		return generated;
	}
	
    
	public static int randomNumber(int min, int max) {
		Random r = new Random();
		int result = r.nextInt(max - min) + min;
		return result;
	}

	public static String generate5DigitNumeric() {
		// long random =Math.round(Math.random() * 100000);
		String rendomString = Long.toString(Math.round(Math.random() * 100000));
		return rendomString;

	}

	public static String generate10DigitNumeric() {
		String rendomString = Long
				.toString(Math.round(Math.random() * 10000000000L));
		return rendomString;
	}

	/**
	 * 
	 * @param numberOfDays
	 * @return return the date with today's date plus numberOf days as we send
	 *         as a parameter.
	 */
	public static String getRequiredDate(int numberOfDays) {
		Calendar calendar = Calendar.getInstance();
		Date date;
		calendar.add(Calendar.DATE, numberOfDays);
		date = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		return dateFormat.format(date);

	}

	/**
	 * 
	 * @param numberOfDays
	 * @return return the date with today's date plus numberOf days as we send
	 *         as a parameter.
	 */
	public static String getRequiredDateWithMMDDYY(int numberOfDays) {
		Calendar calendar = Calendar.getInstance();
		Date date;
		calendar.add(Calendar.DATE, numberOfDays);
		date = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("MMddyyyy");

		return dateFormat.format(date);

	}

	
	public static String toTitleCase(String input) {
		StringBuilder titleCase = new StringBuilder();
		boolean nextTitleCase = true;

		for (char c : input.toCharArray()) {
			if (Character.isSpaceChar(c)) {
				nextTitleCase = true;
			} else if (nextTitleCase) {
				c = Character.toTitleCase(c);
				nextTitleCase = false;
			}

			titleCase.append(c);
		}

		return titleCase.toString();
	}


    /**
     * Random Numeric String generator
     *
     * @param length
     * @return
     * @author amit kumar
     */
 /*   public static String randomNumberGenerator(int length) {
        // Random number generator
        String randomNumber = RandomStringUtils.randomNumeric(length);

        // Return random number
        return randomNumber;
    }
    
    *//**
     * Random String generator
     *
     * @param length
     * @return
     *//*
    public static String randomStringGenerator(int length) {

        // Random string generator
        String randomNumber = RandomStringUtils.randomAlphabetic(length);

        // Return random number
        return randomNumber;
    }*/
    
    
	public static String getTimeStamp() {
		Timestamp t = new Timestamp(new java.util.Date().getTime());
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		String today = formatter.format(t);
		return today;

	}

	/* Specific Date Format for HomeAway */
	public static String getRequiredDateHA(int numberOfDays) {
		Calendar calendar = Calendar.getInstance();
		Date date;
		calendar.add(Calendar.DATE, numberOfDays);
		date = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		return dateFormat.format(date);

	}
	
	
	
	
}

