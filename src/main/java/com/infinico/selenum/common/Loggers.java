package com.infinico.selenum.common;

import java.util.logging.Logger;

public class Loggers
	{

		private static final Logger auditLogger = Logger.getLogger("com.vTest.auditLog");

		public static Logger getLogger(Class<?> clazz)
			{
				return Logger.getLogger(clazz.getPackage().getName());
			}

		public static Logger getAuditLogger()
			{
				return auditLogger;
			}
	}
