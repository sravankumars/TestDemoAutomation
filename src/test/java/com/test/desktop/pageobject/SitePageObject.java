package com.test.desktop.pageobject;



import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByCssSelector;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.infinico.selenum.common.CommonUtil;
import com.infinico.selenum.common.Loggers;
import com.test.desktop.common.BasePO;
import com.test.desktop.common.DriverHelper;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.TestException;

import java.util.ArrayList;
import java.util.List;

public class SitePageObject extends BasePO {
	
	 //Input data
	 public static final String emailId	 	= "infinico"+ CommonUtil.getUniquenumber() + "@test.com";
	 public static final String pswrd 		= "Maisa786";
	 public static final String name	 	= "S"+ CommonUtil.generateRandomString();
	
	
	
     //Login
	 public final static By USERNAME 		= By.id("email");
	 public final static By PASSWORD		= By.id("pass");	 
	 public final static By SIGNIN_BUTTON	= By.id("loginbutton");
	 public final static By verifyhome		= By.xpath("html/body/div[1]/div[2]/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div/a");
	 
	 
	 //SignUp
	 public final static By fname 			= By.name("firstname");
	 public final static By lname 			= By.name("lastname");
	 public final static By email	 		= By.name("reg_email__");
	 public final static By email1 			= By.name("reg_email_confirmation__");
	 public final static By password1 		= By.name("reg_passwd__");
	 public final static By verify 			= By.xpath("//*[@id=\"content\"]/div/div/div/div/div[2]/div[1]/div[1]/span");
	 
	 
	 public final static By upload 		= By.className("home-buttons");
	 
	 
/*	 public void createSheet() throws IOException{
		 XSSFWorkbook workbook = new XSSFWorkbook(); 
		 XSSFSheet spreadsheet = workbook.createSheet("WaveLabs");
		  
		 
		  Row row = spreadsheet.createRow(1);
		  
		  Cell cellname= row.createCell(0);
		  cellname.setCellValue("value");
		//System.out.println("Row_column0 "+s);
		  
		  Cell cellphone= row.createCell(1);
		  cellphone.setCellValue("value");
		  
		  
		  FileOutputStream out = new FileOutputStream(new File("D:\\Sravan\\Automation\\Selenium\\Infi\\InfiniGo\\Excel_Sheet\\Test.xlsx"));
			 workbook.write(out);
		 }*/
		
		 
		 
		 
		 
	 
	 
	//int cnum = getRandomIndex(1, 4);
	@FindBy(id = "firstname")
	public WebElement sss;

	@FindBy(id = "firstName")
	private WebElement BUCchildFirstName;

	@FindBy(id = "lastName")
	private WebElement BUCchildLasttName;

	
	
	/*
	
	@FindBy(id = "dob")
	private WebElement BUCchildDOB;

	@FindBy(xpath = "//label[input[@id='child_boy' and @value='M' ]]")
	private WebElement selectBoy;

	@FindBy(xpath = "//label[input[@name='inHomeAdditionalInfo.inHomeChildDriving' and @value='false' ]]")
	private WebElement ChildTransportation;

	@FindBy(xpath = "//label[input[@name='inHomeAdditionalInfo.provideCar' and @value='false' ]]")
	private WebElement isCarAvailable;

	@FindBy(xpath = "//label[input[@name='inHomeAdditionalInfo.isParkingAvailable' and @value='false' ]]")
	private WebElement isParkingAvailable;

	@FindBy(xpath = "//label[input[@name='inHomeAdditionalInfo.isPublicTransportationAvailable' and @value='false' ]]")
	private WebElement isPublicTransportationAvailable;

	@FindBy(xpath = "//label[input[@name='inHomeAdditionalInfo.comfortableWithPets' and @value='false' ]]")
	private WebElement comfortableWithPets;

	@FindBy(id = "lastName")
	private WebElement lastName;

	@FindBy(id = "addressLine1")
	private WebElement addressLine1org;

	@FindBy(xpath = "//label[input[@value='incenter' and @name='carecenter' ]]")
	private WebElement clickOnIncenter;

	@FindBy(xpath = "	//label[input[@value='inhome' and @name='carecenter' ]]")
	private WebElement clickOnInHome;

	@FindBy(name = "addressLine1")
	private WebElement addressLine1;

	@FindBy(id = "cityStateZIP")
	private WebElement cityStateZIP;

	@FindBy(id = "emailId")
	private WebElement email;

	@FindBy(name = "password")
	private WebElement password;

	@FindBy(id = "submitButton")
	private WebElement submitButton;

	@FindBy(xpath = ".//*[@id='childrenAgesInfoForm']/div/div[1]/div[2]/label[2]")
	private WebElement twoKids;

	@FindBy(xpath = ".//*[@id='childrenAgesInfoForm']/div/div[1]/div[2]/label[3]")
	private WebElement threeKids;

	@FindBy(xpath = ".//*[@id='childrenAgesInfoForm']/div/div[3]/div[2]/label")
	private WebElement howOld;

	@FindBy(className = "karooContinueBtn")
	private WebElement karooContinueBtn;

	@FindBy(className = "submit")
	private WebElement submit;

	@FindBy(id = "myKids")
	private WebElement childCareMainMenu;

	@FindBy(xpath = "//button[@type='button' and @data-service-id='CHILDCARE']")
	private WebElement childCareButton;

	@FindBy(xpath = "//button[@type='button' and @data-sub-service='nanny']")
	private WebElement childCareSubService;

	@FindBy(xpath = "//label[input[@name='jobDays' and @value='TUE']]")
	private WebElement selectTuesday;

	@FindBy(xpath = "//label[input[@name='jobDays' and @value='WED']]")
	private WebElement selectWednesday;

	@FindBy(xpath = "//label[input[@name='jobDays' and @value='FRI']]")
	private WebElement selectFriday;

	@FindBy(xpath = ".//*[@id='pajSpringForm']/div[1]/section[6]/div[2]/div[1]/div/div[3]/button")
	private WebElement zeroToSixMonths;

	@FindBy(xpath = "//label[input[@id='lightHousekeeping1']]")
	private WebElement clickOnLightHousekeeping;

	@FindBy(xpath = "//label[input[@id='mealPreparation1']]")
	private WebElement clickOnMealPreparation;

	@FindBy(xpath = "//input[@id='pajSubmit' and @value='Continue'] ")
	private WebElement clickOnContinue;
	
	@FindBy(id= "membership-options-continue")
	private WebElement ContinueButton;
	
	public void ContinueButton() {
		ContinueButton.click();
	}
	
	
	
	@FindBy(xpath = "//input[@id='pajSubmit' and @value='Upgrade now']")
	private WebElement upgradeCTA;
	
	public void upgradeCta() {
		upgradeCTA.click();
	}

	@FindBy(id = "CHILDCARE")
	private WebElement childCare;

	@FindBy(id = "SENIRCARE")
	private WebElement seniorCare;

	@FindBy(id = "SPCLNEEDS")
	private WebElement specialNeeds;

	@FindBy(id = "HOUSEKEEP")
	private WebElement houseKeeping;

	@FindBy(id = "PETCAREXX")
	private WebElement petCare;

	@FindBy(id = "TUTORINGX")
	private WebElement tutoring;

	@FindBy(id = "DATENIGHT")
	private WebElement dateNight;

	// @FindBy(id = "childCareCenter")
	@FindBy(xpath = "//div[@class='optionsContentTopBlock optionsContent']/div/div[6]")
	private WebElement dayCareCenter;

	// under childcare
	@FindBy(id = "stepNodeAction-3-childCareCenter-seeker")
	private WebElement dayCareCenterseeker;

	@FindBy(name = "DOBMonths")
	private WebElement birthMonth;

	@FindBy(name = "DOBYears")
	private WebElement birthYear;

	@FindBy
	private WebElement daysNeeded;
	private String daysNeededPath = "//input[@type='checkbox' and @value='WORKGDAYS00";

	@FindBy(name = "childCareStartDate")
	private WebElement enrollmentMonth;

	@FindBy(name = "primaryPhone")
	private WebElement phone;

	@FindBy(name = "attr-leadGen.childCare.comment")
	private WebElement comments;

	@FindBy(id = "submitButton")
	private WebElement sendMyInfo;

	@FindBy(xpath = ".//*[@id='RshellContainer']/div/div/div[2]/div/div/div/div[5]/a/input")
	private WebElement continuetoCare;

	@FindBy(id = "withindays")
	private WebElement withindays;

	@FindBy(id = "recurring")
	private WebElement recurring;

	@FindBy(xpath = "//input[@value='MON']")
	private WebElement monDay;

	@FindBy(xpath = "//input[@value='TUE']")
	private WebElement tuesDay;

	@FindBy(xpath = "//input[@value='WED']")
	private WebElement wednesDay;

	@FindBy(xpath = "//input[@value='THU']")
	private WebElement thursDay;

	@FindBy(xpath = "//input[@value='FRI']")
	private WebElement friDay;

	@FindBy(xpath = "//input[@value='SAT']")
	private WebElement satDay;

	@FindBy(xpath = "//input[@value='SUN']")
	private WebElement sunDay;

	@FindBy(name = "hourlyRateFrom")
	private WebElement hourlyRateFrom;

	@FindBy(name = "hourlyRateTo")
	private WebElement hourlyRateTo;

	@FindBy(name = "startingJobTime")
	private WebElement startingJobTime;

	@FindBy(name = "endingJobTime")
	private WebElement endingJobTime;

	@FindBy(name = "postJobUntil")
	private WebElement enterDate3;

	@FindBy(name = "jobZip")
	private WebElement jobZip;

	@FindBy(name = "attr-job.childCare.numberOfChildren")
	private WebElement numberOfChildren;

	@FindBy(xpath = "//button[@id='sekNextBtnImg']")
	private WebElement upgradeButton;

	@FindBy(xpath = "//input[@id='pajSubmit' and @value='Upgrade now']")
	private WebElement PatternLibUpgradeNow;

	@FindBy(linkText = "No thanks")
	private WebElement addOnFeature;

	// Ages of Children
	@FindBy(xpath = "//input[@type='checkbox' and @value='CHLDAGEGP001']")
	private WebElement upto6Months;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CHLDAGEGP002']")
	private WebElement months7To3Years;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CHLDAGEGP003']")
	private WebElement years4To6;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CHLDAGEGP004']")
	private WebElement years7To11;

	// Type of care required
	@FindBy(xpath = "//input[@type='radio' and @value='CHLDCGTYP002']")
	private WebElement babysitter;

	@FindBy(xpath = "//input[@type='radio' and @value='CHLDCGTYP001']")
	private WebElement nanny;

	@FindBy(xpath = "//input[@type='radio' and @value='CHLDCGTYP003']")
	private WebElement mothersHelper;

	@FindBy(xpath = "//input[@type='radio' and @value='CHLDCGTYP004']")
	private WebElement liveInCaregiver;

	@FindBy(xpath = "//input[@type='radio' and @value='TRNVHDT001']")
	private WebElement carProvided;

	@FindBy(xpath = "//input[@type='radio' and @value='TRNVHDT002']")
	private WebElement carOwn;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CCKIDLVDG001']")
	private WebElement sports;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CCKIDLVDG004']")
	private WebElement games;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CCKIDLVDG002']")
	private WebElement music;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CCKIDLVDG003']")
	private WebElement artsAndCrafts;

	// Gender
	@FindBy(xpath = "//input[@type='radio' and @value='F']")
	private WebElement female;

	@FindBy(xpath = "//input[@type='radio' and @value='M']")
	private WebElement male;

	@FindBy(name = "monthOfBirth")
	private WebElement monthOfBirth;

	@FindBy(name = "dayOfBirth")
	private WebElement dayOfBirth;

	@FindBy(name = "yearOfBirth")
	private WebElement yearOfBirth;

	@FindBy(name = "attr-job.childCare.caregiverType")
	private WebElement typeOFCareLookingFor;

	*//**
	 * Month-Day-Year drop down
	 *//*
	// Month drop down
	@FindBy(xpath = "//div[@class='ask select month-box']//span[contains(@class,'arrow')]")
	private WebElement monthDropdownList;

	// to select day
	@FindBy(xpath = "//div[@class='ask select day-box']//span[contains(@class,'arrow')]")
	private WebElement dateDropDownList;

	// to select year
	@FindBy(xpath = "//div[@class='ask select year-box']//span[contains(@class,'arrow')]")
	private WebElement yearDropDownList;

	@FindBy(xpath = "//div[@class='ask select month-box']")
	private WebElement month;

	@FindBy(xpath = "//div[@class='ask select day-box']")
	private WebElement day;

	@FindBy(xpath = "//div[@class='ask select year-box']")
	private WebElement year;

	// My WorkTime
	@FindBy(xpath = "//input[@type='radio' and @name='workTime' and @value='FULL_TIME']")
	private WebElement myWorkTime_fullTime;

	// Partner workTime
	@FindBy(xpath = "//input[@type='radio' and @name='partnerWorkTime' and @value='FULL_TIME']")
	private WebElement partnerJobTime_FullTime;

	// @FindBy(xpath = "//input[@type='checkbox' and @name='specialNeeds']")
	@FindBy(xpath = "//input[@name='specialNeeds']")
	private WebElement specialNeedsCheckBox;

	// @FindBy(xpath = "//input[@type='checkbox' and @name='pets']")
	@FindBy(xpath = "//input[@name='pets']")
	private WebElement petsCheckBox;

	@FindBy(xpath = "//input[@name='ageingParents']")
	private WebElement ageingParents;

	@FindBy(xpath = "//input[@name='seniorServedMilitary']")
	private WebElement seniorServedMilitary;

	@FindBy(xpath = "//input[@name='children']")
	private WebElement children;

	@FindBy(xpath = "//input[@name='militaryFamily']")
	private WebElement militaryFamily;

	@FindBy(id = "threeMonth")
	private WebElement subscriptionPlan_3months;

	@FindBy(xpath = "//div[@class='loud']/div[2]/div/input")
	private WebElement threeMonthsPlan;

	// 3 months subscription plan
	// @FindBy(xpath =
	// "//div[@class='membershipPlan.pricingPlan_JUN121002']/div/span")
	@FindBy(xpath = "//input[@name='membershipPlan.pricingPlan'])[3]")
	private WebElement pricingPlanFor3Months;

	// 3 months subscription
	@FindBy(xpath = "//input[@value='NOV121002']")
	private WebElement three_Months_Plan;

	@FindBy(name = "firstNameOnCard")
	private WebElement firstNameOnCard;

	@FindBy(name = "lastNameOnCard")
	private WebElement lastNameOnCard;

	@FindBy(name = "cardNumber")
	private WebElement cardNumber;

	@FindBy(name = "cardType")
	private WebElement typeOfCreditCard;

	private WebElement creditCardType;
	private String cardPath = "//input[@name='cardType'][contains(@value,'";
	// browser.getElementByName("cardNumber").sendKeys(CreateCreditCard.getCreditCard(CreditCardType.VISA));
	@FindBy(name = "cvv")
	private WebElement cvv;
	// browser.getElementByName("cvv").sendKeys("123");

	@FindBy(xpath = "//div[span[text()='Expiration Month']]")
	private WebElement expirationMonth;

	@FindBy(xpath = "//div[span[text()='Expiration Year']]")
	private WebElement expirationYear;

	@FindBy(name = "billingZIP")
	private WebElement billingZIP;

	@FindBy(name = "careConciergePhone")
	private WebElement careConciergePhone;

	@FindBy(xpath = "//input[@type='checkbox' and @name='optinStateChecked']")
	private WebElement optinStateChecked;

	@FindBy(name = "&lid=Log Out&lpos=l0")
	private WebElement logOut;

	@FindBy(xpath = "//a[@name='&lid=Not you?&lpos=l0']")
	private WebElement notyou;

	@FindBy(partialLinkText = "Skip this step")
	private WebElement skipThisStep;

	@FindBy(partialLinkText = "skip this step")
	private WebElement skipLink;

	// SeniorCare Page
	// Services Needed
	@FindBy(xpath = "//input[@type='checkbox' and @value='SCSERCAT002']")
	private WebElement assistedLiving;

	@FindBy(xpath = "//input[@type='checkbox' and @value='SCSERCAT007']")
	private WebElement adultDayHealth;

	@FindBy(xpath = "//input[@type='checkbox' and @value='SCSERCAT003']")
	private WebElement transportationServices;

	@FindBy(xpath = "//input[@type='checkbox' and @value='SCSERCAT008']")
	private WebElement independentLiving;

	@FindBy(xpath = "//input[@type='checkbox' and @value='SCSERCAT004']")
	private WebElement homeCare;

	@FindBy(xpath = "//input[@type='checkbox' and @value='SCSERCAT010']")
	private WebElement generalSeniorCareServices;

	// Care Concerns
	@FindBy(xpath = "//input[@type='checkbox' and @value='CARECONCE001']")
	private WebElement dementia;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CARECONCE003']")
	private WebElement bathing;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CARECONCE004']")
	private WebElement dressing;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CARECONCE005']")
	private WebElement mobility;

	// Caregig
	@FindBy(xpath = "//input[@type='checkbox' and @value='CGSERVCES001']")
	private WebElement errand;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CGSERVCES002']")
	private WebElement shopping;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CGSERVCES003']")
	private WebElement vacation;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CGSERVCES004']")
	private WebElement parties;

	@FindBy(xpath = "//input[@type='checkbox' and @value='CGSERVCES005']")
	private WebElement moving;

	// Transportation
	@FindBy(xpath = "//input[@type='checkbox' and @value='MON|ALL']")
	private WebElement monAll;

	@FindBy(xpath = "//input[@type='checkbox' and @value='TUE|ALL']")
	private WebElement tueAll;

	@FindBy(xpath = "//input[@type='checkbox' and @value='WED|ALL']")
	private WebElement wedAll;

	@FindBy(xpath = "//input[@type='checkbox' and @value='THU|ALL']")
	private WebElement thuAll;

	@FindBy(xpath = "//input[@type='checkbox' and @value='FRI|ALL']")
	private WebElement friAll;

	// Job Time Individual
	@FindBy(xpath = "//input[@type='checkbox' and @value='SAT|EM']")
	private WebElement satEm;

	@FindBy(xpath = "//input[@type='checkbox' and @value='SAT|EE']")
	private WebElement satEe;

	@FindBy(xpath = "//input[@type='checkbox' and @value='SUN|EM']")
	private WebElement sunEm;

	@FindBy(xpath = "//input[@type='checkbox' and @value='SUN|EM']")
	private WebElement sunEe;

	@FindBy(name = "primaryPhone")
	private WebElement primaryPhone;

	// while editing
	@FindBy(name = "primaryPhone")
	private WebElement editPrimaryPhone;

	@FindBy(name = "sitterService")
	private WebElement sitterService;

	@FindBy(name = "milesFromZipCode")
	private WebElement milesFromZipCode;

	@FindBy(name = "zipCode")
	private WebElement zipCode;

	@FindBy(xpath = "//span[@class='fav']//a[contains(.,'Favorites')]")
	private WebElement favorites;

	@FindBy(className = "siteLogo")
	private WebElement siteLogo;
	
	@FindBy(className = "navbar-logo-container")
	private WebElement logo;

	@FindBy(className = "myCareOnCall")
	private WebElement myCareOnCall;

	@FindBy(className = "myJobs")
	private WebElement myJobs;

	@FindBy(name = "serviceId")
	private WebElement serviceId;

	@FindBy(xpath = "//input[@type='checkbox' and @name='sendToFavorites']")
	private WebElement sendToFavorites;

	@FindBy(name = "title")
	private WebElement title;

	@FindBy(xpath = "//textarea[@name='description']")
	private WebElement description;

	@FindBy(id = "enterDate0")
	private WebElement enterDate0;

	@FindBy(id = "enterDate1")
	private WebElement editJobDate1;

	@FindBy(id = "enterDate2")
	private WebElement editJobDate2;

	@FindBy(id = "enterDate4")
	private WebElement editJobDate4;

	@FindBy(xpath = "//input[@name='recurringJobStartDate']|//input[@name='oneTimeJobEndDate']")
	private WebElement enterDate1;

	@FindBy(xpath = "//input[@name='recurringJobStartDate' and @id='enterDate1']")
	private WebElement renterDate1;

	@FindBy(xpath = "//input[@name='recurringJobEndDate' and @id='enterDate2']")
	private WebElement renterDate2;

	@FindBy(name = "recurringJobEndDate")
	private WebElement enterDate2;

	@FindBy(name = "oneTimeJobStartTime")
	private WebElement oneTimeJobStartTime;

	@FindBy(name = "oneTimeJobStartAM_PM")
	private WebElement oneTimeJobStartAM_PM;

	@FindBy(name = "oneTimeJobEndTime")
	private WebElement oneTimeJobEndTime;

	@FindBy(name = "oneTimeJobEndAM_PM")
	private WebElement oneTimeJobEndAM_PM;

	@FindBy(id = "loginLink")
	private WebElement login;

	@FindBy(xpath = "//input[@type='submit']")
	private WebElement loginButton;

	@FindBy(xpath = "//input[@class='btn btn-primary submitMessage']")
	private WebElement replyButton;

	@FindBy(xpath = "//input[@class='submitMessage btn btn-flat btn-primary']")
	private WebElement newReplyButton;

	@FindBy(xpath = "//*[@class='btn btn-primary']")
	private WebElement finishButton;

	// @FindBy(xpath = "//input[@type='image']")
	@FindBy(xpath = "//*[@id='careOnCallForm']/input[@type='image']")
	private WebElement sendMessage;

	@FindBy(partialLinkText = "Edit")
	private WebElement editJob;

	@FindBy(partialLinkText = "View")
	private WebElement viewJob;
	
	@FindBy(className = "ca-button POSITIVE")
	private WebElement caCard;
	
	public void CAcard(){
		caCard.click();
	}

	@FindBy(id = "postAJobTitle")
	private WebElement postAJobTitle;

	@FindBy(xpath = "//textarea[@id='postAJobDescription']")
	private WebElement postJobDescription;

	@FindBy(id = "educationLevel")
	private WebElement educationLevel;

	@FindBy(xpath = "//textarea[@id='familyDescription']")
	private WebElement familyDescription;

	@FindBy(xpath = "//input[@name='attr-seeker.comfortableWithPets' and @value='true']")
	private WebElement comfortWithPets;

	@FindBy(xpath = "//input[@name='attr-seeker.typesOfPets' and @value='PCANIMALS008']")
	private WebElement farmAnimals;

	@FindBy(xpath = "//input[@name='attr-seeker.typesOfPets' and @value='PCANIMALS006']")
	private WebElement smallAnimals;

	@FindBy(xpath = "//form[@name='seekerCareNeedsForm']/div[5]/input")
	private WebElement submitCareNeeds;

	@FindBy(xpath = "//input[@type='checkbox' and @name='publicProfile']")
	private WebElement publicProfile;

	@FindBy(xpath = "//form[@name='preferencesForm']/table[3]/tbody/tr[2]/td/input")
	private WebElement submitPreferenceForm;

	@FindBy(xpath = "//a[contains(text(),'Post a job')]")
	private WebElement getStarted;

	@FindBy(xpath = "//*[@id='searchBarForm']/button")
	private WebElement goButton;

	private WebElement checkbox;
	private String checkvalue = "//*[@id='_";

	// // @FindBy(className = "centered")
	// @FindBy(xpath = "//td[@class='centered']/input")
	// private WebElement centeredButton;
	//
	// // @FindBy(xpath ="//div[@class='carousel-inner']/div/div[3]/a/i")
	// // @FindBy(xpath="html/body/div[1]/form/div/div/div[1]/div[3]/a")
	// @FindBy(xpath = "//a[@class='btn btn-next']/i")
	// private WebElement bookADateSubmit;

	@FindBy(className = "dnWidget")
	private WebElement frame;

	@FindBy(linkText = "Continue")
	private WebElement bookADateSubmit;

	@FindBy(xpath = "//td[@class='centered']/input")
	private WebElement centeredButton;

	@FindBy(xpath = "//input[@name='childCareDetails-CHLDAGEGP005']")
	private WebElement numberOf12YrsPlusChildren;

	@FindBy(xpath = ".//*[@id='step3-btn']")
	private WebElement submitNoOfKids;

	@FindBy(xpath = "//*[@id='postReviewForm']/div/div[2]/div/div/a")
	private WebElement reviewOption;

	// Job includes
	@FindBy(xpath = "//input[@value='DNJOBACTI001']")
	private WebElement feedingDinner;
	@FindBy(xpath = "//input[@value='DNJOBACTI002']")
	private WebElement bathTime;
	@FindBy(xpath = "//input[@value='DNJOBACTI003']")
	private WebElement bedTime;
	@FindBy(xpath = "//input[@value='DNJOBACTI004']")
	private WebElement playTime;

	@FindBy(name = "DOBMonth")
	private WebElement dobMonth;

	@FindBy(name = "DOBDay")
	private WebElement dobDay;

	@FindBy(name = "DOBYear")
	private WebElement dobYear;

	@FindBy(name = "paymentType")
	private WebElement paymentType;

	// Subjects for Tutoring
	@FindBy(xpath = "//input[@value='GENRALSUB004']")
	private WebElement engineering;

	@FindBy(xpath = "//input[@value='GENRALSUB014']")
	private WebElement dance;

	@FindBy(xpath = "//input[@value='GENRALSUB002']")
	private WebElement business;

	@FindBy(xpath = "//input[@value='GENRALSUB007']")
	private WebElement math;

	@FindBy(name = "attr-job.specialNeeds.diagnosis")
	private WebElement diagnosis;

	@FindBy(xpath = "//input[@value='SNAGEGRUP001']")
	private WebElement infant;

	@FindBy(xpath = "//input[@value='SNAGEGRUP002']")
	private WebElement adult;

	@FindBy(xpath = "//input[@value='SNAGEGRUP003']")
	private WebElement youth;

	@FindBy(xpath = "//input[@value='SNAGEGRUP004']")
	private WebElement senior;

	@FindBy(xpath = "//input[@value='SNAGEGRUP005']")
	private WebElement teen;

	private WebElement needCare;
	private String needCareXpath = "//div[@class='jobDatesAndTimesSection']//label[";

	private WebElement howSoonCareNeeded;
	private String careNeededXpath = "//div[@class='row clearfix']//div[";

	private WebElement selectDay;
	private String selectDayXpath = "//div[@id='recurringCont']//label[";

	private String selectDayXpathV2 = "(//div[@class='recurringCont']/div/div/span)";

	// list view of jobtimings
	@FindBy(xpath = "//div[@class='l_r_c3']//span[contains(text(),'Start Time')]")
	public WebElement jobStartTimeli;

	@FindBy(xpath = "//div[@class='l_r_c3']//span[contains(text(),'End Time')]")
	public WebElement jobEndTimeLi;

	// @FindBy(xpath =
	// "//tr[@id='datePicker0']//div[@class='customSelect']//span[@class='arrow']")
	@FindBy(xpath = ".//span[contains(text(),'AM/PM')]|.//a[contains(text(),'AM/PM')]")
	private WebElement jobStartAm_pm;
	 private String jobStartAm_pmXpath = "//tr[@id='datePicker0']//li["; 

	@FindBy(xpath = "//tr[@id='datePicker4']//div[@class='customSelect']//span[@class='arrow']")
	private WebElement jobEndAm_Pm;

	@FindBy(id = "enterDate4")
	private WebElement enterDate4;

	@FindBy(id = "enterDate5")
	private WebElement occasionalJobPostDate;

	// list of payrate
	@FindBy(xpath = "//div[@class='leftratediv']//span[contains(@class,'arrow')]")
	private WebElement minpayRateDropdown;

	@FindBy(xpath = "//div[@class='rightratediv']//span[contains(@class,'arrow')]")
	private WebElement maxPayRateDropdow;

	private WebElement houseKeepingServices;
	private String hkServicesxpath = "//div[@class='childCareAges']//input[@value='HKSERVCES00";

	private WebElement hkPremiumServices;
	private String hkPremiumServicesxpath = "//input[@name='attr-job.housekeeping.services' and @value='HKSERVCES00";

	private WebElement supplies;
	private String supliesXpath = "//input[@name='attr-job.provideSupplies' and  @value='";

	private WebElement equipment;
	private String equipmentXpath = "//input[@name='attr-job.provideEquipments' and  @value='";

	private WebElement availableForAfterSchool;
	private String availableForAfterSchoolXpath = "//input[@name='attr-job.afterSchool' and @value='";

	private WebElement availableForBeforeSchool;
	private String availableForBeforeSchoolXpath = "//input[@name='attr-job.beforeSchool' and @value='";

	private WebElement summerCareJob;
	private String summerCareJobXpath = "//input[@name='attr-job.summerCare' and  @value='";

	@FindBy(xpath = "//div[@class='jobDetailsSection']//span[contains(@class,'arrow')]")
	private WebElement numberOfChildrenDropdown;

	private WebElement typeOfCare;
	private String typeOfCare_xpath = "//input[@type='radio' and @value='CHLDCGTYP00";

	private WebElement selectSimulatorDay;
	private String selectSimulatorDayXpath = "//div[@id='recurringCont']//span[";

	@FindBy(xpath = "//table[@id='startAndEndJobTime']//div[@class='dojoxRangeSliderBarContainer']/div[1]")
	private WebElement timedraggable;

	@FindBy(xpath = "//table[@id='startAndEndJobTime']//div[@class='dojoxRangeSliderBarContainer']/div[3]")
	private WebElement timedraggable1;

	@FindBy
	private WebElement servicesNeededfor;
	private String petCareXpath = "//input[@value='0' and @name='petCareDetails-PCANMNP00";

	@FindBy(xpath = "//table[@id='hourlyRateFromAndTo']//div[@class='dojoxRangeSliderBarContainer']/div[1]")
	private WebElement payDraggable;

	@FindBy(xpath = "//table[@id='hourlyRateFromAndTo']//div[@class='dojoxRangeSliderBarContainer']/div[3]")
	private WebElement payDraggable1;

	@FindBy(xpath = ".//div[@id[contains(.,'submit')]]|.//a[@id[contains(.,'submit')]]")
	private WebElement lastPageSubmit;

	private WebElement jobDetails;
	private String jobDetailsXpath = "//div[@class='counter']/div[";

	private WebElement petDetails;
	private String petDetailsXpath = "//div[@class='counter']/div[";

	@FindBy(xpath = "//div[@class='buttonWrapper']//span[@class='increment']")
	private WebElement transportDetails;

	private WebElement seniorCareEnrollneeds;
	private String seniorCareEnrollXpath = "//input[@value='SCENRNEED00";

	// Services Needed
	@FindBy
	private WebElement seniorCareServices;
	private String seniorCarePath = "//input[@type='checkbox' and @value='SCSVCND00";

	 @FindBy(xpath = "//*[@name='attr-job.seniorCare.recipientAge']") 
	@FindBy(xpath = "//*[@id='awPostJobForm']/div[5]/div[2]/div[2]/div/div/a")
	private WebElement ageDropdown;

	 @FindBy(xpath = "//*[@name='attr-job.seniorCare.relation']") 
	@FindBy(xpath = "//*[@id='awPostJobForm']/div[5]/div[3]/div[2]/div/div/a")
	private WebElement relationshipDropdown;

	private WebElement typesOfSeniorCaregiver;
	private String seniorCaregiverXpath = "//input[@value='SCCGTYP00";

	private WebElement seniorServices;
	private String seniorServicesXpath = "//input[@value='SCSVCND00";

	// @FindBy(xpath =
	// "//div[@class='l_r_c4' and
	// @name='attr-job.specialNeeds.diagnosis']//span")
	@FindBy(xpath = "//div[@class='l_r_c4']/div/div/div/div/span[2]")
	private WebElement diagnosisDropdown;

	@FindBy(xpath = "//a[@class='myCaregivers']")
	private WebElement myCaregivers;

	@FindBy(xpath = "//a[contains(text(), 'My Messages')]")
	private WebElement myMessages;
	
	@FindBy(xpath = "//*[@id='all_tabs']/span/ul/li[5]/a/div")
	private WebElement msg;
	
	@FindBy(xpath= ".//*[@id='monetate_lightbox_contentMap']/area[1]")
	private WebElement monetateLightbox;
	
	@FindBy(className = "list-item open-message-thread active")
	private WebElement latestMessage;
	
	@FindBy(className = "form-control text-message")
	private WebElement reply;

	@FindBy(xpath = "//*[@name='attr-job.specialNeeds.diagnosis' and @value='SNSOCLEMO001']")
	private WebElement spldiagnosisDropdown;

	@FindBy(xpath = "//*[@name='primaryPhoneType']/option[3]")
	private WebElement coOpPhone;

	@FindBy(xpath = "//*[@class='centered']/input[@type='image']")
	private WebElement nextbtn;

	// @FindBy(xpath = "//img[@alt='Child Care']")
	@FindBy(xpath = "//div[@class='inner clearfix']/div[2]/a[1]")
	private WebElement childLogo;

	// @FindBy(xpath = "//img[@alt='Special Needs']")
	@FindBy(xpath = "//div[@class='inner clearfix']/div[4]/a[3]")
	private WebElement splLogo;

	// @FindBy(xpath = "//img[@alt='Senior Care']")
	@FindBy(xpath = "//div[@class='inner clearfix']/div[2]/a[3]")
	private WebElement seniorLogo;

	// @FindBy(xpath = "//img[@alt='Pet Care']")
	@FindBy(xpath = "//div[@class='inner clearfix']/div[3]/a[1]")
	private WebElement petLogo;

	// @FindBy(xpath = "//img[@alt='Housekeeping']")
	@FindBy(xpath = "//div[@class='inner clearfix']/div[3]/a[2]")
	private WebElement hkLogo;

	// @FindBy(xpath = "//img[@alt='Errands Odd Jobs']")
	@FindBy(xpath = "//div[@class='inner clearfix']/div[3]/a[3]")
	private WebElement caregigLogo;

	// @FindBy(xpath = "//img[@alt='Tutoring Lessons']")
	@FindBy(xpath = "//div[@class='inner clearfix']/div[4]/a[1]")
	private WebElement tutorLogo;

	// @FindBy(xpath = "//img[@alt='Transportation']")
	@FindBy(xpath = "//div[@class='inner clearfix']/div[4]/a[2]")
	private WebElement transportLogo;

	@FindBy(name = "photo")
	private WebElement photoPath;

	@FindBy(id = "continueButton")
	private WebElement karooContinue;
	
	@FindBy(xpath = "//*[@id='vhp_header']/ul/li[4]/button")
	private WebElement plJoinNow;

	@FindBy(xpath = ".//*[@id='singlePageEnrollmentForm']/div[12]/button")
	private WebElement joinNow;

	@FindBy(id = "groupName")
	private WebElement groupName;

	@FindBy(id = "groupShortName")
	private WebElement groupShortName;

	@FindBy(xpath = "//input[@name ='contentPublic' and @value='false']")
	private WebElement groupPrivate;

	@FindBy(xpath = "//input[@name='contentPublic' and @value='true']")
	private WebElement groupPublic;

	@FindBy(xpath = "//input[@name='coOps' and @value='true']")
	private WebElement coopGroup;

	@FindBy(name = "allowCaregiversToContact")
	private WebElement allowSitterContact;

	@FindBy(xpath = "//div[@class='sectionBody']/div[4]/div[1]/div/div")
	private WebElement creditCardMonth;

	@FindBy(xpath = "//div[@class='sectionBody']/div[4]/div[2]/div/div")
	private WebElement creditCardYear;

	// Objects for PIAP seeker enrollment

	@FindBy(id = "stepNodeAction-1-1-seeker")
	private WebElement findCare;

	@FindBy(xpath = "//*[@id='stepNodeAction-2-Children-seeker']")
	private WebElement myKids;

	@FindBy(xpath = "//*[@id='stepNodeAction-3-Children-seeker']")
	private WebElement nanniesBabies;

	@FindBy(xpath = "//*[@id='stepNodeAction-4-I-seeker']")
	private WebElement rightNow;

	@FindBy(partialLinkText = "Get Started")
	private WebElement getStart;

	@FindBy(xpath = "//*[@id='smartEnrollmentForm']/div/div[6]/div/input")
	private WebElement emailPiap;

	@FindBy(xpath = "//*[@id='smartEnrollmentForm']/div/div[7]/input")
	private WebElement pwdPiap;

	@FindBy(partialLinkText = "Continue")
	private WebElement continuePiap;

	@FindBy(xpath = "//*[@id='smartEnrollmentForm']/div/div[11]/input")
	private WebElement joinNowPiap;

	@FindBy(id = "PageContent_wiz_ucClient_conPhones_nxtHomePhone")
	private WebElement homePhone;

	@FindBy(id = "PageContent_wiz_ucClient_conPhones_nxtWorkPhone")
	private WebElement workPhone;

	@FindBy(id = "PageContent_wiz_rblCarRequired_0")
	private WebElement publicTransport;

	@FindBy(id = "PageContent_wiz_rblUsedBefore_0")
	private WebElement serviceUsedBefore;

	@FindBy(id = "PageContent_wiz_StartNavigationTemplateContainerID_btnNext")
	private WebElement nextStep;

	@FindBy(id = "PageContent_wiz_StepNavigationTemplateContainerID_btnNext")
	private WebElement nextStepPiap;

	@FindBy(id = "PageContent_wiz_txtFirstNameChild")
	private WebElement childFirstName;

	@FindBy(id = "PageContent_wiz_txtLastNameChild")
	private WebElement childLastName;

	@FindBy(name = "&lid=Log Out&lpos=l0")
	private WebElement logout;

	@FindBy(id = "PageContent_wiz_btnAddChild")
	private WebElement addChild;

	@FindBy(name = "ctl00$PageContent$wiz$txtNameOnCard")
	private WebElement nameOnCard;

	@FindBy(id = "PageContent_wiz_chkAccept")
	private WebElement acceptPiap;

	@FindBy(id = "PageContent_wiz_FinishNavigationTemplateContainerID_btnFinish")
	private WebElement finishPiap;

	@FindBy(id = "PageContent_hypBucReturn")
	private WebElement clickHere;

	@FindBy(id = "PageContent_wiz_ddlCardTypes")
	private WebElement cardType;

	@FindBy(name = "ctl00$PageContent$wiz$txtNameOnCard")
	private WebElement cardName;

	@FindBy(name = "ctl00$PageContent$wiz$txtCardNumber")
	private WebElement cardNo;

	@FindBy(id = "PageContent_wiz_ddlMonth")
	private WebElement cardMonth;

	@FindBy(id = "PageContent_wiz_ddlYear")
	private WebElement cardYear;

	@FindBy(className = "bgBtn")
	private WebElement bgRequestOld;

	@FindBy(linkText = "Request for free")
	private WebElement bgRequestMaguire;

	@FindBy
	private WebElement providerBGC;

	@FindBy
	private WebElement requestLink;

	// Review and message webElements
	@FindBy(partialLinkText = "Send Message")
	private WebElement messageLink;

	@FindBy(xpath = "//a[contains(text(),'Contact ')]")
	private WebElement maguireMessageLink;

	@FindBy(xpath = "//a[contains(text(),'Message ')]")
	private WebElement maguireMessageContact;

	@FindBy(xpath = "//a[contains(text(),'Interviewed with you')]")
	private WebElement relationDropDown;

	@FindBy(xpath = "//div[@class='czenCustomSelect']/div/a")
	private WebElement relation;
	private String relationPath = "//div[contains(@class,'Open')]//li[";

	@FindBy
	private WebElement overallRating;
	private String overallRatingPath = ".//*[@id='star_containr']/ul/li[";

	@FindBy
	private WebElement punctuality;
	private String punctualityPath = "//div[@id='punctuality']/span[";
	@FindBy
	private WebElement reliableTransportation;
	private String reliableTransportationPath = "//div[@id='reliableTrans']/span[";
	@FindBy
	private WebElement dependability;
	private String dependabilityPath = "//div[@id='dependability']/span[";
	@FindBy
	private WebElement accurateCalendar;
	private String accurateCalendarPath = "//div[@id='accurateCal']/span[";

	@FindBy
	private WebElement hireagain;
	private String hireagainPath = "//div[@id='rehire']/span[";

	@FindBy(id = "writeReview")
	private WebElement reviewComment;

	@FindBy(xpath = "//button[contains(text(),'Submit feedback')]")
	private WebElement submitReview;

	@FindBy(xpath = "//textarea[@id='mpTextarea']")
	private WebElement messageSubject;

	@FindBy(xpath = "//textarea[@id='mpTextarea']")
	private WebElement messageBody;

	@FindBy(xpath = "//input[@id='msgSubject']")
	private WebElement maguireSubject;

	@FindBy(xpath = "//button[@id='smSumbit']")
	private WebElement clickSendMessage;

	@FindBy(xpath = "//div[@class='dijitDialog inlineHelpCloud bgCheckCloud inlineHelpCloudFocused bgCheckCloudFocused dijitFocused']//span")
	private WebElement closeDialog;

	// Objects for CSR BUC booking approval

	@FindBy(name = "sitterName")
	private WebElement sitterName;

	@FindBy(xpath = "//textarea[@name='csrForm[0].notes']")
	private WebElement csrNotes;


	@FindBy(id = "stepNodeAction-1-1-seeker")
	private WebElement postSeekerJob;

	@FindBy(id = "stepNodeAction-2-Children-seeker")
	private WebElement childCareSeeker;

	@FindBy(id = "stepNodeAction-2-Home-seeker-3")
	private WebElement homeCareSeeker;

	@FindBy(id = "stepNodeAction-2-Pets-seeker-3")
	private WebElement petCareSeeker;

	@FindBy(id = "stepNodeAction-2-Seniors-seeker-3")
	private WebElement seniorCareSeeker;

	@FindBy(id = "stepNodeAction-3-Children-seeker")
	private WebElement nannyCareSeeker;

	@FindBy(id = "stepNodeAction-3-Tutoring-seeker")
	private WebElement tutoringSeeker;

	@FindBy(id = "stepNodeAction-3-SpecialNeed-seeker")
	private WebElement specialNeedsSeeker;

	@FindBy(xpath = "//div[@id='stepNodeAction-3-Children-seeker' and @data-subcategory='AfterSchool']")
	private WebElement afterSchoolSitter;
	
	@FindBy(xpath=".//*[@id='stepNodeAction-4-dateNightSitter-seeker']")
	private WebElement dateNightSitter;

	@FindBy(id = "stepNodeAction-4-W-seeker")
	private WebElement careNeedsOn;

	@FindBy(id = "firstName")
	private WebElement firstNameWithId;

	@FindBy(id = "lastName")
	private WebElement lastNameWithId;

	@FindBy(name = "cityStateZIP")
	private WebElement cityStateZip;

	@FindBy(xpath = "//form[@id='smartEnrollmentForm']/div/div[6]/div/input")
	private WebElement emailId;

	@FindBy(xpath = "//form[@id='smartEnrollmentForm']/div/div[7]/input")
	private WebElement pwd;
	@FindBy(xpath = "//form[@id='smartEnrollmentForm']/div/div[11]/input")
	private WebElement submitForEnroll;

	@FindBy(xpath = "//a[@href='/seeker/capturePostJobStep1.do?serviceId=CHILDCARE']")
	private WebElement merchandisePAJCHILDCARE;

	@FindBy(xpath = "//a[@href='/seeker/capturePostJobStep1.do?serviceId=SENIRCARE&attr-job.seniorCare.caregiverType=SCCGTYP001']")
	private WebElement merchandisePAJSENIRCARE;

	@FindBy(xpath = "//a[@href='/seeker/capturePostJobStep1.do?serviceId=PETCAREXX&attr-job.petCare.caregiverType=PCCGTYP001']")
	private WebElement merchandisePAJPETCAREXX;

	@FindBy(xpath = "//a[@href='/seeker/capturePostJobStep1.do?serviceId=HOUSEKEEP&jobSubType=HOUSEKEEPER']")
	private WebElement merchandisePAJHOUSEKEEP;

	@FindBy(xpath = "//a[@href='/seeker/capturePostJobStep1.do?serviceId=TUTORINGX']")
	private WebElement merchandisePAJTUTORINGX;

	@FindBy(xpath = "//a[@href='/seeker/capturePostJobStep1.do?serviceId=CAREGIGSX'][1]")
	private WebElement merchandisePAJCAREGIGSX;

	@FindBy(xpath = "//a[@href='/seeker/capturePostJobStep1.do?serviceId=TRANSPORTATION&attr-job.transportation.transportationFor=TRANFOR001']")
	private WebElement merchandisePAJTRANSPORTATION;

	@FindBy(xpath = "//a[@href='/seeker/capturePostJobStep1.do?serviceId=SPCLNEEDS']")
	private WebElement merchandisePAJSPCLNEEDS;

	@FindBy(xpath = "//input[@value='MON|ALL']")
	private WebElement mondayAll;

	@FindBy(xpath = "//input[@value='TUE|ALL']")
	private WebElement tuesdayAll;

	@FindBy(xpath = "//input[@value='WED|ALL']")
	private WebElement wednesdayAll;

	@FindBy(xpath = "//input[@value='THU|ALL']")
	private WebElement thursdaydayAll;

	@FindBy(xpath = "//input[@value='FRI|ALL']")
	private WebElement fridayAll;

	@FindBy(xpath = "//input[@value='SAT|ALL']")
	private WebElement saturdayAll;

	@FindBy(xpath = "//*[@id='smartEnrollmentForm']/div/div[@class='ask submit submitBtn']/input")
	private WebElement joinNowStepButton;

	private WebElement petCareServices;
	private String petCareServicesXpath = "//input[@name='attr-job.petCare.additionalRequirements' and @value='PCADDRQ00";

	@FindBy(xpath = "//select[@name='attr-job.seniorCare.recipientAge']")
	private WebElement seniorsAge;

	@FindBy(partialLinkText = "Let me get a feel for Care.com for free")
	private WebElement gbbFree;

	@FindBy(id = "DATENIGHT")
	private WebElement datenight;

	@FindBy(className = "joinNowLink")
	private WebElement JoinNow;

	@FindBy(partialLinkText = "Ok, got it")
	private WebElement bookingPopup;

	@FindBy(xpath = "//*[@id='grid']/div[1]/div[3]/div[2]/label/span")
	private WebElement goodOneMonth;

	@FindBy(xpath = "//*[@id='grid']/div[1]/div[3]/div[1]/label/span")
	private WebElement goodSixMonth;

	@FindBy(xpath = "//*[@id='grid']/div[2]/div[3]/div[2]/label/span")
	private WebElement betterOneMonth;

	@FindBy(xpath = "//*[@id='grid']/div[2]/div[3]/div[1]/label/span")
	private WebElement betterSixMonth;

	@FindBy(xpath = "//*[@id='grid']/div[3]/div[3]/div[2]/label/span")
	private WebElement bestOneMonth;

	@FindBy(xpath = "//*[@id='grid']/div[3]/div[3]/div[1]/label/span")
	private WebElement bestSixMonth;

	@FindBy(xpath = "//*[@id='wizardNextStep']/div/div/a/input")
	private WebElement continueButton1;

	@FindBy(className = "booking-link-ok")
	private WebElement bookingCloudOkButton;

	// booking Schedule

	@FindBy(xpath = "(//a[contains(text(),'Phone Interview')])[2]|//div[contains(text(),'Phone Interview')]")
	private WebElement phoneInterview;

	@FindBy(xpath = "(//a[contains(text(),'In-Person Interview')])[2]|//div[contains(text(),'In-Person Interview')]")
	private WebElement inPersonInterview;

	String bookColSelection = ".//*[@id='cal']/div/div/table/tbody/tr/td/div/div/div/table/tbody/tr/td[";
	String bookRowSelection = ".//*[@id='cal']/div/div/table/tbody/tr/td/div/div/div[2]/table/tbody/tr[";

	@FindBy(linkText = "Ok, thanks")
	private WebElement Okthanks;

	@FindBy(xpath = "//button[contains(text(),'Next')]")
	private WebElement nextLink;

	@FindBy(xpath = "//div[contains(text(),'Request interview')]")
	private WebElement requestInterview;

	@FindBy(name = "note")
	private WebElement optionalNote;

	@FindBy(name = "bookerPhone")
	private WebElement bookerPhone;

	@FindBy(linkText = "No thanks")
	private WebElement appDownloadLink;

	String sendMessageConfirmationButtonXpath = "(//div[@class='fraud-cloud']/div/div[@class='buttons']/div/button[contains(text(),'Send message')])";
	@FindBy(xpath = "(//div[@class='fraud-cloud']/div/div[@class='buttons']/div/button[contains(text(),'Send message')])[last()]")
	private WebElement sendMessageConfirmationButton;

	@FindBy(xpath = "//a[@class='btn btn-flat btn-secondary btn-wide sendMessage boldBtn']")
	private WebElement sendMessageButtonProfilePage;

	@FindBy(className = "bk-send-message")
	private WebElement replyTextBoxInThread;

	@FindBy(xpath = "//input[@class='submitMessage btn btn-flat btn-secondary']")
	private WebElement replyButtonInThread;

	@FindBy(xpath = "//div[@class='right']/div[@class='section']/div/span[@class='textSmallBold']/a")
	private WebElement seekerProfileLinkOnJob;

	@FindBy(xpath = "//a[contains(@href,'upgrade')]")
	private WebElement upgradeNow;

	@FindBy(xpath = "//a[@href='/upgrade-upgrade-p1029-q13.html']")
	private WebElement upgradenow1;

	@FindBy(xpath = "//a[contains(@href,'html')]/ancestor-or-self::a[contains(text(),'Upgrade now')]")
	private WebElement upgradenow;

	@FindBy(xpath = "//a[contains(text(),'Change')]")
	private WebElement changeCard;

	@FindBy(xpath = "//*[@id='upgradeBtn']")
	private WebElement upgrade;

	@FindBy(xpath = "//a[contains(text(),'Upgrade today')]")
	private WebElement upgradeToday;

	@FindBy(name = "promoCode")
	private WebElement promoCodeField;

	@FindBy(xpath = "//a[contains(text(),'Babysitter or Nanny')]")
	private WebElement babysitterornanny;

	@FindBy(xpath = "//*[@id='recurring-toggle']")
	private WebElement recurringtoggle;

	private WebElement educationlevel;
	private String educationlevelXPath = "//*[@id='nthDayPaj']/div[";

	private WebElement Collegedegree;
	private String CollegedegreeXPath = "html/body/div[";

	private WebElement saveAndcontinue;
	private String saveAndcontinueXpath = "//*[@id='nthDayPaj']/div[";

	private WebElement seniorAgeRelation;
	private String seniorAgeRelationXPath = "//*[@id='nthDayPaj']/div[3]/div[1]/dl/dd[";

	@FindBy(xpath = "//*[@id='nthDayPaj']/div[2]/button")
	private WebElement submitJob;

	@FindBy(xpath = "//a[contains(@href,'SPCLNEEDS')]")
	private WebElement splneed;

	@FindBy(xpath = "//a[contains(@href,'HOUSEKEEPER')]")
	private WebElement housekeep;

	@FindBy(xpath = "//a[contains(text(),'Pet Sitter')]")
	private WebElement petcare;

	@FindBy(xpath = "//a[contains(text(),'Tutoring / Lessons')]")
	private WebElement tutororlessons;

	@FindBy(xpath = "//a[contains(text(),'Companion Care')]")
	private WebElement campanioncare;

	@FindBy(xpath = "//a[contains(text(),'Errands')]")
	private WebElement errandjob;

	@FindBy(xpath = "//*[@id='job-title']")
	private WebElement JobTitle;

	@FindBy(xpath = "//*[@id='job-description']")
	private WebElement JobDescription;

	@FindBy(xpath = "//form[@class='smartForm']/div[4]/div[1]")
	private WebElement expiryMonth;

	@FindBy(xpath = "//form[@class='smartForm']/div[4]/div[2]")
	private WebElement expiryYear;

	public SitePageObject(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[contains(text(),'Submit')]")
	private WebElement tuitionSeeker;

	@FindBy(xpath = "//*[contains(text(),'Tell me more')]")
	private WebElement tuitionSitter;

	@FindBy(xpath = "//div[text()=' Select membership']")
	private WebElement upgradeFirstStepButton;

	@FindBy(xpath = "//*[@id='upgradeMembershipPlanForm']/div[1]/div[2]")
	private WebElement upgradeSecondStepButton;

	@FindBy(xpath = ".//*[@id='singlePageEnrollmentForm']/div[4]/div[2]")
	private WebElement enterPayment;

	@FindBy(id = "expDate")
	private WebElement ccExpiryDateTextBox;

	@FindBy(xpath = "//input[@id='dateOfBirth']")
	private WebElement dobTextBox;

	@FindBy(xpath = "//*[contains(text(),'End Subscription')]")
	private WebElement endSubscription;

	@FindBy(xpath = "//*[contains(text(),'Remind')]")
	private WebElement remindMeLater;

	@FindBy(xpath = "//*[contains(text(),'Continue Subscription')]")
	private WebElement continueSubscription;

	@FindBy(id = "extendLink")
	private WebElement extendLink;

	@FindBy(id = "noThanksLink")
	private WebElement noThanksLink;

	@FindBy(xpath = ".//*[@id='monetate_lightbox_contentMap']/area[2]")
	private WebElement monetatePopUp;

	@FindBy(xpath = ".//*[@id='monetate_lightbox_contentMap']/area[1]")
	private WebElement closeMonetatePopUp;

	@FindBy(id = "hireYes")
	private WebElement hireYes;

	@FindBy(xpath = ".//*[@id='facepile-list']/div/a")
	private WebElement facePile;

	private WebElement facePileSelect;
	private String facePileSelectXPath = "//a[contains(@href,'";

	@FindBy(xpath = "//*[contains(@class,'editProfileCloud')]")
	private WebElement seekerBio;

	@FindBy(xpath = "//textarea[@name='careNeeds']")
	private WebElement seekerBioText;

	@FindBy(xpath = "//button[@type='button' and contains(text(),'Save')]")
	private WebElement saveButton;

	@FindBy(xpath = "//a[@class='myProfile']")
	private WebElement clickMyProfile;

	private WebElement seekerFamily;
	private String seekerFamilyXPath = ".//*[@id='dojoUnique";

	@FindBy(xpath = ".//*[@id='dojoUnique2']/div")
	private WebElement family1;

	@FindBy(xpath = ".//*[@id='you']")
	private WebElement familyYou;

	@FindBy(xpath = ".//*[@id='partner']")
	private WebElement familyPartner;

	@FindBy(xpath = ".//*[@id='children']")
	private WebElement familyChildren;

	@FindBy(xpath = ".//*[@id='extendedFamily']")
	private WebElement familyExtend;

	@FindBy(xpath = ".//*[@id='pets']")
	private WebElement familyPet;

	@FindBy(xpath = "//input[@name='accountStatus' and @value='confirmSnooze']")
	private WebElement regularSnooze;

	@FindBy(xpath = "//input[@name='accountStatus' and @value='downgradeConfirm']")
	private WebElement regularDowngrade;

	@FindBy(xpath = "//*[@id='promoCodeForm']/a")
	private WebElement enterPromoCode;

	@FindBy(xpath = "//a[@class='applyCodeBtn']")
	private WebElement applyPromoCode;

	@FindBy(id = "one-time-toggle")
	private WebElement oneTimeToggle;

	@FindBy(id = "otStartDate")
	private WebElement oneTimeStartDate;

	@FindBy(id = "otEndDate")
	private WebElement oneTimeEndDate;

	@FindBy(name = "oneTimeJobStartDate")
	private WebElement oneTimeStartDateHK;

	@FindBy(name = "oneTimeJobEndDate")
	private WebElement oneTimeEndDateHK;

	private WebElement searchSendMessage;
	private String searchSendMessageXPath = "(//*[contains(text(),'Send message')])[position()=";

	private WebElement clickProviderSearchPage;
	private String clickProviderSearchPageXPath = "(//a[contains(@class,'socialSearch')])[position()=";

	@FindBy(xpath = "//*[contains(text(),'Near Me')]")
	private WebElement clickNearMe;

	@FindBy(xpath = "//*[contains(text(),'My Favorites')]")
	private WebElement clickMyFavorites;

	@FindBy(xpath = "//a[contains(@class,'btn-keep')]")
	private WebElement jobAccept;

	@FindBy(xpath = "//a[contains(@class,'btn-decline')]")
	private WebElement jobDecline;

	
	 * Backup-care
	 * 
	 * Newly added
	 

	@FindBy(xpath = "(//div[contains(text(), 'How soon do you need it?')])[1]")
	private WebElement howSoonDoUNeedIt;

	private static final java.util.logging.Logger logger = Loggers.getLogger(SitePageObject.class);

	@FindBy(xpath = ".//*[@id='dropdownTop']/div[1]/a")
	private WebElement todayDropdown;

	@FindBy(xpath = ".//*[@id='startDate']")
	private WebElement clickOnCalendar;

	@FindBy(xpath = "//button[@class='btn btn-primary flow-next btn-next' and @type='submit']")
	private WebElement clickOnNext;

	@FindBy(xpath = "//label[input[@name='additionalInfo.allergies.enabled' and @value='false' ]]")
	private WebElement clickOnBabyAllergies;

	@FindBy(xpath = "//label[input[@name='additionalInfo.medicationsToBeAdministered.enabled' and @value='false' ]]")
	private WebElement clicknOnMedicationsToBeAdministered;

	@FindBy(xpath = "//label[input[@name='additionalInfo.additionalCareNeeds.enabled' and @value='false' ]]")
	private WebElement clickOnAdditionalCareNeeds;

	@FindBy(xpath = "//label[input[@name='knowledgeUniverseWaivers.mediaCapture' and @value='false' ]]")
	private WebElement clickOnMediaCapture;

	@FindBy(xpath = "//button[@class='btn btn-primary btn-next btn-where']")
	private WebElement clickOnNextButton;

	@FindBy(xpath = "//button[@class='btn btn-primary btn-next btn-why']")
	private WebElement NextButton;

	@FindBy(xpath = "//button[@class='btn btn-primary btn-next btn-where']")
	private WebElement nextButton;

	@FindBy(xpath = "//label[input[@value='CHILDCARE_UNAVAILABLE' and @name='reasonForCare' ]]")
	private WebElement clickOnChildCareUnavailable;

	@FindBy(xpath = "//label[input[@value='303029' and @name='incenter' ]]")
	private WebElement selectCareCenterForm;

	@FindBy(xpath = "//*[contains(text(), 'Continue')]")
	private WebElement conTinue;

	@FindBy(id = "phoneNumber")
	private WebElement BUCphoneNumber;

	@FindBy(xpath = "//*[contains(text(), 'Submit')]")
	private WebElement BUCSubmit;

	@FindBy(xpath = "(//*[contains(text(), 'Expiration Month')])[2]")
	private WebElement myAccountSelectExpirationMonth;

	@FindBy(xpath = "(//*[contains(text(), 'Expiration Year')])[2]")
	private WebElement myAccountSelectExpirationYear;

	@FindBy(id = "email")
	private WebElement BUCemail;

	@FindBy(id = "password")
	private WebElement BUCpassword;

	@FindBy(xpath = "//span[@class='defaultSel']")
	private WebElement iAmA;

	@FindBy(xpath = "//li[contains(text(), 'Babysitter')]")
	private WebElement babySitterProvider;

	@FindBy(xpath = "//input[@name='schoolName']")
	private WebElement schoolName;

	@FindBy(xpath = "//input[@name='yearOfGraduation']")
	private WebElement yearOfGraduation;

	@FindBy(xpath = "//a[contains(text(), 'Skip for now')]")
	private WebElement skipForNow;

	@FindBy(xpath = "//*[@name='question1']")
	private WebElement question1;

	@FindBy(xpath = "//*[@name='question2']")
	private WebElement question2;

	@FindBy(xpath = "//*[@value='Get Started']")
	private WebElement getStarted1;

	@FindBy(xpath = "//*[@name='question3']")
	private WebElement question3;

	@FindBy(xpath = "//li[contains(text(), 'Weekends')]")
	private WebElement weekends;

	@FindBy(id = "infoSubmit")
	private WebElement infoSubmit;

	@FindBy(xpath = "//input[@value='CCADDSRVC005']")
	private WebElement carPooling;

	@FindBy(xpath = "//*[contains(text(), 'Log Out')]")
	private WebElement logOuT;

	@FindBy(xpath = "//select[@class='form-control']")
	private WebElement formControl;

	@FindBy(xpath = "//select[@name='yearsOfExperience']")
	private WebElement yearsOfExperience;

	@FindBy(id = "phone")
	WebElement BUCphone;

	@FindBy(id = "relationToChild")
	WebElement relationToChild;

	@FindBy(id = "e0firstName")
	private WebElement EmergencyFirstName;

	@FindBy(id = "e1firstName")
	private WebElement EmergencyFirstName1;

	@FindBy(id = "e0lastName")
	private WebElement EmergencyLastName;

	@FindBy(id = "e1lastName")
	private WebElement EmergencyLastName1;

	@FindBy(id = "e0phone")
	private WebElement EmergencyPhone;

	@FindBy(id = "e1phone")
	private WebElement EmergencyPhone1;

	@FindBy(id = "e0addressLine1")
	private WebElement EmergencyAddressLine1;

	@FindBy(id = "e1addressLine1")
	private WebElement EmergencyAddressLine11;

	@FindBy(id = "e0addressLine2")
	private WebElement EmergencyAddressLine2;

	@FindBy(id = "e0csz")
	private WebElement zipOfEmergency;

	@FindBy(id = "e1csz")
	private WebElement zipOfEmergency1;

	@FindBy(id = "e1addressLine2")
	private WebElement EmergencyAddressLine21;

	@FindBy(id = "e0relationshipWithChild")
	private WebElement EmergencyRelationshipWithChild;

	@FindBy(id = "e1relationshipWithChild")
	private WebElement EmergencyRelationshipWithChild1;

	@FindBy(id = "pediatricianName")
	private WebElement DoctorName;

	@FindBy(id = "pediatricianPhone")
	private WebElement Doctorsphone;

	@FindBy(id = "insuranceLastPhysicalExamDate")
	private WebElement LastPhyiscalExamDate;

	@FindBy(id = "hospitalDisplayName")
	private WebElement HospitalName;

	@FindBy(id = "pediatricianAddressLine1")
	private WebElement HospitalAddressLine1;

	@FindBy(id = "pediatricianAddressLine2")
	private WebElement HospitalAddressLine2;

	@FindBy(id = "csz")
	private WebElement HospitalZip;

	@FindBy(id = "insuranceProvider")
	private WebElement InsuranceCompany;

	@FindBy(id = "insuranceAccountNumber")
	private WebElement PolicyNumber;

	@FindBy(xpath = "//a[@id='btn_submit_cc_a']")
	private WebElement ConfirmRequest;

	@FindBy(xpath = ".//*[@id='e0canPickupDropoff']")
	private WebElement pickUpandDrop;

	@FindBy(id = "e0phone")
	WebElement BUCphone1;

	@FindBy(id = "e0addressLine1")
	WebElement adress1;

	@FindBy(id = "e0relationshipWithChild")
	WebElement relationshipWithChild1;

	@FindBy(id = "e1phone")
	WebElement BUCphone2;

	@FindBy(id = "e1addressLine1")
	WebElement address2;

	@FindBy(id = "e1relationshipWithChild")
	WebElement relationshipWithChild2;

	@FindBy(xpath = ".//*[@id='caregiverInfoForm']/div[2]/button")
	WebElement BUCNext;

	@FindBy(id = "email")
	WebElement emailDetails;

	@FindBy(id = "password")
	WebElement passwordDetails;

	public void NextButton() {
		NextButton.click();
	}

	public void clickOnIncenter() {
		clickOnIncenter.click();
	}

	public void clickOnInHome() {
		clickOnInHome.click();
	}

	public boolean backUpCareOptions() {
		boolean c = false;
		int q = getRandomInteger(1, 2);
		if (q == 1) {
			clickOnInHome.click();
			c = true;

		}
		if (q == 2) {
			clickOnIncenter.click();
		}
		return c;
	}

	public void loginDetails(String email) {
		emailDetails.sendKeys(email);
		passwordDetails.sendKeys("letmein1");

	}

	@FindBy(id = "freeTrial")
	WebElement freeTrial;

	@FindBy(name = "DOBMonth")
	WebElement DOBMonth;

	@FindBy(name = "DOBDay")
	WebElement DOBDate;

	@FindBy(name = "DOBYear")
	WebElement DOBYear;

	@FindBy(xpath = "(//*[@name='email'])[2]")
	WebElement DTEmail;

	@FindBy(xpath = "(//*[@name='password'])[2]")
	WebElement DTPassword;

	@FindBy(xpath = "//button[contains(text(), 'Log in')]")
	WebElement LoginButton;

	@FindBy(xpath = "//*[@class='btn btn-flat btn-secondary btn-wide sendMessage boldBtn sendMessage']")
	WebElement contactSitter;

	@FindBy(id = "msgSubject")
	WebElement msgSubject;

	@FindBy(id = "mpTextarea")
	WebElement textArea;

	@FindBy(xpath = "//*[@class='btn btn-flat btn-secondary']")
	WebElement searchButton;

	@FindBy(xpath = "(//*[contains(text(), 'Apply Now')])[1]")
	WebElement applyNow;

	@FindBy(id = "apply_now_anim")
	WebElement ApplyNow;

	@FindBy(xpath = "//a[contains(@class,'join-btn pro-join-now')]")
	private WebElement joinFreeToGetStarted;

	@FindBy(id = "accountNumber")
	private WebElement accNumber;

	@FindBy(id = "cvv")
	WebElement cVV;

	public void cVV() {
		cVV.sendKeys("123");
	}

	public void accNumber() {
		accNumber.sendKeys("4111111111111111");
	}

	public void clickOnJoinFreeToGetStarted() {
		joinFreeToGetStarted.click();
	}

	@FindBy(xpath = "//input[@id='accountNumber']")
	private WebElement ccNumber;

	@FindBy(xpath = "//input[@id='cvv']")
	private WebElement cvvNumber;

	@FindBy(xpath = "//input[@name='cardType' and @value='Visa']")
	private WebElement cardType1;

	@FindBy(xpath = "//*[@name='expMonth']")
	private WebElement expMonth;

	@FindBy(xpath = "//*[@name='expYear']")
	private WebElement expYear;
	
	@FindBy(xpath = "//*[@id='cardNumber']")
	private WebElement creditNumber;
	

	@FindBy(xpath = ".//*[@id='expDate']")
	private WebElement creditcardExpDate;

	@FindBy(xpath = "//*[@id='upgradeBtn']")
	private WebElement submitUpgrade;

	@FindBy(xpath = "//div[contains(text(),'Enter payment')]")
	private WebElement paymentTab;

	@FindBy(linkText = "continue with limited access for free")
	private WebElement continueWithLimitedAccessFree;

	@FindBy(xpath = ".//*[@id='membership-options-continue']")
	private WebElement membershipOptionContinue;

	@FindBy(linkText = "Continue")
	private WebElement continu;

	@FindBy(xpath = "//*[@type='submit']")
	private WebElement TypeSubmit;

	public void typeSubmit() {
		TypeSubmit.click();
	}

	public void membershipOptionContinue() {
		membershipOptionContinue.click();
	}

	public void continueWithLimitedAccessFree() {
		continueWithLimitedAccessFree.click();
	}

	public void myAccountSelectExpirationMonth() {
		myAccountSelectExpirationMonth.click();
	}

	public void myAccountSelectExpirationYear() {
		myAccountSelectExpirationYear.click();
	}

	public void ApplyNow() {
		ApplyNow.click();
	}

	public void applyNow() {
		applyNow.click();
	}

	public void searchButton() {
		searchButton.click();
	}

	public void textArea(String string) {
		textArea.sendKeys("string");
	}

	public void msgSubject(String stringText) {
		msgSubject.sendKeys("stringText");
	}

	public void contactSitter() {
		contactSitter.click();
	}

	public void LoginButton() {
		LoginButton.click();
	}

	public void DTPassword(String password) {
		DTPassword.sendKeys(password);
	}

	public void DTEmail(String username) {
		DTEmail.sendKeys(username);
	}

	public void DateOfBirth() {
		int p = getRandomIndex(1, 12);
		int q = getRandomIndex(1, 19);
		int r = getRandomIndex(28, 36);
		selectByIndex(DOBMonth, p);
		selectByIndex(DOBDate, q);
		selectByIndex(DOBYear, r);
	}

	@FindBy(xpath="//*[@id='dateOfBirth']")
	private WebElement DOB;
	
	public void DOB(){
		DOB.sendKeys("04101989");
	}
	
	@FindBy(xpath="//*[@id='wizardNextStep']/div[2]/a")
	private WebElement SkipAddOn;
	
	public void SkipAddOn(){
		SkipAddOn.click();
	}
	
	@FindBy(xpath="//*[@id='subscriptionFeatureAddOnForm']/div/div/div[4]/div[2]/a")
	private WebElement JobNoThanks;
	
	public void JobNoThanks(){
		JobNoThanks.click();
	}
	
	@FindBy(xpath="//*[@id='wizardNextStep']/div/div/a")
	private WebElement Continuee;
	
	public void Continuee(){
		Continuee.click();
	}
	
	public void expiration() {
		int p = getRandomIndex(5, 12);
		int q = getRandomIndex(5, 16);
		driver.findElement(By.xpath(".//*[@id='billingInfo']/div/div[1]"
				+ "/div/div/div[4]/div[1]/div/div/div/div/div[2]/ul/li[" + p + "]")).click();
		driver.findElement(By.xpath(
				".//*[@id='billingInfo']/div/div[1]/div/div/div[4]/div[2]/div/div/div/div/div[2]/ul/li[" + q + "]"))
				.click();
	}

	public void freeTrial() {
		freeTrial.click();
	}

	public void kindOfNeeds() {
		int P = getRandomIndex(1, 3);
		int Q = getRandomIndex(4, 7);
		driver.findElement(By.xpath(".//*[@id='recurringCont']/div[1]/div/span[" + P + "]")).click();
		driver.findElement(By.xpath(".//*[@id='recurringCont']/div[1]/div/span[" + Q + "]")).click();
	}

	public void increment() {
		int P = getRandomIndex(1, 5);
		WebElement incri = driver.findElement(By.xpath("(//span[@class='increment'])[" + P + "]"));
		incri.click();
		incri.click();

	}

	public void BUCNext() {
		BUCNext.click();
	}

	public void relationshipWithChild2() {
		Select relation2 = new Select(relationshipWithChild2);
		relation2.selectByVisibleText("AUNT");
	}

	public void address2() {
		address2.sendKeys("united states");
	}

	public void BUCphone2() {
		BUCphone2.sendKeys("9875454562");
	}

	public void relationshipWithChild1() {
		Select relation1 = new Select(relationshipWithChild1);
		relation1.selectByVisibleText("AUNT");
	}

	public void adress1() {
		adress1.sendKeys("united states");
	}

	public void BUCphone1() {
		BUCphone1.sendKeys("8547654578");
	}

	public void relationToChild() {
		Select relation = new Select(relationToChild);
		relation.selectByVisibleText("AUNT");
	}

	public void EmergencyFirstName() {
		EmergencyFirstName.sendKeys("jhonsmit");
	}

	public void EmergencyFirstName1() {
		EmergencyFirstName1.sendKeys("coreyn");
	}

	public void EmergencyLastName() {
		EmergencyLastName.sendKeys("jhonsmit");
	}

	public void EmergencyLastName1() {
		EmergencyLastName1.sendKeys("sbbhd");
	}

	public void EmergencyPhone() {
		EmergencyPhone.sendKeys("9160622990");
	}

	public void EmergencyPhone1() {
		EmergencyPhone1.sendKeys("9246127825");
	}

	public void EmergencyAddressLine1() {
		EmergencyAddressLine1.sendKeys("Waltham street");
	}

	public void EmergencyAddressLine11() {
		EmergencyAddressLine11.sendKeys("Waltham street");
	}

	public void EmergencyAddressLine2() {
		EmergencyAddressLine2.sendKeys("waltham");
	}

	public void zipOfEmergency() {
		zipOfEmergency.sendKeys("02451");
	}

	public void zipOfEmergency1() {
		zipOfEmergency1.sendKeys("02451");
	}

	public void EmergencyAddressLine21() {
		EmergencyAddressLine21.sendKeys("waltham");
	}

	public void EmergencyRelationshipWithChild() {
		Select relation = new Select(EmergencyRelationshipWithChild);
		relation.selectByVisibleText("AUNT");
	}

	public void EmergencyRelationshipWithChild1() {
		Select relation = new Select(EmergencyRelationshipWithChild1);
		relation.selectByVisibleText("AUNT");
	}

	public void DoctorName() {
		DoctorName.sendKeys("jhon smith");
	}

	public void Doctorsphone() {
		Doctorsphone.sendKeys("9182982570");
	}

	public void LastPhyiscalExamDate() {
		LastPhyiscalExamDate.sendKeys("11112016");
	}

	public void HospitalName() {
		HospitalName.sendKeys("lms hospital");
	}

	public void HospitalAddressLine1() {
		HospitalAddressLine1.sendKeys("waltham street");
	}

	public void HospitalAddressLine2() {
		HospitalAddressLine2.sendKeys("waltham");
	}

	public void HospitalZip() {
		HospitalZip.sendKeys("02451");
	}

	public void InsuranceCompany() {
		InsuranceCompany.sendKeys("zogi company");
	}

	public void PolicyNumber() {
		PolicyNumber.sendKeys("11111111111111");
	}

	public void ConfirmRequest() {
		ConfirmRequest.click();
	}

	public void pickUpandDrop() {
		pickUpandDrop.click();
	}

	public void BUCphone() {
		BUCphone.sendKeys("9854754216");
	}

	public void formControl() {
		selectByIndex(formControl, 2);
	}

	public void yearsOfExperience() {
		selectByIndex(yearsOfExperience, 2);
	}

	public void logOuT() {
		logOuT.click();
	}

	public void carPooling() {
		carPooling.click();
	}

	public void infoSubmit() {
		infoSubmit.click();
	}

	public void weekends() {
		weekends.click();
	}

	public void getStarted1() {
		getStarted1.click();
	}

	public void question3() {
		selectByIndex(question3, 5);
	}

	public void question2() {
		selectByIndex(question2, 5);
	}

	public void question1() {
		selectByIndex(question1, 5);
	}

	public void skipForNow() {
		skipForNow.click();
	}

	public void yearOfGraduation() {
		yearOfGraduation.sendKeys("2015");
	}

	public void schoolName() {
		schoolName.sendKeys("Carmel English School");
	}

	public void babySitterProvider() {
		babySitterProvider.click();
	}

	public void iAmA() {
		iAmA.click();
	}

	public void BUCcredentials(String user) {
		BUCemail.sendKeys(user);
		BUCpassword.sendKeys("letmein1");
	}

	public void selectByIndex(WebElement ele, int p) {
		Select selectBox = new Select(ele);
		selectBox.selectByIndex(p);

	}

	
	 * public void myAccountSelectExpirationYear() { int p = getRandomIndex(5, 15);
	 * selectByIndex(myAccountSelectExpirationYear, p); }
	 * 
	 * public void myAccountSelectExpirationMonth() { int p = getRandomIndex(1, 12);
	 * selectByIndex(myAccountSelectExpirationMonth, p); }
	 
	public void BUCSubmit() {
		BUCSubmit.click();
	}

	public void BUCphoneNumber() {
		BUCphoneNumber.sendKeys("(844) 618-9735");
	}

	public void conTinue() {
		conTinue.click();
	}

	public void BUCzip() {
		int p = getRandomnumberIndex(1, 5);
		cityStateZip1.clear();
		cityStateZip1.sendKeys("0245" + p);
	}

	public void BUCfirstName(String fname) {
		firstName.sendKeys(fname);
	}

	public void BUCchildFirstName() {
		BUCchildFirstName.sendKeys("shansjnk");
	}

	public void BUCchildLasttName() {
		BUCchildLasttName.sendKeys("childckay");
	}

	public void BUCchildDOB() {
		BUCchildDOB.sendKeys("11112015");
	}

	public void selectBoy() {
		selectBoy.click();
	}

	public void ChildTransportation() {
		ChildTransportation.click();
	}

	public void isCarAvailable() {
		isCarAvailable.click();
	}

	public void isParkingAvailable() {
		isParkingAvailable.click();
	}

	public void isPublicTransportationAvailable() {
		isPublicTransportationAvailable.click();
	}

	public void comfortableWithPets() {
		comfortableWithPets.click();
	}

	public void BUClastName(String lname) {
		lastName.sendKeys(lname);
	}

	public void todayDropdown() {
		todayDropdown.click();
	}

	public void clickOnCalendar() {
		clickOnCalendar.clear();
		clickOnCalendar.sendKeys(getDateAfterMonth2());
	}

	public void clickOnNext() {
		clickOnNext.click();
	}

	public void clickOnBabyAllergies() {
		clickOnBabyAllergies.click();
	}

	public void clicknOnMedicationsToBeAdministered() {
		clicknOnMedicationsToBeAdministered.click();
	}

	public void clickOnAdditionalCareNeeds() {
		clickOnAdditionalCareNeeds.click();
	}

	public void clickOnMediaCapture() {
		clickOnMediaCapture.click();
	}

	public void clickOnNextButton() {
		clickOnNextButton.click();
	}

	public void nextButton() {
		nextButton.click();
	}

	public void clickOnChildCareUnavailable() {
		clickOnChildCareUnavailable.click();
	}

	public void selectCareCenterForm() {
		if (selectCareCenterForm.isDisplayed()) {
			selectCareCenterForm.click();
		}
	}

	public String getDateAfterMonth1() {
		Calendar calendar = Calendar.getInstance();
		Date date = new Date();
		calendar.add(Calendar.DATE, 30);
		date = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		return dateFormat.format(date);
	}

	public String getDateAfterMonth2() {
		Calendar calendar = Calendar.getInstance();
		Date date = new Date();
		calendar.add(Calendar.MONTH, 1);
		date = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		return dateFormat.format(date);
	}

	public void howSoonDoUNeedIt() {
		howSoonDoUNeedIt.click();
	}

	public void cityStateZip() {
		cityStateZip.clear();
		cityStateZip.sendKeys("02451");
	}

	*//**
	 * For filling enrollment form
	 *
	 * @param fName
	 * @param lName
	 * @param cityState
	 * @param email
	 *//*
	public void enrollmentForm(String fName, String lName, String address, String email) {
		firstNameWithId.sendKeys(fName);
		lastNameWithId.sendKeys(lName);
		addressLine1.sendKeys(address);
		emailId.sendKeys(email);
		pwd.sendKeys("letmein1");
		submitForEnroll.click();
	}

	public void postSeekerJob() {
		postSeekerJob.click();
	}

	public void childCareSeeker() {
		childCareSeeker.click();
	}

	public void homeCareSeeker() {
		homeCareSeeker.click();
	}

	public void petCareSeeker() {
		petCareSeeker.click();
	}

	public void seniorCareSeeker() {
		seniorCareSeeker.click();
	}

	public void nannyCareSeeker() {
		nannyCareSeeker.click();
	}

	public void tutoringSeeker() {
		tutoringSeeker.click();
	}

	public void specialNeedsSeeker() {
		specialNeedsSeeker.click();
	}

	public void afterSchoolSitter() {
		afterSchoolSitter.click();
	}
	
	public void dateNightSitter() {
		dateNightSitter.click();
	}

	public void careNeedsOn() {
		careNeedsOn.click();
	}

	*//**
	 * seeker click the Bg request link
	 *//*
	public void bgRequestOld() {
		bgRequestOld.click();
	}

	public void bgRequestMaguire() {
		bgRequestMaguire.click();
	}

	*//**
	 * seeker clicks the links for sending a request for provider Bgc access
	 *//*

	public void seekerBgaccess() {

		// click the radio button of provider initiated BGC

		providerBGC = driver.switchTo().activeElement()
				.findElement(By.xpath("//input[@type='radio' and @id='bgCheckType']"));

		providerBGC.click();

		WebElement asdd = driver.findElement(By.className("requestBGBigBtn"));
		asdd.findElement(By.tagName("button")).click();

		// Button Click for Request BackgroundCheck
		requestLink = driver.switchTo().activeElement().findElement(By.xpath("//div[@class='c']"));

		// requestLink.click();
		closeDialog.click();

	}

	public void nextStepPiap() {
		nextStepPiap.click();
	}

	public void cardExpMonth(String cMonth) {
		cardMonth.click();
		cardMonth.sendKeys(cMonth);
	}

	public void cardExpYear(String cYear) {
		cardYear.click();
		cardYear.sendKeys(cYear);
	}

	public void cardType(String cType) {
		cardType.click();
		cardType.sendKeys(cType);
	}

	public void cardType() {
		cardType1.click();
	}

	public void cardName(String cName) {
		cardName.sendKeys(cName);
	}

	public void cardNo(String cardNum) {
		cardNo.sendKeys(cardNum);
	}

	public void acceptPiap() {
		acceptPiap.click();
	}

	public void finishPiap() {
		finishPiap.click();
	}

	public void clickHere() {
		clickHere.click();
	}

	public void nameOnCard() {
		nameOnCard.click();
	}

	public void addChild() {
		addChild.click();
	}

	public void logoutPiap() {
		logout.click();
	}

	public void childFirstName(String fName) {
		childFirstName.sendKeys(fName);
	}

	public void childLastName() {
		childLastName.click();
	}

	public void nextStep() {
		nextStep.click();
	}

	public void upgradeButton() {
		upgradeButton.click();
	}

	public void PatternLibUpgradeNow() {
		PatternLibUpgradeNow.click();
	}

	public void addOnFeature() {
		addOnFeature.click();
	}

	public void publicTransport() {
		publicTransport.click();
	}

	public void serviceUsedBefore() {
		serviceUsedBefore.click();
	}

	public void workPhone() {
		workPhone.click();
	}

	public void homePhone() {
		homePhone.sendKeys(CommonUtil.generate10DigitNumeric());
	}

	public void ccMonth() {
		creditCardMonth.findElement(new ByCssSelector("span.arrow")).click();
	}

	public void ccMonthSelect() {
		creditCardMonth.findElement(new ByCssSelector("li.item.selectItemInd5")).click();
	}

	public void ccYear() {
		creditCardYear.findElement(new ByCssSelector("span.arrow")).click();
	}

	public void ccYearSelect() {
		creditCardYear.findElement(new ByCssSelector("li.item.selectItemInd4")).click();
	}

	public void joinNowPiap() {
		joinNowPiap.click();
	}

	public void continuePiap() {
		continuePiap.click();
	}

	public void getStartedClick() {
		getStart.click();
	}

	public void fillRegForm() {
		firstName.sendKeys("SeekerfirstName" + CommonUtil.generateRandomString());
		lastName.sendKeys("SeekerlastName" + CommonUtil.generateRandomString());
		addressLine1.sendKeys("24 dohlenweg");
		cityStateZIP.sendKeys("Waltham, MA 02451");
		emailPiap.sendKeys("childCareSeeker" + CommonUtil.getUniquenumber() + "@care.com");
		pwdPiap.sendKeys("letmein1");

	}

	public void rightNow() {
		rightNow.click();
	}

	public void nanniesBabies() {
		nanniesBabies.click();
	}

	public void myKids() {
		myKids.click();
	}

	public void findCare() {
		findCare.click();
	}

	public void fillSignUPForm() {
		firstName.sendKeys("SeekerfirstName");
		lastName.sendKeys("Seekerlastname");
		addressLine1.sendKeys("24 dohlenweg");
		// email.sendKeys(emailID);
		password.sendKeys("letmein1");
		cityStateZIP.sendKeys("Waltham, MA 02451" + Keys.ENTER);
	}

	public void enterEmail(String emailId) {
		email.sendKeys(emailId);
	}

	public void emailId(String email) {
		emailId.sendKeys(email);
	}

	public void submit() {
		if (submitButton.isDisplayed()) {
			submitButton.click();
		}
	}

	public void upgradeButton1() {
		
		
	}
	public void twoKids() {
		twoKids.click();
	}

	public void howOld() {
		howOld.click();
	}

	public void submitKaroo() {
		karooContinueBtn.click();
	}

	public void submitNext() {
		submit.click();
	}

	public void childCareMainMenu() {
		childCareMainMenu.click();
	}

	public void childCareButton() {
		childCareButton.click();
	}

	public void childCareSubService() {
		childCareSubService.click();
	}

	public void selectTuesday() {
		selectTuesday.click();
	}

	public void selectWednesday() {
		selectWednesday.click();
	}

	public void selectFriday() {
		selectFriday.click();
	}

	public void zeroToSixMonths() {
		zeroToSixMonths.click();
	}

	public void clickOnLightHousekeeping() {
		clickOnLightHousekeeping.click();
	}

	public void clickOnMealPreparation() {
		clickOnMealPreparation.click();
	}

	public void clickOnContinue() {
		clickOnContinue.click();
	}

	*//**
	 * ChildCare
	 *//*
	public void clickOnChildCare() {
		childCare.click();
	}

	*//**
	 * SeniorCare
	 *//*
	public void clickOnSeniorCare() {
		seniorCare.click();
	}

	*//**
	 * House Keepig
	 *//*
	public void clickOnHouseKeeping() {
		houseKeeping.click();
	}

	*//**
	 * PetCare
	 *//*
	public void clickOnPetCare() {
		petCare.click();
	}

	*//**
	 * Tutoring
	 *//*
	public void clickOnTutoring() {
		tutoring.click();
	}

	public void dateNightJob() {
		dateNight.click();
	}

	*//**
	 * DayCare
	 *//*
	public void dayCareCenter() {
		dayCareCenter.click();
	}

	public void dayCareCenterseeker() {
		dayCareCenterseeker.click();
	}

	public void birthMonth(String value) {
		System.out.println("---- value---" + value);
		setSelectedField(birthMonth, value);
	}

	public void birthYear(String value) {
		System.out.println("--- value--" + value);
		setSelectedField(birthYear, value);
	}

	public void daysNeeded() {
		for (int i = 0; i < 4; i++) {
			int index = getRandomnumberIndex(1, 4);
			daysNeeded = driver.findElement(By.xpath(daysNeededPath + index + "']"));
			isSelected(daysNeeded);
		}
	}

	public void enrollmentMonth(String value) {
		System.out.println("--- value--" + value);
		setSelectedField(enrollmentMonth, value);
	}

	public void phone() {
		phone.sendKeys("8989898989");
	}

	public void AdditionalComments() {
		comments.sendKeys("additional comments added");
	}

	public void sendMyInfo() {
		sendMyInfo.click();

	}

	public void continuetoCare() {
		continuetoCare.click();
	}

	*//**
	 * Method to select indexes randomly .
	 *
	 * @param minimum
	 * @param maximum
	 * @return index
	 *//*

	private int getRandomnumberIndex(int minimum, int maximum) {
		int index = 0;
		for (int i = 0; i < 1; i++) {
			int min = minimum;
			int max = maximum;
			index = ((int) (Math.random() * (max - min + 1)) + min);
		}
		return index;
	}

	*//**
	 * Special Needs
	 *//*
	public void clickOnSpecialNeeds() {
		specialNeeds.click();
	}

	*//**
	 * How soon care required
	 *//*
	public void withinDays() {
		withindays.click();
	}

	public void recurring() {
		recurring.click();
	}

	public void monday() {
		monDay.click();
	}

	public void thursday() {
		thursDay.click();
	}

	*//**
	 * Timings
	 *
	 * @param value
	 *//*
	public void selectFrom() {
		setSelectedField(startingJobTime, "EM");
	}

	public void selectTo() {
		setSelectedField(endingJobTime, "EE");
	}

	*//**
	 * Hourly charge
	 *
	 * @param value
	 *//*
	public void selectHourlyChargeMin(String value) {
		setSelectedField(hourlyRateFrom, value);
	}

	public void selectHourlyChargeMax(String value) {
		setSelectedField(hourlyRateTo, value);
	}

	public void enterDate3() {
		if (driver.findElement(By.name("postJobUntil")).isDisplayed()) {
			enterDate3.clear();
			enterDate3.sendKeys(getDateAfterMonth1());
		}
	}

	public void jobZip() {
		jobZip.sendKeys(getRandomZipNotInGBBSTZ());
	}

	public void jobZip(String zip) {
		jobZip.clear();
		jobZip.sendKeys(zip);
	}

	public void selectNumberOfChildren(String value) {
		setSelectedField(numberOfChildren, value);
	}

	public void ageOfChildrens() {
		upto6Months.click();
		months7To3Years.click();
		years4To6.click();
		years7To11.click();
	}

	*//**
	 * Type of Care : babysitter
	 *//*
	public void babySitter() {
		babysitter.click();
	}

	public void myKidLoves() {
		sports.click();
		games.click();
		music.click();
		artsAndCrafts.click();
	}

	*//**
	 * gender
	 *//*
	public void selectGenderF() {
		female.click();
	}

	public void selectGenderM() {
		male.click();
	}

	public void dateOfBirth(String month, String day, String year) {
		setSelectedField(monthOfBirth, month);
		setSelectedField(dayOfBirth, day);
		setSelectedField(yearOfBirth, year);
	}

	*//**
	 * Type Of Care by dropDown
	 *//*
	public void typeOfCareLookingFor() {
		setSelectedField(typeOFCareLookingFor, "CHLDCGTYP003");
	}

	*//**
	 * list selection of date of birth
	 *//*
	public void dateOfBirthListSelection() {
		int p = getRandomIndex(2, 12);
		selectFromDropDown(monthDropdownList, p);
		int p1 = getRandomIndex(2, 28);
		selectFromDropDown(dateDropDownList, p1);
		int p2 = getRandomIndex(2, 113);
		if (p2 < 30) {
			p2 = 30;
		}
		selectFromDropDown(yearDropDownList, p2);
	}

	public void dateOfBirthDropDown(String monthName, String day1, String year1) {
		setSelectedField(month, monthName);
		setSelectedField(day, day1);
		setSelectedField(year, year1);
	}

	public void myJobTime() {
		myWorkTime_fullTime.click();
	}

	public void myPartnerJobTime() {
		partnerJobTime_FullTime.click();
	}

	// about your family
	public void aboutFamily() {
		children.click();
		specialNeedsCheckBox.click();
		petsCheckBox.click();
	}

	*//**
	 * Aged parents
	 *//*
	public void ageingParents() {
		children.click();
		specialNeedsCheckBox.click();
		petsCheckBox.click();
		militaryFamily.click();
		ageingParents.click();
	}

	*//**
	 * Aged Parents
	 *//*
	public void agedParents() {
		ageingParents.click();
	}

	*//**
	 * Military Services
	 *//*
	public void militaryFamily() {
		militaryFamily.click();
	}

	*//**
	 * Select 3 months subscription1 plan
	 *//*
	public void threeMonths() {
		subscriptionPlan_3months.click();
	}

	*//**
	 * 3 months Plan
	 *//*
	public void threeMonthsPaln() {
		threeMonthsPlan.click();
	}

	*//**
	 * Select 3 months subscription plan
	 *//*
	public void pricing3MonthsPlan() {
		pricingPlanFor3Months.click();
	}

	public void subscriptionPlan() {
		three_Months_Plan.click();
	}

	public void enterPayment() {
		enterPayment.click();
	}

	*//**
	 * First Name1 to display1 on the credit card1
	 *
	 * @param fNameOnCard
	 *//*
	public void firstNameOnCard(String fNameOnCard) {
		firstNameOnCard.clear();
		firstNameOnCard.sendKeys(fNameOnCard);
	}

	*//**
	 * Last Name to display on Credit card
	 *
	 * @param lNameOnCard
	 *//*
	public void lastNameOnCard(String lNameOnCard) {
		lastNameOnCard.clear();
		lastNameOnCard.sendKeys(lNameOnCard);
	}

	*//**
	 * Generates unique credit card number
	 *//*

	public void cardNumberVisa() {
		cardNumber.sendKeys(CreateCreditCard.getCreditCard(CreditCardType.VISA));
	}

	public void typeOfCreditCard() {
		setSelectedField(typeOfCreditCard, "Visa");
	}

	*//**
	 * Generates unique credit card number
	 *//*

	public void cardNumber() {
		String card = null;
		switch (cnum) {

		case 1:

			cardNumber.sendKeys(CreateCreditCard.getCreditCard(CreditCardType.VISA));
			card = "Visa";
			creditCardType = driver.findElement(By.xpath(cardPath + card + "')]"));
			if (creditCardType.isDisplayed()) {
				creditCardType.click();
			}
			break;

		case 2:

			cardNumber.sendKeys(CreateCreditCard.getCreditCard(CreditCardType.MASTERCARD));
			card = "MasterCard";
			creditCardType = driver.findElement(By.xpath(cardPath + card + "')]"));
			if (creditCardType.isDisplayed()) {
				creditCardType.click();
			}
			break;

		case 3:

			cardNumber.sendKeys(CreateCreditCard.getCreditCard(CreditCardType.AMEX));
			card = "American Express";
			creditCardType = driver.findElement(By.xpath(cardPath + card + "')]"));
			if (creditCardType.isDisplayed()) {
				creditCardType.click();
			}
			break;

		case 4:

			cardNumber.sendKeys(CreateCreditCard.getCreditCard(CreditCardType.DISCOVER));
			card = "Discover";
			creditCardType = driver.findElement(By.xpath(cardPath + card + "')]"));
			if (creditCardType.isDisplayed()) {
				creditCardType.click();
			}
			break;

		}

	}
	
	public void ccNumber() {
		creditNumber.sendKeys("4111111111111111");
	}
	
	
	public void creditcardExpDate() {
		creditcardExpDate.sendKeys("022025");
	}


	public void cardNumberMC() {
		cardNumber.sendKeys(CreateCreditCard.getCreditCard(CreditCardType.MASTERCARD));
	}

	*//**
	 * @param cvv1
	 *//*
	public void cvv() {
		cvv.sendKeys("123");
	}

	public void cvvText() {

		switch (cnum) {

		case 1:
			cvv.sendKeys("123");
			break;

		case 2:
			cvv.sendKeys("123");
			break;

		case 3:
			cvv.sendKeys("1234");
			break;

		case 4:
			cvv.sendKeys("123");
			break;

		}
	}

	
	public void creditCardMonthAndYear() {
		driver.findElement(By.xpath("//div[@class='sectionBody']/div[4]/div[1]/div/div"))
				.findElement(new ByCssSelector("span.arrow")).click();
		driver.findElement(By.xpath("//div[@class='sectionBody']/div[4]/div[1]/div/div"))
				.findElement(new ByCssSelector("li.item.selectItemInd5")).click();
		driver.findElement(By.xpath("//div[@class='sectionBody']/div[4]/div[2]/div/div"))
				.findElement(new ByCssSelector("span.arrow")).click();
		driver.findElement(By.xpath("//div[@class='sectionBody']/div[4]/div[2]/div/div"))
				.findElement(new ByCssSelector("li.item.selectItemInd4")).click();
	}

	public void selectExpirationMonth(String value) {
		expirationMonth.click();
		sleep(1200);
		driver.findElement(By.xpath("//li[text()='" + value + "']")).click();
		sleep(1000);
	}

	public void selectExpirationYear(String value) {
		expirationYear.click();
		sleep(1200);
		driver.findElement(By.xpath("//li[text()='" + value + "']")).click();
		sleep(1000);
	}

	public void billingZIP(String zip) {
		billingZIP.clear();
		billingZIP.sendKeys(zip);
	}

	public void billingStreetAddress() {
		billingStreetAddress.sendKeys("Waltham street");
	}

	public WebElement getbillingZIP() {
		return billingZIP;
	}

	public void phoneNumber(String phoneNum) {
		careConciergePhone.sendKeys(phoneNum);
	}

	public void checkForMail() {
		optinStateChecked.click();
	}

	public void logOut() {
		logOut.click();
	}

	public void skipThisStep() {
		skipThisStep.click();
	}

	public void skipLink() {
		skipLink.click();
	}

	public void servicesNeeded() {
		assistedLiving.click();
		adultDayHealth.click();
		transportationServices.click();
		independentLiving.click();
		homeCare.click();
		// generalSeniorCareServices.click();
	}

	public void seniorCareNeeds() {
		// input[@type='checkbox' and @value='SCSVCND001']
		String needsPath = "//input[@type='checkbox' and @value='SCSVCND00";
		for (int i = 0; i < 4; i++) {
			int needs = getRandomIndex(1, 8);
			driver.findElement(By.xpath(needsPath + needs + "']")).click();

		}
	}

	// Age Of SeniorCare
	public void seniCareAge() {
		// select[@name='attr-job.seniorCare.recipientAge']
		setSelectedField(seniorsAge, "SCNRAGE005");
	}

	// Age Of SeniorCare
	public void seniorRecipientAge(int x, int y) {

		seniorAgeRelation(1);
		Collegedegree(x, y);
	}

	public void seniorRelationShip(int x, int y) {

		seniorAgeRelation(2);
		Collegedegree(x, y);
	}

	public void seniorCareConcerns() {
		dementia.click();
		bathing.click();
		mobility.click();
		dressing.click();
	}

	public void primaryPhone(String num) {
		primaryPhone.sendKeys(num);
	}

	public void editPrimaryPhone(String num) {
		editPrimaryPhone.clear();
		editPrimaryPhone.sendKeys(num);
	}

	
	 * Select looking care type
	 
	public void selectCareType(String value) {
		setSelectedField(sitterService, value);
	}

	public void searchSitterVertical(String vertical) {

		switch (vertical) {

		case "CHILDCARE":
			setSelectedField(sitterService, "babysitter");
			break;

		case "PETCAREXX":
			setSelectedField(sitterService, "petCare");
			break;

		case "SENIRCARE":
			setSelectedField(sitterService, "seniorCare");
			break;

		case "SPCLNEEDS":
			setSelectedField(sitterService, "specialNeeds");
			break;

		case "HOUSEKEEP":
			setSelectedField(sitterService, "housekeeping");
			break;

		case "TUTORINGX":
			setSelectedField(sitterService, "tutoring");
			break;

		case "CAREGIGSX":
			setSelectedField(sitterService, "careGigs");
			break;

		default:
			setSelectedField(sitterService, "babysitter");
			break;

		}

	}

	public void searchJobVertical(String vertical) {

		switch (vertical) {

		case "CHILDCARE":
			setSelectedField(sitterService, "childCareJob");
			break;

		case "PETCAREXX":
			setSelectedField(sitterService, "petCareJob");
			break;

		case "SENIRCARE":
			setSelectedField(sitterService, "seniorCareJob");
			break;

		case "SPCLNEEDS":
			setSelectedField(sitterService, "specialNeedsJob");
			break;

		case "HOUSEKEEP":
			setSelectedField(sitterService, "housekeepingJob");
			break;

		case "TUTORINGX":
			setSelectedField(sitterService, "tutoringJob");
			break;

		case "CAREGIGSX":
			setSelectedField(sitterService, "careGigsJob");
			break;

		default:
			setSelectedField(sitterService, "childCareJob");
			break;

		}

	}

	public void selectMilesToBeTravel(String value) {
		setSelectedField(milesFromZipCode, value);
	}

	public void zipCode(String zip) {
		zipCode.clear();
		zipCode.sendKeys(zip);
	}

	*//**
	 * To make favorite
	 *//*
	public void favoritesLink() {
		favorites.click();
	}

	*//**
	 * Click on Care.com Site Logo
	 *//*
	public void siteLogo() {
		siteLogo.click();
	}
	
	public void navLogo(){
		logo.click();
	}

	*//**
	 * Click BackUpCare Link
	 *//*
	public void backUpCare() {
		myCareOnCall.click();
	}

	*//**
	 * Click MyJobs Link
	 *//*
	public void myJobs() {
		myJobs.click();
	}

	*//**
	 * Service Id
	 *
	 * @param value
	 *//*
	public void selectServiceId(String value) {
		setSelectedField(serviceId, value);
	}

	*//**
	 * To send messages to favorites
	 *//*
	public void favoriteCheckBox() {
		sendToFavorites.click();
	}

	*//**
	 * Message title
	 *
	 * @param title1
	 *//*
	public void messageTitle(String title1) {
		title.sendKeys(title1);
	}

	public void availableTimings() {
		for (int i = 0; i < 15; i++) {
			int min = 45;
			int max = 51;
			int p = ((int) (Math.random() * (max - min + 1)) + min);
			checkbox = driver.findElement(By.xpath(checkvalue + p + "']/input"));
			checkbox.click();
		}
	}

	public void myKidLove() {
		String myKidLovePath = "//input[@type='checkbox' and @value='CCKIDLVDG00";
		for (int i = 0; i < 4; i++) {
			int value = getRandomIndex(1, 5);
			driver.findElement(By.xpath(myKidLovePath + value + "']")).click();
		}
	}

	*//**
	 * Message description
	 *
	 * @param description1
	 *//*
	public void messageDesc(String description1) {
		description.sendKeys(description1);
	}

	public void selectFromDateAndTime(String date1) {
		enterDate0.sendKeys(date1);
		oneTimeJobStartTime.sendKeys("9");
		setSelectedField(oneTimeJobStartAM_PM, "AM");
	}

	public void selectToDateAndTime(String date1) {
		enterDate1.clear();
		enterDate1.sendKeys(date1);
		oneTimeJobEndTime.sendKeys("6");
		setSelectedField(oneTimeJobEndAM_PM, "PM");
	}

	public void clickLoginLink() {
		login.click();
	}

	// Login as Seeker
	public void seekerCredens(String user, String pwd) {
		email.clear();
		email.sendKeys(user);
		password.sendKeys("letmein1");
	}

	public void clickLoginButton() {
		loginButton.click();
	}

	public void clickLogin() {
		loginButton1.click();
	}

	*//**
	 * To send a message to provider
	 *//*
	public void sendMessage() {
		sendMessage.click();
	}

	public String getTomorrowsDate() {
		Calendar calendar = Calendar.getInstance();
		Date date = new Date();
		calendar.add(Calendar.DATE, 1);
		date = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		return dateFormat.format(date);
	}

	public String getDateAfterAWeek() {
		Calendar calendar = Calendar.getInstance();
		Date date = new Date();
		calendar.add(Calendar.DATE, 7);
		date = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		return dateFormat.format(date).toString();
	}

	public String getDateAfterMonth() {
		Calendar calendar = Calendar.getInstance();
		Date date = new Date();
		calendar.add(Calendar.DATE, 30);
		date = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		return dateFormat.format(date);
	}

	*//**
	 * Click on Edit Job link
	 *//*
	public void editJob() {
		editJob.click();
	}

	*//**
	 * Click on View Job link
	 *//*
	public void viewJob() {
		viewJob.click();
	}

	*//**
	 * Title to post a job
	 *//*
	public void postJobTitle() {
		postAJobTitle.clear();
		postAJobTitle.sendKeys("Child Care Seeker");
	}

	*//**
	 * Description
	 *//*
	public void postJobDesc() {
		postJobDescription.clear();
		postJobDescription.sendKeys("Child Care description");
	}

	*//**
	 * Used as StartJob time in EditMyJob when service selected as OneTime service
	 *
	 * @param date1
	 *//*
	public void oneTime(String date1, String date2) {
		enterDate0.clear();
		enterDate0.sendKeys(date1);
		editJobDate4.clear();
		editJobDate4.sendKeys(date2);
	}

	*//**
	 * Used as StartJob time in EditMyJob when service selected for recurring
	 *
	 * @param date1
	 *//*
	public void recurring(String date1, String date2) {
		editJobDate1.clear();
		editJobDate1.sendKeys(date1);
		editJobDate2.clear();
		editJobDate2.sendKeys(date2);
	}

	*//**
	 * Used as StartJob time in EditMyJob when service selected for occasional
	 *//*
	public void occasionalJobStart() {
		occasionalJobPostDate.clear();
		occasionalJobPostDate.sendKeys(getTodayDate());
	}

	public void enterDate1(String date1) {
		enterDate1.clear();
		enterDate1.sendKeys(date1);
	}

	public void enterDate2(String date1) {
		enterDate2.clear();
		enterDate2.sendKeys(date1);
	}

	public void renterDate1(String date1) {
		renterDate1.clear();
		renterDate1.sendKeys(date1);
	}

	public void renterDate2(String date1) {
		renterDate2.clear();
		renterDate2.sendKeys(date1);
	}

	// public void enterDate3(String date1) {
	// enterDate3.clear();
	// enterDate3.sendKeys(date1);
	// }

	public void enterDate4(String date1) {
		enterDate4.clear();
		enterDate4.sendKeys(date1);
	}

	public void education() {
		setSelectedField(educationLevel, "HIGH_SCHOOL_DEGREE");
	}

	public void addressLine1org() {
		addressLine1org.sendKeys("Waltham street");
	}

	public void addressLine1(String address) {
		addressLine1.clear();
		addressLine1.sendKeys(address);
	}

	public void fillInAddressLine1(String address) {
		addressLine1.sendKeys(address);
	}

	public WebElement getAddressLine1() {
		return addressLine1;
	}

	public void familyDes() {
		familyDescription.sendKeys("This is about my Family");
	}

	public void yesComfortWithPets() {
		comfortWithPets.click();
	}

	public void comfortePet() {
		farmAnimals.click();
		smallAnimals.click();
	}

	public void submitCareNeeds() {
		submitCareNeeds.click();
	}

	public void unCheckPublicProfile() {
		publicProfile.click();
	}

	public void submitPrefernceForm() {
		submitPreferenceForm.click();
	}

	*//**
	 * Click on GetStarted button to post a job today
	 *//*
	public void getStarted() {
		getStarted.click();
	}

	public void submitPostAJob() {
		centeredButton.click();
	}

	*//**
	 * for continue by selecting time and date
	 *//*
	public void submitBookADate() {
		// bookADateSubmit.click();
		driver.switchTo().frame(frame);
		bookADateSubmit.click();
	}

	*//**
	 * Children age group and number of children
	 *//*
	public void numberOfKidsWith12Yrs() {
		numberOf12YrsPlusChildren.sendKeys("1");
	}

	*//**
	 * Continue by updating children info
	 *//*
	public void submitNumberOfChildrenInfo() {
		submitNoOfKids.click();
	}

	*//**
	 * Select job includes check
	 *//*
	public void jobIncludes() {
		bedTime.click();
		playTime.click();
		bathTime.click();
		feedingDinner.click();
	}

	public void selectRateAndPostDateNightJob() {
		// driver.switchTo().frame("dnWidget");
		setSelectedField(paymentType, "CASH");
	}

	@FindBy(xpath = "//a[contains(text(), 'Month')]")
	private WebElement monthProvider;

	@FindBy(xpath = "//a[contains(text(), 'Day')]")
	private WebElement dayProvider;

	@FindBy(xpath = "//a[contains(text(), 'Year')]")
	private WebElement yearProvider;

	public void providerDOB(String month, String day, String year) {
		setSelectedField(monthProvider, month);
		setSelectedField(dayProvider, day);
		setSelectedField(yearProvider, year);
	}

	public void yourDOB(String month, String day, String year) {
		setSelectedField(dobMonth, month);
		setSelectedField(dobDay, day);
		setSelectedField(dobYear, year);
	}

	public void subjectsArea() {
		dance.click();
		engineering.click();
		math.click();
		business.click();
	}

	*//**
	 * Age of Care Recipients
	 *//*
	public void ageofCareRecipents() {
		// infant.click();
		// adult.click();
		// teen.click();
		// senior.click();
		// youth.click();

		String pathOfAgeGroups = "//input[@value='SNAGEGRUP00"; // 1']
		for (int i = 1; i < 5; i++) {
			driver.findElement((By.xpath(pathOfAgeGroups + i + "']"))).click();
		}
	}

	*//**
	 * To Select Care type like regular,occasional,one time.
	 *//*
	public boolean whenDoYouNeedCare() {
		boolean b = false;
		int p = getRandomIndex(1, 2);
		// needCare = driver.findElement(By.xpath(needCareXpath + p +
		// "]/input"));
		// isSelected(needCare);
		// if ((p == 1) | (p == 2)) {
		// b = true;
		// }
		// return b;

		if (p == 1) {
			driver.findElement(By.id("recurring")).click();
			b = true;
		}
		if (p == 2) {
			driver.findElement(By.id("oneTime")).click();
		}
		return b;
	}

	public void howSoonCareRequired() {
		int p = getRandomIndex(1, 5);
		howSoonCareNeeded = driver.findElement(By.xpath(careNeededXpath + p + "]//input"));
		isSelected(howSoonCareNeeded);
	}

	public void selectDay() {
		boolean usesV2Path = false;
		if (driver.findElements(By.xpath(selectDayXpathV2)).size() > 0) {
			usesV2Path = true;
		}
		for (int i = 0; i < 3; i++) {
			int p = getRandomIndex(1, 7);
			if (usesV2Path) {
				selectDay = driver.findElement(By.xpath(selectDayXpathV2 + "[" + p + "]"));
			} else {
				selectDay = driver.findElement(By.xpath(selectDayXpath + p + "]"));
			}
			isSelected(selectDay);
		}
	}

	public void selectDayV2() {
		for (int i = 0; i < 3; i++) {
			int p = getRandomIndex(1, 7);
			selectDay = driver.findElement(By.xpath(selectDayXpathV2 + p + "]"));
			isSelected(selectDay);
		}
	}

	public void jobTimings() {
		int p = getRandomIndex(2, 3);
		selectFromDropDown(jobStartTimeli, p);
		int p1 = getRandomIndex(4, 6);
		selectFromDropDown(jobEndTimeLi, p1);
	}

	public void oneTime() {
		// start date
		enterDate0.clear();
		enterDate0.sendKeys(getFutureDate(5));
		// time to start
		oneTimeJobStartTime.clear();
		int p2 = getRandomIndex(2, 6);
		String s1 = Integer.toString(p2);
		oneTimeJobStartTime.sendKeys(s1);
		// in am_pm
		// int p3 = getRandomIndex(2, 3);
		// selectFromDropDown(jobStartAm_pm, p3);
		// job Ending time
		enterDate4.clear();
		enterDate4.sendKeys(getFutureDate(20));
		// job end time
		oneTimeJobEndTime.clear();
		int p4 = getRandomIndex(6, 12);
		String s2 = Integer.toString(p4);
		oneTimeJobEndTime.sendKeys(s2);
		// in am-pm
		// int p5 = getRandomIndex(2, 3);
		// selectFromDropDown(jobStartAm_pm, p5);
	}

	public String getTodayDate() {
		Calendar calendar = Calendar.getInstance();
		Date date = new Date();
		date = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		return dateFormat.format(date);
	}

	public String getFutureDate(int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DATE, days);
		Date date = new Date();
		date = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		return dateFormat.format(date);
	}

	*//**
	 * To select random index
	 *
	 * @param mn
	 * @param mx
	 * @return
	 *//*
	private int getRandomIndex(int mn, int mx) {
		int p = 0;
		for (int i = 0; i < 1; i++) {
			int min = mn;
			int max = mx;
			p = ((int) (Math.random() * (max - min + 1)) + min);
		}
		return p;
	}

	private int getRandomInteger(int mn, int mx) {
		int q = 0;
		for (int i = 0; i < 1; i++) {
			int min = mn;
			int max = mx;
			q = ThreadLocalRandom.current().nextInt(min, max + 1);
		}
		return q;
	}

	*//**
	 * to check weather the element is selected or not
	 *//*
	private void isSelected(WebElement ele) {
		if (!(ele.isSelected())) {
			ele.click();
		}
	}

	*//**
	 * list selection of pay rate
	 *//*
	public void payRateList() {
		int p = getRandomIndex(2, 4);
		selectFromDropDown(minpayRateDropdown, p);
		int p1 = getRandomIndex(5, 9);
		selectFromDropDown(maxPayRateDropdow, p1);
	}

	public void houseKeepingServices() {
		int p = getRandomIndex(1, 3);
		houseKeepingServices = driver.findElement(By.xpath(hkServicesxpath + p + "']"));
		isSelected(houseKeepingServices);
	}

	// HouseKeeping Services Details
	public void houseKeepingServicesNeeded() {
		String houseKeepingPath = "//input[@type='checkbox' and @value='HKSERVCES00";
		int value = getRandomIndex(1, 21);
		driver.findElement(By.xpath(houseKeepingPath + value + "']")).click();
	}

	// Premium User HK Job page
	public void hkPremiumServices() {
		int p = getRandomIndex(1, 3);
		hkPremiumServices = driver.findElement(By.xpath(hkPremiumServicesxpath + p + "']"));
		isSelected(hkPremiumServices);
	}

	public void supplies() {
		int p = getRandomIndex(1, 2);
		String s1;
		if (p == 1)
			s1 = "true";
		else {
			s1 = "false";
		}
		supplies = driver.findElement(By.xpath(supliesXpath + s1 + "']"));
		isSelected(supplies);
	}

	public void availableAfterSchoolJob() {
		int p = getRandomIndex(1, 2);
		String s1;
		if (p == 1)
			s1 = "true";
		else {
			s1 = "false";
		}
		availableForAfterSchool = driver.findElement(By.xpath(availableForAfterSchoolXpath + s1 + "']"));
		isSelected(availableForAfterSchool);
	}

	public void availableBeforSchoolJob() {
		int p = getRandomIndex(1, 2);
		String s1;
		if (p == 1)
			s1 = "true";
		else {
			s1 = "false";
		}
		availableForBeforeSchool = driver.findElement(By.xpath(availableForBeforeSchoolXpath + s1 + "']"));
		isSelected(availableForBeforeSchool);
	}

	*//**
	 * summer care job
	 *//*
	public void summerCareJob() {
		int p = getRandomIndex(1, 2);
		String s1;
		if (p == 1)
			s1 = "true";
		else {
			s1 = "false";
		}
		summerCareJob = driver.findElement(By.xpath(summerCareJobXpath + s1 + "']"));
		isSelected(summerCareJob);
	}

	public void numberOfChildrens() {
		int p = getRandomIndex(2, 4);
		selectFromDropDown(numberOfChildrenDropdown, p);
	}

	public void selectSimulatorDays() {
		for (int i = 0; i < 3; i++) {
			int p = getRandomIndex(1, 7);
			selectSimulatorDay = driver.findElement(By.xpath(selectSimulatorDayXpath + p + "]"));
			isSelected(selectSimulatorDay);
		}
	}

	*//**
	 * Type of Care : babysitter
	 *//*
	public void typeOfCare() {
		for (int i = 0; i < 2; i++) {
			int p = getRandomIndex(1, 4);
			typeOfCare = driver.findElement(By.xpath(typeOfCare_xpath + p + "']"));
			isSelected(typeOfCare);
		}
	}

	*//**
	 * to select the job timings with slider
	 *//*
	public void timeSlider() {
		int p = getRandomIndex(1, 2);
		int i2 = (getRandomIndex(1, 17));
		int i1 = (i2 - 1) * 5;
		i2 = i2 * 5;
		int d1 = (getRandomIndex(1, 3));
		int d2 = (d1 + 1) * 5;
		d1 = d1 * 5;
		if (p == 1) {
			new Actions(driver).dragAndDropBy(timedraggable1, i2, 0).build().perform();
			new Actions(driver).dragAndDropBy(timedraggable, i1, 0).build().perform();
		} else {
			new Actions(driver).dragAndDropBy(timedraggable, -d1, 0).build().perform();
			new Actions(driver).dragAndDropBy(timedraggable1, -d2, 0).build().perform();
		}
	}

	public void petCareServicesNeededFor() {
		for (int i = 0; i < 5; i++) {
			int p = getRandomIndex(1, 2);
			servicesNeededfor = driver.findElement(By.xpath(petCareXpath + p + "']"));
			isSelected(servicesNeededfor);
		}
	}

	public void seniorCareEnrollneeds() {
		int p = getRandomIndex(1, 3);
		seniorCareEnrollneeds = driver.findElement(By.xpath(seniorCareEnrollXpath + p + "']"));
		isSelected(seniorCareEnrollneeds);
	}

	public void lastPagesubmit() {
		lastPageSubmit.click();
	}

	public void seniorCareservicesNeeded() {
		for (int i = 0; i < 3; i++) {
			int p = getRandomIndex(2, 8);
			if (p == 6)
				p = 2;
			seniorCareServices = driver.findElement(By.xpath(seniorCarePath + p + "']"));
			isSelected(seniorCareServices);
		}
	}

	*//**
	 * Age group
	 *//*
	public void whoNeedCareAgeIs() {
		int p = getRandomIndex(2, 9);
		selectFromDropDown(ageDropdown, p);
	}

	public void relationship() {
		int p = getRandomIndex(2, 5);
		selectFromDropDown(relationshipDropdown, p);
	}

	
	 * to select caregiver categery
	 
	public void typesOfSeniorCaregiver() {
		int p = getRandomIndex(1, 4);
		typesOfSeniorCaregiver = driver.findElement(By.xpath(seniorCaregiverXpath + p + "']"));
		isSelected(typesOfSeniorCaregiver);
	}

	
	 * to select senior services
	 
	public void typesOfSeniorServicesNeeded() {
		for (int i = 0; i < 4; i++) {
			int p = getRandomIndex(1, 8);
			seniorServices = driver.findElement(By.xpath(seniorServicesXpath + p + "']"));
			isSelected(seniorServices);
		}
	}

	*//**
	 * to select diagnosis of special needs
	 *//*
	public void selectDiagnosis() {
		// int p = getRandomIndex(2, 41);
		// selectFromDropDown(diagnosisDropdown, p);
		String pathOfChkBox = "//input[@value='SNSOCLEMO00";
		for (int i = 1; i < 5; i++) {
			driver.findElement(By.xpath(pathOfChkBox + i + "']")).click();
		}

	}

	public void splselectDiagnosis() {

		isSelected(spldiagnosisDropdown);
	}

	public void coOpPhone() {

		isSelected(coOpPhone);
	}

	*//**
	 * for selecting pay rate with slider
	 *//*
	public void paySlider() {
		int p = getRandomIndex(1, 2);
		int i2 = (getRandomIndex(1, 5));
		int i1 = (i2 - 1) * 30;
		i2 = i2 * 30;
		int d1 = (getRandomIndex(1, 2));
		int d2 = (d1 + 1) * 30;
		d1 = d1 * 30;
		if (p == 1) {
			new Actions(driver).dragAndDropBy(payDraggable1, i2, 0).build().perform();
			new Actions(driver).dragAndDropBy(payDraggable, i1, 0).build().perform();
		} else {
			new Actions(driver).dragAndDropBy(payDraggable, -d1, 0).build().perform();
			new Actions(driver).dragAndDropBy(payDraggable1, -d2, 0).build().perform();
		}
	}

	public void paySliderPAJ(int x, int y) {

		new Actions(driver).dragAndDropBy(payDraggable1, y, 0).build().perform();
		new Actions(driver).dragAndDropBy(payDraggable, x, 0).build().perform();

	}

	public void memberSegmentationDragDrop() {

		int a = getRandomIndex(1, 2);

		int b = getRandomIndex(3, 12);

		int c = getRandomIndex(13, 14);

		int d = getRandomIndex(15, 17);

		// Me
		seekerFamily = driver.findElement(By.xpath(seekerFamilyXPath + a + "']/div"));

		new Actions(driver).dragAndDropBy(family1, 548, 655).build().perform();

		// Partner
		seekerFamily = driver.findElement(By.xpath(seekerFamilyXPath + a + "']/div"));

		new Actions(driver).dragAndDropBy(seekerFamily, 481, 655).build().perform();

		// Children
		seekerFamily = driver.findElement(By.xpath(seekerFamilyXPath + b + "']/div"));

		new Actions(driver).dragAndDropBy(seekerFamily, 625, 655).build().perform();

		// Extended Family
		seekerFamily = driver.findElement(By.xpath(seekerFamilyXPath + c + "']/div"));

		new Actions(driver).dragAndDropBy(seekerFamily, 229, 655).build().perform();

		// Pets
		seekerFamily = driver.findElement(By.xpath(seekerFamilyXPath + d + "']/div"));

		new Actions(driver).dragAndDropBy(seekerFamily, 986, 721).build().perform();

	}

	*//**
	 * To Select number of kids with Counter
	 *//*
	public void numberOfKidsWithCounter() {
		String s1 = "]//span[@class='increment']";
		for (int i = 0; i < 3; i++) {
			int p = getRandomIndex(2, 5);
			jobDetails = driver.findElement(By.xpath(jobDetailsXpath + p + s1));
			isSelected(jobDetails);
		}
	}

	public void numberOfPetsWithCounter() {
		String s1 = "]//span[@class='increment']";
		for (int i = 0; i < 3; i++) {
			int p = getRandomIndex(2, 5);
			petDetails = driver.findElement(By.xpath(petDetailsXpath + p + s1));
			isSelected(petDetails);
		}
	}

	public void numberOfTransportWithCounter() {
		transportDetails.click();

	}

	public void equipment() {
		int p = getRandomIndex(1, 2);
		String s1;
		if (p == 1)
			s1 = "true";
		else {
			s1 = "false";
		}
		equipment = driver.findElement(By.xpath(equipmentXpath + s1 + "']"));
		isSelected(equipment);
	}

	// UploadPhoto
	public void uploadPhoto(String imageName) {
		String profilePhotosFolder = EnvProperties.getProfilePhotosFolder();
		String imageFile = profilePhotosFolder + File.separator + "seeker" + File.separator + imageName;
		photoPath.sendKeys(imageFile);
		try {
			Thread.sleep(10000L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void nextbtn() {
		nextbtn.click();
	}

	public void jobDays() {
		monDay.click();
		tuesDay.click();
		wednesDay.click();
		thursDay.click();
		friDay.click();
	}

	public void notyou() {
		notyou.click();
	}

	public void goButton() {
		goButton.click();
	}

	public void search(String sVertical, String sZip) {

		selectCareType(sVertical);
		zipCode.clear();
		zipCode.sendKeys(sZip);

		goButton();
	}

	public void myCaregivers() {
		myCaregivers.click();
	}

	public void myMessages() {
		myMessages.click();
	}
	
	public void plMsg(){
		msg.click();
	}
	
	public void messageThread(){
		latestMessage.click();
	}
	
	public void replyMessage(){
		reply.sendKeys("Hello provider");
	}

	public void reviewOption() {

		isSelected(reviewOption);
	}

	public void caregigServices() {
		errand.click();
		vacation.click();
		moving.click();
		shopping.click();
		parties.click();
	}

	public void jobTimeAll() {
		monAll.click();
		tueAll.click();
		wedAll.click();
		thuAll.click();
		friAll.click();
	}

	public void jobTimeIndividual() {

		satEm.click();
		satEe.click();
		sunEm.click();
		sunEe.click();
	}

	public void transportCar() {
		carProvided.click();
	}

	public void finishButton() {
		finishButton.click();
	}

	public void childLogo() {
		childLogo.click();
	}

	public void splLogo() {
		splLogo.click();
	}

	public void seniorLogo() {
		seniorLogo.click();
	}

	public void petLogo() {
		petLogo.click();
	}

	public void hkLogo() {
		hkLogo.click();
	}

	public void caregigLogo() {
		caregigLogo.click();
	}

	public void tutorLogo() {
		tutorLogo.click();
	}

	public void transportLogo() {
		transportLogo.click();
	}

	public void karooContinue() {
		karooContinue.click();
	}
	
	public void plJoin(){
		plJoinNow.click();
	}

	public void joinNow() {
		joinNow.click();
	}

	public void groupName(String sgroup) {
		groupName.sendKeys(sgroup);
	}

	public void groupShortName(String sgname) {
		groupShortName.sendKeys(sgname);
	}

	public void groupPublic() {
		groupPublic.click();
	}

	public void groupPrivate() {
		groupPrivate.click();
	}

	public void coopGroup() {
		coopGroup.click();
	}

	public void allowSitterContact() {
		allowSitterContact.click();
	}

	*//**
	 * Relationship With provider
	 *//*
	public void relationshipWithProvider() {
		new Actions(driver).moveToElement(relation).click().build().perform();
		int p = getRandomIndex(1, 4);
		relation = driver.findElement(By.xpath(relationPath + p + "]"));
		new Actions(driver).moveToElement(relationDropDown).click().build().perform();

	}

	*//**
	 * Method about Overall rating
	 *//*
	public void overallRating() {
		String[] ratings = new String[] { "one-star", "two-stars", "three-stars", "four-stars", "five-stars" };
		int p = getRandomIndex(0, 4);
		WebElement star = driver.findElement(By.className(ratings[p]));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click();", star);
		
		 * for (int i = 1; i <= p; i++) { overallRating =
		 * driver.findElement(By.xpath(overallRatingPath + p + "]/a")); new Actions
		 * (driver).moveToElement(overallRating).click().build().perform(); }
		 
	}

	*//**
	 * Methods about Micro Attributes
	 *//*
	public void punctualityMicroAttribute() {
		int p = getRandomIndex(1, 2);
		punctuality = driver.findElement(By.xpath(punctualityPath + p + "]"));
		new Actions(driver).moveToElement(punctuality).click().build().perform();

	}

	public void reliableTransportationMicroAttribute() {
		int p = getRandomIndex(1, 2);
		reliableTransportation = driver.findElement(By.xpath(reliableTransportationPath + p + "]"));
		new Actions(driver).moveToElement(reliableTransportation).click().build().perform();

	}

	public void dependabilityMicroAttribute() {
		int p = getRandomIndex(1, 2);
		dependability = driver.findElement(By.xpath(dependabilityPath + p + "]"));
		new Actions(driver).moveToElement(dependability).click().build().perform();

	}

	public void accurateCalMicroAttribute() {
		int p = getRandomIndex(1, 2);
		accurateCalendar = driver.findElement(By.xpath(accurateCalendarPath + p + "]"));
		new Actions(driver).moveToElement(accurateCalendar).click().build().perform();

	}

	public void wouldyouhireagain() {

		int p = getRandomIndex(1, 2);
		hireagain = driver.findElement(By.xpath(hireagainPath + p + "]"));
		new Actions(driver).moveToElement(hireagain).click().build().perform();

	}

	*//**
	 * method about write a review comment
	 *//*

	public void reviewComment() {
		reviewComment.sendKeys("This provider is really really good. She takes special care in selecting proper "
				+ "activities for the kids appropriate to their age group. She is very patient as well. "
				+ "I am writing this review from an automated script." + " " + CommonUtil.generateRandomString());
	}

	public void submitReview() {
		submitReview.click();
	}

	*//**
	 * Click on send a Message link
	 *//*
	public void messageLink() {
		messageLink.click();
	}

	public void maguireMessageLink() {
		maguireMessageLink.click();
	}

	public void maguireMessageContact() {
		maguireMessageContact.click();
	}

	*//**
	 * method about message subject
	 *//*

	public void messageSubject() {

		messageSubject.sendKeys("hi provider" + " " + CommonUtil.generateRandomString());
	}

	public void maguireSubject() {

		maguireSubject.sendKeys("hi provider" + " " + CommonUtil.generateRandomString());
	}

	*//**
	 * method about message subject
	 *//*

	public void messageBody() {
		messageBody.sendKeys("i am interest about services " + " " + CommonUtil.generateRandomString());
	}

	*//**
	 * click send message button
	 *//*
	public void clickSendMessage() {
		clickSendMessage.click();
	}

	// homebase

	@FindBy(name = "nickName")
	private WebElement nickName;

	public void homeBaseForm() {
		nickName.sendKeys("HomeBasemember");
		firstName.sendKeys("HomeBasefirstName");
		lastName.sendKeys("HomeBaselastname");
		addressLine1.sendKeys("24 dohlenweg");
		cityStateZIP.sendKeys("Waltham, MA 02451");
		primaryPhone.sendKeys("1122112211");

	}

	@FindBy(xpath = ".//span[contains(text(),'Gender')]|.//a[contains(text(),'Gender')]")
	private WebElement genderdropdown;

	public void Gender() {
		int p = getRandomIndex(2, 3);
		selectFromDropDown(genderdropdown, p);
	}

	@FindBy(name = "mobileNumber")
	private WebElement mobileNumber;

	public void mobileNumber(String num) {
		mobileNumber.clear();
		mobileNumber.sendKeys(num);
	}

	@FindBy(name = "shareUpdatesName")
	private WebElement shareUpdatesName;

	public void shareUpdatesName(String num) {
		shareUpdatesName.clear();
		shareUpdatesName.sendKeys(num);
	}

	@FindBy(name = "shareUpdatesEmail")
	private WebElement shareUpdatesEmail;

	public void shareUpdatesEmail(String num) {
		shareUpdatesEmail.clear();
		shareUpdatesEmail.sendKeys(num);
	}

	@FindBy(className = "myHomeBase")
	private WebElement homeBaselnk;

	public void homeBaselnk() {
		homeBaselnk.click();
	}

	@FindBy(xpath = "//a[contains(text(),'This service is for')]")
	private WebElement serviceFor;

	public void ServiceFor() {
		selectFromDropDown(serviceFor, 2);
	}

	@FindBy(xpath = "//a[contains(text(),'Location')]")
	private WebElement location;

	public void Location() {
		selectFromDropDown(location, 2);
	}

	@FindBy(name = "name")
	private WebElement locNickName;

	public void locNickName() {
		locNickName.sendKeys("locnickname");
	}

	@FindBy(name = "address1")
	private WebElement locAddress;

	public void locAddress() {
		locAddress.sendKeys("locaddress");
	}

	@FindBy(id = "enterDateTimeOfCare")
	private WebElement locCareDate;

	public void locCareDate() {
		locCareDate.sendKeys(getDateAfterAWeek());
	}

	@FindBy(name = "dropOffDate")
	private WebElement dropOffDate;

	public void dropOffDate() {
		dropOffDate.sendKeys(getDateAfterMonth1());
	}

	@FindBy(name = "pickUpDate")
	private WebElement pickUpDate;

	public void pickUpDate() {
		pickUpDate.sendKeys(getDateAfterAWeek());
	}

	@FindBy(xpath = "//a[contains(text(),'12:00 AM')]")
	private WebElement locTime;

	public void locTime() {
		int p = getRandomIndex(2, 24);
		selectFromDropDown(locTime, p);
	}

	@FindBy(xpath = "//a[contains(text(),'Schedule/Frequency')]")
	private WebElement locSchedule;

	public void locSchedule() {
		int p = getRandomIndex(2, 4);
		selectFromDropDown(locSchedule, p);
	}

	private WebElement hbPersonalCare;
	private String hbPersonalCare_xpath = "//input[@type='checkbox' and @value='HMBSPCNDS00";

	public void hbPersonalCare() {
		for (int i = 0; i < 2; i++) {
			int p = getRandomIndex(1, 4);
			hbPersonalCare = driver.findElement(By.xpath(hbPersonalCare_xpath + p + "']"));
			isSelected(hbPersonalCare);
		}
	}

	private WebElement hbCompanionCare;
	private String hbCompanionCare_xpath = "//input[@type='checkbox' and @value='HMBSOTHSR00";

	public void hbCompanionCare() {
		for (int i = 0; i < 2; i++) {
			int p = getRandomIndex(1, 4);
			hbCompanionCare = driver.findElement(By.xpath(hbCompanionCare_xpath + p + "']"));
			isSelected(hbCompanionCare);
		}
	}

	private WebElement hbMedication;
	private String hbMedication_xpath = "//input[@type='checkbox' and @value='HMBSMCNDS001']";

	public void hbMedication() {
		hbMedication = driver.findElement(By.xpath(hbMedication_xpath));
		isSelected(hbMedication);

	}

	private WebElement hbAddlInfo;
	private String hbAddlInfo_xpath = "//input[@type='checkbox' and @value='HMBSTRNDS00";

	public void hbAddlInfo() {
		for (int i = 0; i < 2; i++) {
			int p = getRandomIndex(1, 4);
			hbAddlInfo = driver.findElement(By.xpath(hbAddlInfo_xpath + p + "']"));
			isSelected(hbAddlInfo);
		}
	}

	@FindBy(xpath = "//input[@value='Book Service']")
	private WebElement bookService;

	public void bookService() {
		bookService.click();
	}

	@FindBy(xpath = ".//span[contains(text(),'From')]|.//a[contains(text(),'From')]")
	private WebElement locFromDropdown;

	public void LocFromdropdown() {
		selectFromDropDown(locFromDropdown, 2);
	}

	@FindBy(xpath = "//div[@class='dropOff locationBlock']//a[contains(text(),'To')]")
	private WebElement locToDropdown;

	public void LocToDropdown() {
		selectFromDropDown(locToDropdown, 3);
	}

	@FindBy(name = "returnTrip")
	private WebElement returnTrip;

	public void returnTrip() {
		returnTrip.click();
	}

	@FindBy(name = "dropOffLocationName")
	private WebElement dropOffLocName;

	public void dropOffLocName() {
		dropOffLocName.sendKeys("dropOfflocnickname");
	}

	@FindBy(name = "dropOffAddress1")
	private WebElement dropOffAddress1;

	public void dropOffAddress1() {
		dropOffAddress1.sendKeys("dropOffAddress1");
	}

	@FindBy(xpath = "//div[@class='buttonMain']")
	private WebElement addHomeBase;

	public void addHomebase() {
		addHomeBase.click();
	}

	@FindBy(id = "submitBtn")
	private WebElement addHomeBase1;

	public void addHomebase1() {
		addHomeBase1.click();
	}

	@FindBy(id = "notCSZInput1")
	private WebElement notCSZInput1;

	public void notCSZInput1() {
		notCSZInput1.sendKeys("Waltham, MA 02453");
	}

	@FindBy(id = "jobScheduleDescription")
	private WebElement jobScheduleDescription;

	public void jobScheduleDescription() {
		jobScheduleDescription.sendKeys("Testing description");
	}

	@FindBy(name = "adjectivesOfCaregiverDescription")
	private WebElement weAreLookingFor;

	public void weAreLookingFor() {
		weAreLookingFor.sendKeys("Testing adjectives");
	}

	@FindBy(xpath = ".//span[contains(text(),'Companion Care')]|.//a[contains(text(),'Companion Care')]")
	private WebElement CompanionCaredropdown;

	public void CompanionCare() {
		int p = getRandomIndex(1, 3);
		selectFromDropDown(CompanionCaredropdown, p);
	}

	public String getStringFromURL(String sId) {
		int getId = sId.indexOf("sitterId=");
		if (getId > 0) {
			getId = getId + "sitterId=".length();
			int nextIndex = sId.indexOf("&", getId);
			String sitterId = sId.substring(getId, nextIndex);
			return sitterId;
		}
		return null;

	}

	public void replyButton() {
		replyButton.click();
	}

	public void newReplyButton() {
		newReplyButton.click();
	}

	public void csrNotes() {
		csrNotes.sendKeys("CSR updating booking status");
	}

	public void sitterName() {
		sitterName.sendKeys("BUCSeeker");
	}

	public void merchandisePajCHILDCARE() {
		merchandisePAJCHILDCARE.click();
	}

	public void merchandisePAJSENIRCARE() {
		merchandisePAJSENIRCARE.click();
	}

	public void merchandisePAJPETCAREXX() {
		merchandisePAJPETCAREXX.click();
	}

	public void merchandisePAJHOUSEKEEP() {
		merchandisePAJHOUSEKEEP.click();
	}

	public void merchandisePajTUTORINGX() {
		merchandisePAJTUTORINGX.click();
	}

	public void merchandisePajCAREGIGSX() {
		merchandisePAJCAREGIGSX.click();
	}

	public void merchandisePajTRANSPORTATION() {
		merchandisePAJTRANSPORTATION.click();
	}

	public void merchandisePajSPCLNEEDS() {
		merchandisePAJSPCLNEEDS.click();
	}

	public void joinNowStepButton() {
		joinNowStepButton.click();
	}

	public void selectAlternateDays() {
		mondayAll.click();
		wednesdayAll.click();
		fridayAll.click();
	}

	public void petCareServices() {
		int p = getRandomIndex(1, 3);
		petCareServices = driver.findElement(By.xpath(petCareServicesXpath + p + "']"));
		isSelected(petCareServices);
	}

	@FindBy(xpath = "(//div[@class='indResultContainer ']/div/div[2]/div/div[2]/div/a)[last()]")
	private WebElement providerSearchResult;

	public void openLastProviderFromSearchResults() {
		providerSearchResult.click();
	}

	@FindBy(xpath = "(//*[@class='indResultContainer']/.//a/img)[last()]")
	private WebElement jobSearchResult;

	public void openLastJobFromSearchResult() {
		jobSearchResult.click();
	}

	@FindBy(xpath = "(//a[contains (text(), 'Request background check')])")
	private WebElement requestBGCLink;

	public void clickRequestBGCLink() {
		requestBGCLink.click();
	}

	@FindBy(xpath = "//div[@id='czen_Dialog_3']/div/div/span")
	private WebElement crossButtonOnRequestedBGCCloud;

	public void closeResuestedBGCCloud() {
		crossButtonOnRequestedBGCCloud.click();
	}

	@FindBy(xpath = "//a[@class='myProfileSettings']")
	private WebElement accountAndSettings;

	public void clickAccountAndSettings() {
		accountAndSettings.click();
	}

	@FindBy(xpath = "//a[contains(text(),'Downgrade')]")
	private WebElement downgradeLink;

	public void clickDowngradeLink() {
		downgradeLink.click();
	}

	@FindBy(xpath = "//input[@alt='Continue']")
	private WebElement continueButton;

	public void clickContinueButton() {
		continueButton.click();
	}

	@FindBy(xpath = "//input[@value='Log In']")
	private WebElement landingPageLoginButton;

	@FindBy(xpath = ".//*[@id='loginSmartForm']/div[4]/input[@type='submit']")
	private WebElement loginButton1;

	public void clickLandingPageLoginButton() {
		landingPageLoginButton.click();
	}

	@FindBy(id = "hireNo")
	private WebElement hireNo;

	public void clickNotHired() {
		hireNo.click();
	}

	@FindBy(id = "reasonSelDown")
	private WebElement downgradeReasonDropdown;

	@FindBy(css = ".writeReviewBtn")
	private WebElement writeAReview;

	@FindBy(xpath = "//form[@name='postReviewForm']/div/div/button")
	private WebElement submitReview2;

	@FindBy(xpath = "//form[@name='postReviewForm']/div/button")
	private WebElement submitReview3;

	@FindBy(id = "thankYouBlock")
	private WebElement reviewSuccessThankYouMsg;

	public void selectDowngradeReason(String reasonValue) {
		Select reasonDropdown = new Select(downgradeReasonDropdown);
		reasonDropdown.selectByValue(reasonValue);
	}

	@FindBy(name = "downgradeNPS")
	private WebElement recommendRatingDropdown;

	public void selectRecommendCareRating(String recommendValue) {
		Select recommendCareOnDowngrade = new Select(recommendRatingDropdown);
		recommendCareOnDowngrade.selectByValue(recommendValue);
	}

	@FindBy(xpath = "//input[@value='downgradeConfirm']")
	private WebElement confirmDowngrade;

	public void clickConfirmDowngrade() {
		confirmDowngrade.click();
	}

	@FindBy(xpath = "//input[@value='downgradeImmediately']")
	private WebElement downgradeImmediately;

	public void clickDowngradeImmediately() {
		downgradeImmediately.click();
	}

	*//**
	 * @param reasonValue
	 *            : Send value among option in dropdown to select reason for
	 *            downgrade
	 * @param recommendValue
	 *            : Rating between 1 to 10 for recommending Care.com to others,
	 *            checks to be added in future
	 * @param downgradeImmediatelyAsCSR
	 *            : true for CSR session ONLY
	 *//*
	public void fillDowngradeFeedback(String reasonValue, Integer recommendValue, boolean downgradeImmediatelyAsCSR) {
		// Fill out the form
		clickNotHired();
		selectDowngradeReason(reasonValue);
		// If user is CSR, we hide Care recommendation dropdown
		if (!downgradeImmediatelyAsCSR) {
			selectRecommendCareRating(recommendValue.toString());
		} else {
			// Select downgrade immediately
			clickDowngradeImmediately();
		}
	}

	public void clickJoinNow() {
		JoinNow.click();
	}

	public void selectdateNight() {
		datenight.click();
	}

	public void getGBBLimited() {
		gbbFree.click();
	}

	public void handleBookingPopUp() {
		bookingPopup.click();
	}

	public void selectGoodOneMonth() {
		goodOneMonth.click();
	}

	public void selectGoodSixMonth() {
		goodSixMonth.click();
	}

	public void selectBetterOneMonth() {
		betterOneMonth.click();
	}

	public void selectBetterSixMonth() {
		betterSixMonth.click();
	}

	public void selectBestOneMonth() {
		bestOneMonth.click();
	}

	public void selectBestSixMonth() {
		bestSixMonth.click();
	}

	public void clickContinue() {
		continueButton1.click();
	}

	*//**
	 * Method to get ZIP Code wich is not in GBBSTZ
	 *
	 * @return
	 *//*
	public String getRandomZipNotInGBBSTZ() {
		return DBUtil.executeQuery(DBQueries.ZIP_NOT_IN_GBBSTZ, new Object[] {}).get(0).get("ZIP").toString();
	}

	public void SleepTime(int seconds) {
		int i = seconds * 1000;
		try {
			Thread.sleep(i);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void closeBookingsCloud() {
		if (bookingCloudOkButton.isDisplayed()) {
			bookingCloudOkButton.click();
		}
		// WebElement close = new
		// WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(bookingCloudOkButton));
		// close.click();
	}

	// Desktop schedule booking

	public void scheduleLink(String firstName) {

		String schedule = "Schedule" + " " + firstName;
		driver.findElement(By.linkText(schedule)).click();
	}

	public void phoneInterview() {
		phoneInterview.click();

	}

	public void inPesronInterview() {
		inPersonInterview.click();

	}

	public void continueLink() {
		bookADateSubmit.click();
	}

	public void bookingTime() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		day = day + 2;
		System.out.println("*********************" + day);
		driver.findElement(By.xpath(bookColSelection + day + "]")).click();

	}

	public void provideravaPopUp() {
		Okthanks.click();
	}

	public void nextLink() {
		nextLink.click();
	}

	public void bookerPhone() {
		bookerPhone.sendKeys("9" + CreateCreditCard.generateRandom(9));
	}

	public void optionalNote(String note) {
		optionalNote.sendKeys("Testing" + note + " " + CommonUtil.generateRandomString());
	}

	public void requestInterview() {
		requestInterview.click();
	}

	public void appDownloadLink() {
		appDownloadLink.click();
	}

	public void closeMessageSendConfirmationDialog() {
		if ((driver.findElements(By.xpath(sendMessageConfirmationButtonXpath)).size() != 0)
				&& sendMessageConfirmationButton.isDisplayed()) {
			sendMessageConfirmationButton.click();
		}
	}

	public void clickSendMessageButtonOnProfilePage() {
		sendMessageButtonProfilePage.click();
	}

	public void openMessageThread(String threadId) {
		String xpath = "//div[@id='message-thread-" + threadId + "']/a[@class='subject-link']";
		driver.findElement(By.xpath(xpath)).click();
	}

	public void replyInMessageThread(String message) {
		replyTextBoxInThread.click();
		replyTextBoxInThread.sendKeys(message);
		replyButtonInThread.click();
	}

	public void replyMessageThread(String message) {
		replyTextBoxInThread.sendKeys(message);
		replyButtonInThread.click();
	}

	public void clickSeekerProfileLinkOnJobPage() {
		seekerProfileLinkOnJob.click();
	}

	public void upgradeNow() {
		upgradeNow.click();
	}

	public void upgradenow() {
		upgradenow1.click();
	}

	public void clickUpgrade() {
		upgradenow.click();
	}

	public void upgrade() {
		upgrade.click();
	}

	public void upgradeToday() {
		upgradeToday.click();
	}

	public void promoCodeField(String text) {
		promoCodeField.clear();
		promoCodeField.sendKeys(text);

	}

	public void babysitterornanny() {
		babysitterornanny.click();
	}

	public void recurringtoggle() {
		recurringtoggle.click();
	}

	public void educationlevel(int x, int y) {

		educationlevel = driver.findElement(By.xpath(educationlevelXPath + x + "]/div[" + y + "]/div/div/div/a"));

		educationlevel.click();

	}

	public void Collegedegree(int x, int y) {

		Collegedegree = driver.findElement(By.xpath(CollegedegreeXPath + x + "]/ul/li[" + y + "]"));

		Collegedegree.click();

	}

	public void saveAndcontinue(int x) {
		saveAndcontinue = driver.findElement(By.xpath(saveAndcontinueXpath + x + "]/button"));

		saveAndcontinue.click();
	}

	public void submitJob() {
		submitJob.click();
	}

	public void splneed() {
		splneed.click();
	}

	public void housekeep() {
		housekeep.click();
	}

	public void petcare() {
		petcare.click();
	}

	public void tutororlessons() {
		tutororlessons.click();
	}

	public void campanioncare() {
		campanioncare.click();
	}

	public void errandjob() {
		errandjob.click();
	}

	*//**
	 * job title
	 *//*
	public void JobTitle() {
		JobTitle.clear();
		JobTitle.sendKeys("Posting Job for Testing Purpose");
	}

	*//**
	 * Description
	 *//*
	public void JobDesc() {
		JobDescription.clear();
		JobDescription.sendKeys("Job Description for Testing");
	}

	public void tuitionSeeker() {
		tuitionSeeker.click();
	}

	public void tuitionSitter() {
		tuitionSitter.click();
	}

	public void seniorAgeRelation(int x) {
		seniorAgeRelation = driver.findElement(By.xpath(seniorAgeRelationXPath + x + "]/div/div/div/a"));
		seniorAgeRelation.click();
	}

	public void fillDOBTextBox(String dob) {
		dobTextBox.sendKeys(dob);
	}

	public void dobTextBox() {
		dobTextBox.sendKeys("05111985");
	}

	public WebElement getDOBTextBox() {
		return dobTextBox;
	}

	public void fillCCExpiryDate(String date) {
		ccExpiryDateTextBox.sendKeys(date);
	}

	public void ccExpiryDateTextBox() {
		ccExpiryDateTextBox.sendKeys("0522");
	}

	public void clickPayment() {
		upgradeSecondStepButton.click();
	}

	public void selectPricingPlanInTwoStepUpgrade() {
		List<WebElement> pricingPlansAvailable = driver
				.findElements(By.xpath("(//*[@name='membershipPlan.pricingPlan'])"));
		pricingPlansAvailable.get(ThreadLocalRandom.current().nextInt(1, 3)).click();
	}

	public void endSubscription() {
		endSubscription.click();
	}

	public void remindMeLater() {
		remindMeLater.click();
	}

	public void continueSubscription() {
		continueSubscription.click();
	}

	public void extendLink() {
		extendLink.click();
	}

	public void noThanksLink() {
		noThanksLink.click();
	}

	public void monetatePopUp() {
		monetatePopUp.click();
	}

	public void closeMonetatePopUp() {
		closeMonetatePopUp.click();
	}

	public void clickHired() {
		hireYes.click();
	}

	public void overallCareRating(int x) {
		overallRating = driver.findElement(By.xpath(overallRatingPath + x + "]/a"));
		new Actions(driver).moveToElement(overallRating).click().build().perform();

	}

	public void facePile() {
		facePile.click();
	}

	public void facePileSelect(String text) {
		facePileSelect = driver.findElement(By.xpath(facePileSelectXPath + text + "')]"));
		facePileSelect.click();
	}

	public void seekerBio() {
		seekerBio.click();
	}

	public void seekerBioText() {
		seekerBioText.sendKeys("I am seeking for all type of needs");
	}

	public void saveButton() {
		saveButton.click();
	}

	public void clickMyProfile() {
		clickMyProfile.click();
	}

	public void clickWriteAReviewButton() {
		writeAReview.click();
	}

	public void clickSubmitFeedbackButton() {
		submitReview2.click();
	}

	public void clickSubmitFeedbackButton3() {
		submitReview3.click();
	}

	public boolean isThankYouMsgVisible() {
		return reviewSuccessThankYouMsg.isDisplayed();
	}

	@FindBy(id = "preferred+")
	private WebElement newPreferredPlusBGCButton;

	@FindBy(id = "preferred")
	private WebElement newPreferredBGCButton;

	@FindBy(id = "premier")
	private WebElement newPremierBGCButton;

	public void selectOtherSubjectInMessageCloud() {
		if (driver.switchTo().activeElement().findElements(By.xpath("//select[@id='jobIdAndTitle']")).size() > 0) {
			Select subjectSelect = new Select(
					driver.switchTo().activeElement().findElement(By.xpath("//select[@id='jobIdAndTitle']")));
			subjectSelect.selectByVisibleText("Other");
		}
	}

	public void regularSnooze() {
		regularSnooze.click();
	}

	public void regularDowngrade() {
		regularDowngrade.click();
	}

	public boolean isElementPresentName(String element) {

		boolean value;
		if (!driver.findElements(By.linkText(element)).isEmpty()) {
			value = true;
		} else {
			value = false;
		}
		return value;

	}

	public void enterPromoCode() {
		enterPromoCode.click();
	}

	public void applyPromoCode() {
		applyPromoCode.click();
	}

	public void clickSubmitReviewButton() {
		submitReview2.click();
	}

	public void writeAReview() {
		this.clickWriteAReviewButton();
		this.SleepTime(5);
		// new
		// Actions(driver).moveToElement(driver.findElement(By.xpath("./*//*[@id='star_containr']/ul/li[3]/a"))).click().perform();
		this.SleepTime(5);
		this.overallRating();
		this.punctualityMicroAttribute();
		this.reliableTransportationMicroAttribute();
		this.dependabilityMicroAttribute();
		this.wouldyouhireagain();
		this.reviewComment();
		clickSubmitFeedbackButton3();
		this.SleepTime(5);
	}

	@FindBy(css = "a.rate.btn-primary")
	private WebElement rateAndReviewSMBButton;

	@FindBy(id = "review")
	private WebElement reviewTextboxSMB;

	@FindBy(xpath = "(//div[@id='reviewModal']//form//button[contains(@class,'submitReview')])[2]")
	private WebElement submitBtnReviewFormModal;

	public void clickSMBReviewButton() {
		rateAndReviewSMBButton.click();
	}

	String xpathForSMBReviewStars = "//input[@value=";

	public void fillSMBReview(String reviewText) {
		clickSMBReviewButton();
		// int rating = CommonUtil.getRandomNumberBetween(1,5);
		driver.switchTo().activeElement().findElement(By.xpath(xpathForSMBReviewStars + 4 + "]")).click();
		reviewTextboxSMB.sendKeys(reviewText);
		submitBtnReviewFormModal.click();
	}

	@FindBy(xpath = "//div[contains(@class,'message-tab')]")
	private WebElement messageTabL1;

	@FindBy(xpath = "//div[contains(@class,'message-tab')]/div/div[1]")
	private WebElement messagesL2;

	@FindBy(xpath = "//div[contains(@class,'message-tab')]/div/div[2]")
	private WebElement alertL2;

	public void openAlertFromL1() {
		new Actions(driver).moveToElement(messageTabL1).moveToElement(alertL2).click().build().perform();
	}

	public void openMessageFromL1() {
		new Actions(driver).moveToElement(messageTabL1).moveToElement(messagesL2).click().build().perform();
	}

	@FindBy(xpath = "(//a[contains(@class,'sendMessage')])[1]")
	private WebElement sendMessageButtonProfilePageSideBar;

	public void clickSendMsgBtnFromProfileSideBar() {
		sendMessageButtonProfilePageSideBar.click();
	}
	
	@FindBy(xpath = ".//*[@id='mainFramework']/div/div[13]/div[1]/div[5]/div[2]/div/a")
	private WebElement sendMessageButtonProfilePageSideBar;
	
	public void clickSendMsgBtnFromProfileSideBar(){
		sendMessageButtonProfilePageSideBar.click();
	}

	@FindBy(xpath = "(//div[contains(@class,'subject')])[1]")
	private WebElement mostRecentMessageThread;

	public void clickMostRecentMessageThread() {
		mostRecentMessageThread.click();
	}
	
	@FindBy(xpath = ".//*[@id='mainFramework']/div/div[13]/div[1]/div[5]/div[2]/div/a")
	private WebElement sendMessageButtonProfilePageSideBar1;
	
	public void clickSendMsgBtnFromProfileSideBar1() {
		sendMessageButtonProfilePageSideBar1.click();
	}

	@FindBy(xpath = "//div[text()='End Subscription']")
	private WebElement endSubscriptionButton;

	public void clickEndSubscriptionButton() {
		endSubscriptionButton.click();
	}

	@FindBy(xpath = "//input[@value='enhanced']/following-sibling::label")
	private WebElement newEnhancedBGCRadio;

	@FindBy(xpath = "//input[@value='enhancedPlus']/following-sibling::label")
	private WebElement newEnhancedPlusBGCRadio;

	@FindBy(xpath = "//input[@value='premier']/following-sibling::label")
	private WebElement newPremierBGCRadio;

	@FindBy(id = "firstName")
	private WebElement eBureauFirstName;

	@FindBy(id = "lastName")
	private WebElement eBureauLastName;

	@FindBy(id = "addressLine1")
	private WebElement eBureauAddress;

	@FindBy(xpath = ".//*[@id='addressLine1']")
	private WebElement billingStreetAddress;

	@FindBy(id = "newCSZInput")
	private WebElement eBureauZip;

	@FindBy(xpath = "//div[contains(@class,'ask select month')]")
	private WebElement monthDropdownListEbureau;

	// to select day
	@FindBy(xpath = "//div[contains(@class,'ask select day')]")
	private WebElement dateDropDownListEbureau;

	// to select year
	@FindBy(xpath = "//div[contains(@class,'ask select year')]")
	private WebElement yearDropDownListEbureau;

	public void ebureauFormFill(String fName, String lName) {
		WebElement displayQuestionnaireCloud = driver.findElement(By.id("displayQuestionnaire"));
		WebElement retryPersonalInfoCloud = driver.findElement(By.id("retryPersonalInfo"));
		WebElement displayAlternateDataCloud = driver.findElement(By.id("displayAlternateData"));
		driver.switchTo().activeElement();
		driver.findElement(By.id("firstName")).sendKeys(fName);
		driver.findElement(By.id("lastName")).sendKeys(lName);
		driver.findElement(By.name("addressLine1")).sendKeys("100 Dohlenweg");
		driver.findElement(By.id("newCSZInput")).sendKeys("02453");
		dateOfBirthListSelection();
		driver.findElement(By.className("submitBtnLrg")).click();
		this.SleepTime(5);
		// TODO : Check for isVisible, then proceed
		// TODO : Using Non standard select procedure since dropdown in this
		// cloud are non standard
		WebElement q1 = displayQuestionnaireCloud.findElement(By.name("question1"));
		WebElement q2 = displayQuestionnaireCloud.findElement(By.name("question2"));
		WebElement q3 = displayQuestionnaireCloud.findElement(By.name("question3"));
		WebElement btn = displayQuestionnaireCloud.findElement(By.className("continueBtn"));
		new Actions(driver).moveToElement(q1).click().sendKeys(Keys.END).sendKeys(Keys.TAB).sendKeys(Keys.END)
				.sendKeys(Keys.TAB).sendKeys(Keys.END).sendKeys(Keys.TAB)
				// .moveToElement(driver.findElement(By.xpath("(.//option[@value='None
				// of the above'])[1]"))).click()
				.perform();
		
		 * new Actions(driver).moveToElement(q2).click()
		 * .moveToElement(driver.findElement
		 * (By.xpath("(.//option[@value='None of the above'])[2]"))).click() .perform();
		 * new Actions(driver).moveToElement(q3).click() .moveToElement
		 * (driver.findElement(By. xpath("(.//option[@value='None of the above'])[3]"
		 * ))).click() .perform();
		 
		this.SleepTime(5);
		btn.click();
		this.SleepTime(5);
	}

	// Month drop down
	@FindBy(xpath = ".//*[@id='add-child-fields_0']/div[3]/div[1]/div")
	private WebElement monthDropdown;

	// to select year
	@FindBy(xpath = ".//*[@id='add-child-fields_0']/div[3]/div[3]/div/div/a")
	private WebElement yearDropDown;

	*//**
	 * list selection of date of birth
	 *//*
	public void dateOfBirthSelection() {
		int p = getRandomIndex(2, 12);
		selectFromDropDown(monthDropdown, p);
		int p2 = getRandomIndex(2, 113);
		if (p2 < 30) {
			p2 = 30;
		}
		selectFromDropDown(yearDropDown, p2);
	}

	// @FindBy(xpath =
	// ".//*[@id='backupCareAboutYourFamily']/div[2]/div[1]/div[2]/div/div/a")
	@FindBy(xpath = ".//div[2]/div[1]/div[2]/div/div/a")
	private WebElement phoneType;

	public void phoneType() {
		int p = getRandomIndex(2, 3);
		selectFromDropDown(phoneType, p);
	}

	@FindBy(xpath = ".//*[@id='backupCareAboutYourFamily']/div[6]/div[1]/div/div/div/a")
	private WebElement reasonForCare;

	public void reasonForCare() {
		int p = getRandomIndex(2, 5);
		selectFromDropDown(reasonForCare, p);
	}

	@FindBy(xpath = ".//*[@id='backupCareInHomeBookingDetailsForm']/div[2]/div[2]/div[1]/div[2]/div/div/a")
	private WebElement bucrequeststarttime;

	public void bucRequestStartTime() {
		int p = getRandomIndex(2, 20);
		selectFromDropDown(bucrequeststarttime, p);
	}

	@FindBy(xpath = ".//*[@id='backupCareInHomeBookingDetailsForm']/div[2]/div[2]/div[1]/div[2]/div/div/a")
	private WebElement bucrequestendtime;

	public void bucRequestEndTime() {
		int p = getRandomIndex(2, 20);
		selectFromDropDown(bucrequestendtime, p);
	}

	// @FindBy(xpath =
	// ".//*[@id='backupCareInHomeBookingDetailsForm']/div[3]/div[4]/div[1]/div/div/a")
	@FindBy(xpath = ".//div[4]/div[1]/div/div/a")
	private WebElement ccexpmonth;

	public void ccExpMonth() {
		int p = getRandomIndex(2, 10);
		selectFromDropDown(ccexpmonth, p);
	}

	// @FindBy(xpath =
	// ".//*[@id='backupCareInHomeBookingDetailsForm']/div[3]/div[4]/div[2]/div/div/a")
	@FindBy(xpath = ".//div[4]/div[2]/div/div/a")
	private WebElement ccexpyear;

	public void ccExpYear() {
		int p = getRandomIndex(2, 10);
		selectFromDropDown(ccexpyear, p);
	}

	@FindBy(xpath = ".//*[@id='backupCareInHomeBookingDetailsForm']/div[2]/div[2]/div[2]/div[3]/div/div/a")
	private WebElement am_pm;

	public void am_pm() {
		// int p = getRandomIndex(2, 10);
		selectFromDropDown(am_pm, 2);
	}

	@FindBy(xpath = ".//*[@id='RshellContainer']/div/div/div[2]/div/div[2]/div[2]/form/div[7]/div[2]/div[2]/div[4]/div/div/a")
	private WebElement selectchoice;

	public void selectChoice() {
		// int p = getRandomIndex(2, 10);
		selectFromDropDown(selectchoice, 2);
	}

	@FindBy(xpath = ".//*[@id='add-child-fields_0']/div[3]/div[1]/div/div/a")
	private WebElement incentermonth;

	@FindBy(xpath = ".//*[@id='add-child-fields_0']/div[3]/div[2]/div/div/a")
	private WebElement incenterday;

	@FindBy(xpath = ".//*[@id='add-child-fields_0']/div[3]/div[3]/div/div/a")
	private WebElement incenteryear;

	public void dateOfBirthIncenter() {
		int p = getRandomIndex(2, 12);
		selectFromDropDown(incentermonth, p);
		int p1 = getRandomIndex(2, 28);
		selectFromDropDown(incenterday, p1);
		int p2 = getRandomIndex(2, 10);

		selectFromDropDown(incenteryear, p2);

	}

	@FindBy(xpath = ".//*[@id='add-child-fields_0']/div[3]/div[4]/div/div/a")
	private WebElement incentergender;

	public void incenterGender() {
		int p = getRandomIndex(2, 3);
		selectFromDropDown(incentergender, p);
	}

	public void oneTimeToggle() {
		oneTimeToggle.click();
	}

	public void oneTimePAJ() {
		oneTimeStartDate.clear();
		oneTimeStartDate.sendKeys(getFutureDate(1));
		oneTimeEndDate.clear();
		oneTimeEndDate.sendKeys(getFutureDate(1));
		driver.findElement(By.xpath(".//*[@id='nthDayPaj']/div[2]/div[2]/dl/dd[2]/div[3]/div/div/a")).click();
		driver.findElement(By.xpath("/html/body/div[6]/ul/li[1]")).click();
	}

	public void oneTimePAJHK() {
		oneTimeStartDateHK.clear();
		oneTimeStartDateHK.sendKeys(getFutureDate(1));
		oneTimeEndDateHK.clear();
		oneTimeEndDateHK.sendKeys(getFutureDate(1));
		driver.findElement(By.xpath(".//*[@id='nthDayPaj']/div[3]/div[3]/dl/dd[2]/div[3]/div/div/a")).click();
		driver.findElement(By.xpath("/html/body/div[11]/ul/li[1]")).click();
	}

	public void searchPageSendMessage(int x) {
		searchSendMessage = driver.findElement(By.xpath(searchSendMessageXPath + x + "]"));
		searchSendMessage.click();
	}

	public void clickProviderSearchPage(int x) {
		clickProviderSearchPage = driver.findElement(By.xpath(clickProviderSearchPageXPath + x + "]"));
		clickProviderSearchPage.click();
	}

	public void clickNearMe() {
		clickNearMe.click();
	}

	public void clickMyFavorites() {
		clickMyFavorites.click();
	}

	public void jobAccept() {
		jobAccept.click();
	}

	public void jobDecline() {
		jobDecline.click();
	}

	public void jobPhoneInterview() {
		phoneInterview();
		continueLink();
		// provideravaPopUp();
		bookingTime();
		nextLink();
		optionalNote("phone Interview");
		requestInterview();
		appDownloadLink();
	}

	public void jobInPersonInterview() {
		inPesronInterview();
		continueLink();
		// provideravaPopUp();
		bookingTime();
		nextLink();
	
		addressLine1("test");
		optionalNote("In-person Interview");
		requestInterview();
		appDownloadLink();
	}

	public void enrollAsSeekerWithBasicDetails(String fName, String lName) {
		firstName.sendKeys(fName + CommonUtil.generateRandomString());
		lastName.sendKeys(lName + CommonUtil.generateRandomString());
		cityStateZIP.clear();
//		cityStateZIP.sendKeys(getRandomZipNotInGBBSTZ());
		// password.sendKeys("letmein1");

	}

	public void seekerEnrollmentBasicDetail() throws InterruptedException {
		firstName1.sendKeys("seekerFirstName" + CommonUtil.generateRandomString());
		lastName1.sendKeys("seekerLastName" + CommonUtil.generateRandomString());
		addressLine1.sendKeys("Waltham street");
		eBureauZip.clear();
		eBureauZip.sendKeys("02451");
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='smartEnrollmentForm']/div/div[7]/input")).sendKeys("letmein1");
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='smartEnrollmentForm']/div/div[11]/input")).click();

	}

	public void rtpCardDetails() {
		firstNameOnCard.sendKeys("firstName");
		lastNameOnCard.sendKeys("lastName");
		cardNumber.sendKeys("4242424242424242");
		cvv.sendKeys("123");

	}

	public void setExpiry() {
		setSelectedField(expiryMonth, "10");
		setSelectedField(expiryYear, "2030");

	}

	public void submitLead() {
		WebElement leadsubmit = driver.findElement(By.xpath("(//button[contains(@class,'signUp')])[position()=1]"));

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		// executor.executeScript("arguments[0].scrollIntoView()", leadsubmit);
		executor.executeScript("scroll(0, 250)");
		
		 * Actions action = new Actions(driver); action.sendKeys(Keys.PAGE_DOWN);
		 * SleepTime(2);
		 
		SleepTime(2);
		leadsubmit.click();
	}

	// Seeker BGC Rport Access

	@FindBy(xpath = "//input[@value='Log In Now']")
	private WebElement LoginButton1;

	public void LoginButton1() {
		LoginButton1.click();
	}

	@FindBy(partialLinkText = "Request background check")
	private WebElement reqbgc;

	public void reqbgc() {
		reqbgc.click();
	}

	@FindBy(xpath = "//input[@type='radio' and @value='enhancedPlus']")
	
	 * @FindBy(xpath="//*[@id='activePricing']/table/tbody/tr/td[2]/div/label")
	 
	private WebElement selectEnhancedPlus;

	public void selectEnhancedPlus() {
		selectEnhancedPlus.click();
	}

	@FindBy(xpath = "//input[@id='bgCheckType']")
	private WebElement bgctypeclick;

	public void bgctypeclick() {
		bgctypeclick.click();
	}

	@FindBy(xpath = "//button[text()='Request Background Check']")
	private WebElement reqbgcbtn;

	public void reqbgcbtn() {
		reqbgcbtn.click();

	}

	@FindBy(xpath = "//*[@id='czen_Dialog_9']/div/div[1]/span")
	private WebElement ClickCloseIcon;

	public void ClickCloseIcon() {
		ClickCloseIcon.click();
	}

	@FindBy(xpath = "//*[@id='upsellNoThanks']")
	private WebElement NoThanks;

	public void NoThanks() {
		NoThanks.click();
	}

	@FindBy(id = "confirmGotIt")
	private WebElement Confirm;

	public void Confirm() {
		Confirm.click();
	}

	@FindBy(xpath = "//form[@id='loginForm']//input[@id='emailId']")
	private WebElement Email;

	public void Email(String username) {
		//Email.clear();
		Email.sendKeys(username);
	}

	@FindBy(xpath = "//form[@id='loginForm']//input[@type='password']")
	private WebElement Passwd;

	public void Passwd() {
		Passwd.clear();
		Passwd.sendKeys("letmein1");
	}

	@FindBy(xpath = "//*[@id='loginForm']/button")
	private WebElement Loginbtn;

	public void Loginbtn() {
		Loginbtn.click();
	}

	@FindBy(xpath = "//*[@id='navigation']/div[2]/div[4]/div[6]/div/a")
	private WebElement Backgroundcheck;

	public void Backgroundcheck() {
		Backgroundcheck.click();
	}

	 @FindBy(partialLinkText = "Alerts") 
	@FindBy(xpath = "//*[@id='nav']/div[7]/div/div[2]/a")
	private WebElement Alerts1;

	public void ClickAlerts1() {
		Actions actions = new Actions(driver);
		WebElement we = driver.findElement(By.xpath("//*[@id='nav']/div[7]/a"));
		actions.moveToElement(we).build().perform();
		actions.moveToElement(Alerts1).click().build().perform();

	}

	@FindBy(partialLinkText = "Got it")
	private WebElement GotIt1;

	public void GotIt1() {
		GotIt1.click();
	}

	@FindBy(xpath = "//*[@id='mainFramework']/div/div[3]/div[2]/div[2]/div[2]/div/div[1]/div/div[1]/div/div/div[2]/div/a[2]")
	private WebElement ViewReportLink;

	public void ViewReportLink() {
		ViewReportLink.click();
		;
	}

	@FindBy(xpath = "//*[@id='mainFramework']/div/div[3]/div[2]/div[2]/div[2]/div/div[1]/div/div[1]/div/div/div[2]/div/a[3]")
	private WebElement ViewReportLink1;

	public void ViewReportLink1() {
		ViewReportLink1.click();
	}

	@FindBy(xpath = "//div[contains(@id,'message-thread')]//a")
	private WebElement BgcMessage;

	public void BgcMessage() {
		BgcMessage.click();
	}

	@FindBy(xpath = "//input[@type='checkbox' and @value='']")
	private WebElement Acknowledge;

	public void Acknowledge() {
		Acknowledge.click();
	}

	@FindBy(id = "continueButton")
	private WebElement Continue;

	public void Continue() {
		Continue.click();
	}

	@FindBy(xpath = "//a[text()='Grant Access']")
	private WebElement GrantAccess;

	public void GrantAccess() {
		GrantAccess.click();
	}

	@FindBy(xpath = "//*[@id='federalCheck']")
	private WebElement FedCheck;

	public void FedCheck() {
		FedCheck.click();
	}

	@FindBy(xpath = "//*[@id='federalAgree']")
	private WebElement FedApprove;

	public void FedApprove() {
		FedApprove.click();
	}

	@FindBy(xpath = "//*[@id='requestDate']")
	private WebElement CARequestDate;

	public void CARequestDate() {
		CARequestDate.click();
	}

	@FindBy(xpath = "//*[@id='SaveCopy']")
	private WebElement SaveCopy;

	public void SaveCopy() {
		SaveCopy.click();
	}

	@FindBy(xpath = "//*[@id='californiaAgree']")
	private WebElement CaliforniaAgree;

	public void CaliforniaAgree() {
		CaliforniaAgree.click();
	}

	@FindBy(xpath = "//a[text()='View Check']")
	private WebElement ViewReport;

	public void ViewReport() {
		ViewReport.click();

	}

	// Trusted SeekerBGCRequest

	@FindBy(xpath = "//*[@id='searchBarForm']/input[2]")
	private WebElement zip;

	public void zip() {
		zip.clear();
		zip.sendKeys("02453");
	}

	@FindBy(xpath = "//*[@id='searchBarForm']/button")
	private WebElement Search;

	public void Search() {
		Search.click();
	}

	@FindBy(id = "option100")
	private WebElement NewMembers;

	public void NewMember() {
		NewMembers.click();
	}

	@FindBy(xpath = ".//*[@id='genInfoContent']/ul/li[20]/input")
	private WebElement RefineResults;

	public void RefResults() {
		RefineResults.click();
	}

	@FindBy(xpath = "//input[@type='radio' and @value='enhanced']")
	private WebElement enhanced;

	@FindBy(xpath = "//*[@id='czen_Dialog_9']/div/div[2]/div[2]/div[1]/form/div/div[7]/div/button")
	private WebElement enhanced1;

	public void SelectEnhanced() {
		enhanced.click();

		try {
			enhanced.click();

		} catch (NoSuchElementException e) {
			enhanced1.click();
		}
	}

	@FindBy(xpath = "//button[contains(@class,'next-step')][text()='I agree']")
	private WebElement IAgree;

	public void IAgree() {
		IAgree.click();
	}

	@FindBy(xpath = "// button[@id='confirmGotIt']")
	private WebElement GotIt;

	public void GotIt() {
		GotIt1.click();
	}

	@FindBy(partialLinkText = "Alerts")
	private WebElement Alerts;

	public void ClickAlerts() {
		Actions actions = new Actions(driver);
		WebElement we = driver.findElement(By.xpath("//*[@id='nav']/div[7]/a"));
		actions.moveToElement(we).build().perform();
		actions.moveToElement(Alerts1).click().build().perform();

	}

	public void changeCard() {
		changeCard.click();
	}

	public WebElement getCardType() {
		return cardType;
	}

	public void fillCCNumber(String num) {
		cardNumber.sendKeys(num);

	}

	public void fillCardNumber(String num) {
		ccNumber.sendKeys(num);
	}

	public void fillCVVNumber(String num) {
		cvvNumber.sendKeys(num);
	}

	public void selectExpMonth(String month) {
		setSelectedField(expMonth, month);
	}

	public void selectExpYear(String year) {
		setSelectedField(expYear, year);
	}

	public void submitUpgrade() {
		submitUpgrade.click();
	}

	public void navigateToPaymentTab() {
		paymentTab.click();
	}

	 SeekerEnhancedBGCrequest 

	@FindBy(xpath = "//button[text()='Continue']")
	private WebElement IntroPage;

	public void IntroPage() {
		IntroPage.click();
	}

	
	 * @FindBy(xpath = "//button[contains(@class,'next-step')][text()='Next']")
	 
	@FindBy(xpath = "//button[text()='Next']")
	private WebElement Next;

	public void Next() {
		Next.click();

	}

	@FindBy(xpath = "//*[@id='kbaForm']/div[2]/button")
	private WebElement KbaContinue;

	
	 * @FindBy(xpath = "//button[text()='Continue']") private WebElement
	 * KbaContinue;
	 

	public void KbaContinue() {
		KbaContinue.click();
	}

	// UBGCnON MVR
	@FindBy(partialLinkText = "Join now")
	private WebElement JoinNow1;

	public void JoinNow() {
		JoinNow1.click();
	}

	@FindBy(xpath = "//*[text()='Join now' or @id='submitButton']")
	private WebElement Join;

	@FindBy(xpath = ".//*[@id='singlePageEnrollmentForm']/div[12]/button")
	private WebElement seekerJoinNow;

	public void seekerJoinNow() {
		seekerJoinNow.click();
	}

	public void Join() {
		Join.click();
	}

	@FindBy(name = "firstName")
	private WebElement firstName1;

	public void firstName(String fname) {
		firstName1.sendKeys("SeekerFname");
	}

	@FindBy(name = "lastName")
	private WebElement lastName1;

	public void lastName(String lname) {
		lastName1.sendKeys("SeekerLname");
	}

	@FindBy(id = "cityStateZip")
	private WebElement cityStateZip1;
	
	@FindBy(name = "cityStateZIP")
	private WebElement cityzip;
	
	public void zip1(){
		cityzip.clear();
		cityzip.sendKeys("99547");
	}

	public void csz() {

		cityStateZip1.clear();
		cityStateZip1.sendKeys("Salt Lake City, UT 84101");

	}

	@FindBy(xpath = "//div[@class='optionsContentTopBlock optionsContent']/div/div[1]")
	private WebElement Nannies;

	public void Nanny() {
		Nannies.click();
	}

	@FindBy(xpath = "//*[@id='wizardNextStep']/p/a")
	private WebElement SkipSeg;

	public void SkipSegPage() {
		SkipSeg.click();
	}

	@FindBy(partialLinkText = "skip this step")
	private WebElement PajSkip;

	public void PajSkip() {
		PajSkip.click();
	}

	@FindBy(xpath = "//*[@id='ccAddressLine1']")
	private WebElement Address;

	public void Address() {
		Address.sendKeys("UTAH");
	}

	@FindBy(xpath = "//button[text()='Continue']")
	private WebElement Upgrade;

	public void Upgrade() {
		Upgrade.click();
	}

	@FindBy(xpath = "//button[text()='Add to subscription']")
	private WebElement UbgcAddon;

	public void UbgcAddon() {
		UbgcAddon.click();
	}

	@FindBy(xpath = "//*[@id='subscriptionFeatureAddOnForm']/div/div/div[4]/div[2]/a")
	private WebElement NoThanks1;

	public void JobAddonSkip() {
		NoThanks1.click();
	}

	@FindBy(xpath = "//*[@id='wizardNextStep']/div/div")
	private WebElement OrderTotal;

	public void OrderTotal() {
		OrderTotal.click();
	}

	@FindBy(partialLinkText = "skip this step")
	private WebElement Skip;

	public void Skip() {
		Skip.click();
	}

	 jobmatching 
	@FindBy(xpath = ".//*[@id='mainFramework']/div/div/div/div[3]/div/div/div/div[1]/div/a[2]/div")
	private WebElement matchtab;

	public void matchtab() {
		matchtab.click();
	}

	@FindBy(xpath = ".//*[@id='mainFramework']/div/div/div/div[3]/div/div/div/div[1]/div/a[1]/div")
	private WebElement Applicant;

	public void applicantstab() {
		Applicant.click();
	}

	@FindBy(xpath = ".//*[@id='mainFramework']/div/div/div/div[3]/div/div/div/div[1]/div/a[3]/div")
	private WebElement favourite;

	public void favouritetab() {
		favourite.click();

	}

	@FindBy(xpath = ".//*[@id='mainFramework']/div/div/div/div[4]/div[1]/div/div/div[2]/a/div/div/div[2]/div[1]/span[3]/svg/path")
	private WebElement applicantfavourited;

	public void applicantfavourited() {
		applicantfavourited.click();
	}

	@FindBy(xpath = ".//*[@id='mainFramework']/div/div/div/div[4]/div[1]/div/div/a")
	private WebElement declinedapplicants;

	public void declinedapplicants() {
		declinedapplicants.click();
	}

	@FindBy(xpath = ".//*[@id='mainFramework']/div/div/div/div[2]/div/div/div/div[1]/a/div[2]/div/span[2]/a[3]")
	private WebElement closejob;
	
	
	public void closejob() {
		closejob.click();
	}
	
	@FindBy(xpath = "//button[@class='btn btn-primary-2 btn-sm' and @type='button']")
	private WebElement closeCta;
	
	public void closeCTA(){
		closeCta.click();
	}
	
	@FindBy(xpath = "//div[@class='facepile-desc']/div/h5")
	private WebElement hiringFacepileFlow;
	
	public void hiringFlow(){
		hiringFacepileFlow.click();
	}
	
	@FindBy(xpath = "//button[@class='btn btn-primary-1 btn-sm payment-btn' and @type='button']")
	private WebElement hiringPayment;
	
	public void hiringPay(){
		hiringPayment.click();
	}
	
	@FindBy(xpath = ".//*[@id='mainFramework']/div/div[3]/div/span/a/span/span[2]/h6")
	private WebElement backToHome;
	
	public void back2Home(){
		backToHome.click();
	}
	
	@FindBy(xpath = ".//*[@id='nav_dd_trigger']/span/svg/path")
	private WebElement accountSettingSlider;
	
	public void accSettingSlider(){
		accountSettingSlider.click();
	}
	
	@FindBy(xpath = "//button[@class='btn btn-sm btn-primary-2 logout-btn']")
	private WebElement PlLogut;
	
	public void patterLogout(){
		PlLogut.click();
	}

	@FindBy(xpath = ".//*[@id='mainFramework']/div/div/div/div[1]/div/div/a[2]/div")
	private WebElement Closedjobs;

	public void Closedjobs() {
		Closedjobs.click();
	}

	@FindBy(xpath = ".//*[@id='mainFramework']/div/div/div/div[2]/div/div/div/a/div[3]/div/button")
	private WebElement Reopenjob;

	public void Reopenjob() {
		Reopenjob.click();
	}

	@FindBy(xpath = ".//*[@id='apply_now_anim']")
	private WebElement applyjob;

	public void applyjob() {
		applyjob.click();
	}

	@FindBy(xpath = ".//*[@id='applyJobMessage']")
	private WebElement addmesg;

	public void addmesg() {
		addmesg.sendKeys("test");
	}

	@FindBy(xpath = ".//*[@id='submitButton']")
	private WebElement jobapplied;

	public void jobapplied() {
		jobapplied.click();
	}

	@FindBy(xpath = ".//*[@id='availCHILDCARE']/tbody/tr[2]/td[2]/input")
	private WebElement time;

	public void time() {
		time.click();
	}

	@FindBy(xpath = ".//*[@id='nthDayPaj']/div[3]/div[2]/div[2]/div[1]/div/span[1]")
	private WebElement numberofchildren;

	public void numberofchildren() {
		numberofchildren.click();
	}

	@FindBy(xpath = ".//*[@id='nthDayPaj']/div[3]/div[4]/div[2]/div/table/tbody/tr[1]/td[1]/input")
	private WebElement Mykids;

	public void Mykids() {
		Mykids.click();
	}

	@FindBy(xpath = ".//*[@id='nthDayPaj']/div[5]/button")
	private WebElement next;

	public void next() {
		next.click();
	}

	@FindBy(xpath = ".//*[@id='nthDayPaj']/div[2]/button")
	private WebElement nextpage;

	public void nextpage() {
		nextpage.click();
	}

	*//**
	 * 
	 *//*
	public void continu() {
		continu.click();
	}

	@FindBy(xpath = "//span[contains(text(),'Change')]")
	private WebElement ChangeinCareneeds;

	@FindBy(xpath = "//button[@id='preferencesForm']")
	private WebElement PreferencesForm;

	@FindBy(xpath = "//input[@value='downgradeImmediately']")
	private WebElement DowngradeImmediately;

	@FindBy(id = "cancel")
	private WebElement cancel;

	public void changeinCareneeds() {
		ChangeinCareneeds.click();
	}

	public void downgradeRating() {
		int i = getRandomIndex(2, 10);
		WebElement rate = driver.findElement(By.xpath("//span[contains(text(),"
				+ i + ")]"));
		rate.click();
	}

	public void preferencesFormSubmit() {
		PreferencesForm.click();
	}

	public void downgradeImmediately() {
		DowngradeImmediately.click();
	}

	public void cancel() {
		cancel.click();
	}
	
	
	*/
}
