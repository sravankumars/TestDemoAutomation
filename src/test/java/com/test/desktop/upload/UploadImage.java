package com.test.desktop.upload;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.test.desktop.common.BasePO;
import com.test.desktop.common.DriverHelper;
import com.test.desktop.common.Url;
import com.test.desktop.pageobject.SitePageObject;

public class UploadImage extends BasePO{

	@BeforeClass
	public void setUp(){
		driver = DriverHelper.getDriver();
	}
	
	@Test
	public void navigateToHomePage() {
	//	String url = Url.BASEURL.getURL();
	//	System.out.println("Navigating to Infinico: " + url);
		navigateToURL("https://imgbb.com/");
		sleep(1000);
	}
	
	//Using AUTOIT and SCRIPT
	@Test(priority=1)
	public void imageUploadAutoIt(){
		System.out.println("Succefully "+ driver.findElement(SitePageObject.upload).isDisplayed());
		waitForElementToDisplay(SitePageObject.upload);
		click(SitePageObject.upload);
		imageAutoIt();
	
	}
		
  /*
		//Using Robot
		@Test(priority=2)
		public void imageUploadRobot(){
		
			System.out.println("Succefully "+ driver.findElement(SitePageObject.upload).isDisplayed());
			waitForElementToDisplay(SitePageObject.upload);
			click(SitePageObject.upload); 
			imageAutoIt();
			//imageRobot();
		
	
	}*/
	
}
	
	
	
	
	
	

