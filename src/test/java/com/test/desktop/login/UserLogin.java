package com.test.desktop.login;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.apache.tools.ant.util.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.infinico.selenum.common.CommonUtil;
import com.infinico.selenum.common.Loggers;
import com.test.desktop.common.BasePO;
import com.test.desktop.common.DriverHelper;
import com.test.desktop.common.Url;
import com.test.desktop.pageobject.SitePageObject;

public class UserLogin extends BasePO {
	// public WebDriver driver=DriverHelper.getDriver();
	private static final Logger logger = Loggers.getLogger(UserLogin.class);

	// @Parameters("browser")

	@BeforeClass
	public void setUp() {
		driver = DriverHelper.getDriver();
	}

	@Test(priority = 1)
	public void navigateToHomePage() throws IOException {
		String url = Url.BASEURL.getURL();
		System.out.println("Navigating to Infinico: " + url);
		navigateToURL(url);

		takeScreenShot(this.getClass().getSimpleName());

	}

	@Test(priority = 2)
	public void enterUsername() throws IOException {
		// Finding element in page.
		waitForElementToBeVisible(SitePageObject.USERNAME);

		// Calling username from getSheet method from BasePO class.
		getSheet();
		// Sending username
		sendKeys(SitePageObject.USERNAME, BasePO.username);
		// logger.info("SIGNIN_PAGE: Entered email." + BasePO.username);

		/*
		 * try { if(driver==null) { System.out.println("driver is null"); driver =
		 * DriverHelper.getDriver(); }
		 * 
		 * driver.findElement(By.id("email")).sendKeys(emailId); } catch(Exception e) {
		 * System.out.print(e); }
		 */
	}

	@Test(priority = 3)
	public void enterPassword() {
		waitForElementToBeVisible(SitePageObject.PASSWORD);
		sendKeys(SitePageObject.PASSWORD, SitePageObject.pswrd);
		// logger.info("SIGNIN_PAGE: Entered password." + SitePageObject.pswrd);
	}

	@Test(priority = 4)
	public void clickSignInButton() {
		click(SitePageObject.SIGNIN_BUTTON);
		takeScreenShot(this.getClass().getSimpleName());
	}

	@Test(priority = 5)
	public void Verify() {
		waitForElementToBeVisible(SitePageObject.verifyhome);
		checkMessage(driver.findElement(SitePageObject.verifyhome), "Home");
		// System.out.println("Succefully verified: "+
		// driver.findElement(SitePageObject.verifyhome).getText());
	}

}
