package com.test.desktop.common;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.TestException;
import org.testng.annotations.Parameters;

import com.infinico.selenum.common.ApplicationConstants;
import com.infinico.selenum.common.CommonUtil;
import com.infinico.selenum.common.Loggers;
import com.test.desktop.pageobject.SitePageObject;

//BasePO======CommonUtils in another proj
//DriverHelper========DriverUtils in another proj

public class BasePO {

	
	   public static WebDriver driver;
	    public WebDriverWait wait;
	    public Actions actions;
	    public Select select;

	  private static int timeout = 10;
	
	  public static final Logger logger = Loggers.getLogger(BasePO.class);
		
	    public BasePO() {
	        driver = DriverHelper.getDriver();
	    }
	    
		public BasePO(WebDriver driver) {
			this.driver = DriverHelper.getDriver();
			driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
			PageFactory.initElements(driver, this);

		}
	    
	  
	    public static void navigateToURL(String URL) {
	        try {
	            driver.navigate().to(URL);
	        } catch (Exception e) {
	            System.out.println("FAILURE: URL did not load: " + URL);
	            throw new TestException("URL did not load");
	        }
	    }

	    public void navigateBack() {
	        try {
	            driver.navigate().back();
	        } catch (Exception e) {
	            System.out.println("FAILURE: Could not navigate back to previous page.");
	            throw new TestException("Could not navigate back to previous page.");
	        }
	    }

	    public String getPageTitle() {
	        try {
	            return driver.getTitle();
	        } catch (Exception e) {
	            throw new TestException(String.format("Current page title is: %s", driver.getTitle()));
	        }
	    }

	    public String getCurrentURL() {
	        try {
	            return driver.getCurrentUrl();
	        } catch (Exception e) {
	            throw new TestException(String.format("Current URL is: %s", driver.getCurrentUrl()));
	        }
	    }

	    public WebElement getElement(By selector) {
	        try {
	            return driver.findElement(selector);
	        } catch (Exception e) {
	            System.out.println(String.format("Element %s does not exist - proceeding", selector));
	        }
	        return null;
	    }

	  

	    public List<WebElement> getElements(By Selector) {
	        waitForElementToBeVisible(Selector);
	        try {
	            return driver.findElements(Selector);
	        } catch (Exception e) {
	            throw new NoSuchElementException(String.format("The following element did not display: [%s] ", Selector.toString()));
	        }
	    }

	    public List<String> getListOfElementTexts(By selector) {
	        List<String> elementList = new ArrayList<String>();
	        List<WebElement> elements = getElements(selector);

	        for (WebElement element : elements) {
	            if (element == null) {
	                throw new TestException("Some elements in the list do not exist");
	            }
	            if (element.isDisplayed()) {
	                elementList.add(element.getText().trim());
	            }
	        }
	        return elementList;
	    }

	    public void click(By selector) {
	        WebElement element = getElement(selector);
	        waitForElementToBeClickable(selector);
	        try {
	            element.click();
	        } catch (Exception e) {
	            throw new TestException(String.format("The following element is not clickable: [%s]", selector));
	        }
	    }

	    public void scrollToThenClick(By selector) {
	        WebElement element = driver.findElement(selector);
	        actions = new Actions(driver);
	        try {
	            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	            actions.moveToElement(element).perform();
	            actions.click(element).perform();
	        } catch (Exception e) {
	            throw new TestException(String.format("The following element is not clickable: [%s]", element.toString()));
	        }
	    }
	    
	    //Get data from DATABASE
	    public ArrayList<String> getDBdetails() throws Exception {
			ArrayList<String> users= new ArrayList<String>();
			//Right click on mysql workbench -> Select "Copy JDBC connection string...add database name"
			String url="jdbc:mysql://localhost:3306/testsql";
			String uname = "root";
			String pass = "9618696711";
			
			//Select column name and table name
			String query = "select PersonID,LastName from persons";
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection(url,uname,pass);
			Statement st = con.createStatement();
			ResultSet rs= st.executeQuery(query);
			while (rs.next()) {
			String nams = rs.getString("PersonID");
			String lastname = rs.getString("LastName");
			
			users.add(nams);
			users.add(lastname);
			//System.out.print(nams);
			}
			st.close();
			con.close();
			
			return users;
			
			//DB CALLING METHOD IN ANOTHER CLASS
			/*	 DBUtil base = new DBUtil();
					ArrayList<String> users=base.getDBdetails();
					for (String temp : users) {
						System.out.println(temp);
					}

				 System.out.println("DB " +users);
				 System.out.println("DB " +users.get(1));
				*/
		
		}
	
		
		
		
	
	    
	    
	    
	    
	    //Enter Captcha
	    public void captchaEnter(){
	    Scanner scan = new Scanner(System.in);	//System.in is an Input stream
	    System.out.println("Enter Captcha");
	    String captcha = scan.nextLine();
	    
	    }
	    
	    //Take screenShot
	    public void takeScreenShot(String filename){
	    	//Get timestamp
	        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	        //System.out.println(timestamp);
	        
	    	//Take screenshot and store as a file format....Add FileUtils jar in POM
	    	File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    	try {
				FileUtils.copyFile(src, new File("D:\\Sravan\\Automation\\Selenium\\Infi\\InfiniGo\\test-output\\Screenshots\\"+filename+"\\"+java.time.LocalDate.now()+"\\"+timestamp.getTime()+".png"));
			} catch (IOException e) {
				
				System.out.println(e+" Error is coming in TakingScreenshot");
			}
	    
	    	//Use this is in any class, then it will took screenshot.
	    	//takeScreenShot(this.getClass().getSimpleName());
	    	
	    }
	    
	    //Read from excel sheet...
	    public static String username;
	    public void getSheet() throws IOException {
	    	
	    	  //Path of excel sheet in local system
			  FileInputStream file = new FileInputStream(new File(ApplicationConstants.USER_NAMES)); 
			  XSSFWorkbook workbook = new XSSFWorkbook(file);
			  XSSFSheet sheet = workbook.getSheetAt(0);
			  
			  //Getting random email Id's
			  //int random_column = CommonUtil.randomNumber(0, 1);
			  int random_row    = CommonUtil.randomNumber(1, 6);
			  
			  //Getting data from row and column 
			  username = sheet.getRow(random_row).getCell((short) 0).getStringCellValue();
			  System.out.println(username);
			  
			  //Getting rows count
			  int noOfRows = sheet.getLastRowNum();
			  //System.out.println("Rows count is: "+noOfRows);
			  
		      //Getting column count
			  int noOfColumns = sheet.getRow(0).getLastCellNum();
			  //System.out.println("Column count is: "+noOfColumns);
	    }
	    
	     //Create excel Sheet...
		 public void createSheet() throws IOException{
			 XSSFWorkbook workbook = new XSSFWorkbook(); 
			 XSSFSheet spreadsheet = workbook.createSheet("WaveLabs");
			 
			  //Create row number
			  Row row = spreadsheet.createRow(1);
			  
			  //Create column number
			  Cell cellname= row.createCell(0);
			  cellname.setCellValue(SitePageObject.emailId);
			  System.out.println("email is: "+SitePageObject.emailId);
			 
			  
			  //Saved file location
			  FileOutputStream out = new FileOutputStream(new File("D:\\Sravan\\Automation\\Selenium\\Infi\\InfiniGo\\src\\test\\resources\\data\\Test.xlsx"));
			  workbook.write(out);
			 }
	    
		 //Image Upload using AutoIt
		 public void imageAutoIt(){  
			int random = CommonUtil.randomNumber(1, 6);
			 
	    	try {
	    	//Runtime.getRuntime().exec(ApplicationConstants.EXE_PATH+"Test"+random+".exe");
	    	Runtime.getRuntime().exec(ApplicationConstants.EXE_PATH+"Test"+random+"");
			System.out.println("Uploaded image successfully");
		
	       } catch (IOException e) {
	    	 System.out.println("Image Upload Failed");
			 e.printStackTrace();
		}
	}
		 
		//Image Upload using Robot
	    public void imageRobot(){  
	    	try {
				Robot robot = new Robot();
				sleep(2000);
				robot.keyPress(KeyEvent.VK_D);
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_SEMICOLON);
				robot.keyRelease(KeyEvent.VK_SEMICOLON);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_BACK_SLASH);
				robot.keyPress(KeyEvent.VK_S);
				robot.keyPress(KeyEvent.VK_R);
				robot.keyPress(KeyEvent.VK_A);
				robot.keyPress(KeyEvent.VK_V);
				robot.keyPress(KeyEvent.VK_A);
				robot.keyPress(KeyEvent.VK_N);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_BACK_SLASH);
				robot.keyPress(KeyEvent.VK_A);
				robot.keyPress(KeyEvent.VK_U);
				robot.keyPress(KeyEvent.VK_T);
				robot.keyPress(KeyEvent.VK_O);
				robot.keyPress(KeyEvent.VK_M);
				robot.keyPress(KeyEvent.VK_A);
				robot.keyPress(KeyEvent.VK_T);
				robot.keyPress(KeyEvent.VK_I);
				robot.keyPress(KeyEvent.VK_O);
				robot.keyPress(KeyEvent.VK_N);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_BACK_SLASH);
				robot.keyPress(KeyEvent.VK_S);
				robot.keyPress(KeyEvent.VK_E);
				robot.keyPress(KeyEvent.VK_L);
				robot.keyPress(KeyEvent.VK_E);
				robot.keyPress(KeyEvent.VK_N);
				robot.keyPress(KeyEvent.VK_I);
				robot.keyPress(KeyEvent.VK_U);
				robot.keyPress(KeyEvent.VK_M);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_BACK_SLASH);
				robot.keyPress(KeyEvent.VK_I);
				robot.keyPress(KeyEvent.VK_N);
				robot.keyPress(KeyEvent.VK_F);
				robot.keyPress(KeyEvent.VK_I);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_BACK_SLASH);
				robot.keyPress(KeyEvent.VK_I);
				robot.keyPress(KeyEvent.VK_N);
				robot.keyPress(KeyEvent.VK_F);
				robot.keyPress(KeyEvent.VK_I);
				robot.keyPress(KeyEvent.VK_N);
				robot.keyPress(KeyEvent.VK_I);
				robot.keyPress(KeyEvent.VK_G);
				robot.keyPress(KeyEvent.VK_O);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_BACK_SLASH);
				robot.keyPress(KeyEvent.VK_S);
				robot.keyPress(KeyEvent.VK_R);
				robot.keyPress(KeyEvent.VK_C);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_BACK_SLASH);
				robot.keyPress(KeyEvent.VK_T);
				robot.keyPress(KeyEvent.VK_E);
				robot.keyPress(KeyEvent.VK_S);
				robot.keyPress(KeyEvent.VK_T);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_BACK_SLASH);
				robot.keyPress(KeyEvent.VK_R);
				robot.keyPress(KeyEvent.VK_E);
				robot.keyPress(KeyEvent.VK_S);
				robot.keyPress(KeyEvent.VK_O);
				robot.keyPress(KeyEvent.VK_U);
				robot.keyPress(KeyEvent.VK_R);
				robot.keyPress(KeyEvent.VK_C);
				robot.keyPress(KeyEvent.VK_E);
				robot.keyPress(KeyEvent.VK_S);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_BACK_SLASH);
				robot.keyPress(KeyEvent.VK_I);
				robot.keyPress(KeyEvent.VK_M);
				robot.keyPress(KeyEvent.VK_A);
				robot.keyPress(KeyEvent.VK_G);
				robot.keyPress(KeyEvent.VK_E);
				robot.keyPress(KeyEvent.VK_S);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_BACK_SLASH);
				robot.keyPress(KeyEvent.VK_P);
				robot.keyPress(KeyEvent.VK_R);
				robot.keyPress(KeyEvent.VK_O);
				robot.keyPress(KeyEvent.VK_V);
				robot.keyPress(KeyEvent.VK_I);
				robot.keyPress(KeyEvent.VK_D);
				robot.keyPress(KeyEvent.VK_E);
				robot.keyPress(KeyEvent.VK_R);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_BACK_SLASH);
				robot.keyPress(KeyEvent.VK_T);
				robot.keyPress(KeyEvent.VK_E);
				robot.keyPress(KeyEvent.VK_S);
				robot.keyPress(KeyEvent.VK_T);
				robot.keyPress(KeyEvent.VK_1);
				robot.keyPress(KeyEvent.VK_ENTER);
				
			} catch (AWTException e) {
				System.out.println("Upload Failed");
				e.printStackTrace();
			}
	}
	    
	    
	
	    public void sendKeys(By selector, String value) {
	        WebElement element = getElement(selector);
	        clearField(element);
	        try {
	            element.sendKeys(value);
	        } catch (Exception e) {
	            throw new TestException(String.format("Error in sending [%s] to the following element: [%s]", value, selector.toString()));
	        }
	    }

	    public void clearField(WebElement element) {
	        try {
	            element.clear();
	            waitForElementTextToBeEmpty(element);
	        } catch (Exception e) {
	            System.out.print(String.format("The following element could not be cleared: [%s]", element.getText()));
	        }
	    }

	    public void waitForElementToDisplay(By Selector) {
	        WebElement element = getElement(Selector);
	        while (!element.isDisplayed()) {
	            System.out.println("Waiting for element to display: " + Selector);
	            sleep(200);
	        }
	    }

	    public void waitForElementTextToBeEmpty(WebElement element) {
	        String text;
	        try {
	            text = element.getText();
	            int maxRetries = 10;
	            int retry = 0;
	            while ((text.length() >= 1) || (retry < maxRetries)) {
	                retry++;
	                text = element.getText();
	            }
	        } catch (Exception e) {
	            System.out.print(String.format("The following element could not be cleared: [%s]", element.getText()));
	        }

	    }

	    public void waitForElementToBeVisible(By selector) {
	        try {
	            wait = new WebDriverWait(driver, timeout);
	            wait.until(ExpectedConditions.presenceOfElementLocated(selector));
	        } catch (Exception e) {
	            throw new NoSuchElementException(String.format("The following element was not visible: %s", selector));
	        }
	    }

	    public void waitUntilElementIsDisplayedOnScreen(By selector) {
	        try {
	            wait = new WebDriverWait(driver, timeout);
	            wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
	        } catch (Exception e) {
	            throw new NoSuchElementException(String.format("The following element was not visible: %s ", selector));
	        }
	    }

	    public void waitForElementToBeClickable(By selector) {
	        try {
	            wait = new WebDriverWait(driver, timeout);
	            wait.until(ExpectedConditions.elementToBeClickable(selector));
	        } catch (Exception e) {
	            throw new TestException("The following element is not clickable: " + selector);
	        }
	    }

	/*    public void sleep(final long millis) {
	        System.out.println((String.format("sleeping %d ms", millis)));
	        try {
	            Thread.sleep(millis);
	        } catch (InterruptedException ex) {
	            ex.printStackTrace();
	        }
	    }*/

	    public void selectIfOptionTextContains(By selector, String searchCriteria) {

	        waitForElementToBeClickable(selector);
	        Select dropdown = new Select(getElement(selector));

	        List<WebElement> options = dropdown.getOptions();

	        String optionText = "";

	        if (options == null) {
	            throw new TestException("Options for the dropdown list cannot be found.");
	        }

	        for (WebElement option : options) {

	            optionText = option.getText().trim();
	            boolean isOptionDisplayed = option.isDisplayed();

	            if (optionText.contains(searchCriteria) && isOptionDisplayed) {
	                try {
	                    dropdown.selectByVisibleText(optionText);
	                    break;
	                } catch (Exception e) {
	                    throw new NoSuchElementException(String.format("The following element did not display: [%s] ", selector.toString()));
	                }
	            }
	        }
	    }

	    public void selectIfOptionTextEquals(By selector, String searchCriteria) {

	        waitForElementToBeClickable(selector);
	        Select dropdown = new Select(getElement(selector));

	        List<WebElement> options = dropdown.getOptions();

	        String optionText = "";

	        if (options == null) {
	            throw new TestException("Options for the dropdown list cannot be found.");
	        }

	        for (WebElement option : options) {

	            optionText = option.getText().trim();
	            boolean isOptionDisplayed = option.isDisplayed();

	            if (optionText.equals(searchCriteria) && isOptionDisplayed) {
	                try {
	                    dropdown.selectByVisibleText(optionText);
	                    break;
	                } catch (Exception e) {
	                    throw new NoSuchElementException(String.format("The following element did not display: [%s] ", selector.toString()));
	                }
	            }
	        }
	    }

	    public List<String> getDropdownValues(By selector) {

	        waitForElementToDisplay(selector);
	        Select dropdown = new Select(getElement(selector));
	        List<String> elementList = new ArrayList<String>();

	        List<WebElement> allValues = dropdown.getOptions();

	        if (allValues == null) {
	            throw new TestException("Some elements in the list do not exist");
	        }

	        for (WebElement value : allValues) {
	            if (value.isDisplayed()) {
	                elementList.add(value.getText().trim());
	            }
	        }
	        return elementList;
	    }
	
	
	
	// Care ////////////////////////////////




	public void setSelectedField(WebElement element, String value) {
		List<WebElement> options = element.findElements(By.tagName("option"));
		System.out.println(options.size());
		for (WebElement option : options) {
			if (value.equals(option.getAttribute("value"))) {
				if (!option.isSelected()) {
					option.click();
					break;
				}
			}
		}
	}

	public int getDropdownsize(WebElement we) {
		List<WebElement> el = we.findElements(By.tagName("option"));
		return el.size();

	}

	/**
	 * Select Weblist
	 *
	 * @param selectElement by value
	 */
	public void selectWebListByValue(WebElement selectElement, String byValue) {
		Select select = new Select(selectElement);
		select.selectByValue(byValue);
	}

	/**
	 * Common method to select from drop down
	 *
	 * @param element
	 * @param indexNum
	 */
	public void selectFromDropDown(WebElement element, int indexNum) {
		// clicks dropdown
		element.click();
		// finds the element to be selected in list
		WebElement ele = driver.findElement(By.xpath("//div[contains(@class,'Open')]//li[" + indexNum + "]"));
		// clicks on that element
		ele.click();
	}

	public void clickUsingJS(WebElement w) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", w);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	/**
	 * Verify Element
	 *
	 * @param by
	 * @return Return Boolean value
	 */
	public boolean verifyElement(By by) {

		// If element is located than return true otherwise return false
		try {
			driver.findElement(by);

			return true;
		} catch (NoSuchElementException e) {

			e.printStackTrace();

			return false;
		}

	}

	/**
	 * Verify message shown in website.....getText
	 *
	 * @param w
	 * @param message
	 */
	public void checkMessage(WebElement w, String message) {
		Assert.assertTrue(w.getText().trim().equalsIgnoreCase(message), "Given web element inner test is not equals to " + message);
	}
	
	

	/**
	 * Get List of visible elements
	 *
	 * @param by
	 * @return Returns Visible element list
	 */
	public List<WebElement> listOfAllVisibleElements(By by) {

		// Store all elements
		List<WebElement> elementList = driver.findElements(by);
		List<WebElement> visiableElements = new ArrayList<WebElement>();

		// Iterate through list
		for (int i = 0; i < elementList.size(); i++) {

			// Check visibility
			if (elementList.get(i).isDisplayed()) {
				visiableElements.add(elementList.get(i));
			}
		}

		// Return visible element list
		return visiableElements;
	}

	/**
	 * Random String generator
	 *
	 * @param length
	 * @return
	 */
/*	public String randomStringGenerator(int length) {
		// Random string generator
		String randomNumber = RandomStringUtils.randomAlphabetic(length);
		// Return random number
		return randomNumber;
	}*/

	/**
	 * Get visiable elements inner text
	 *
	 * @param by
	 * @return
	 */
	public List<String> lisfOfAllVisiableElementsInnerText(By by) {

		// Store all elements
		List<WebElement> elementList = driver.findElements(by);
		List<String> visiableElements = new ArrayList<String>();

		// Iterate through list
		for (int i = 0; i < elementList.size(); i++) {

			// Check visibility
			if (elementList.get(i).isDisplayed()) {
				visiableElements.add(elementList.get(i).getText().trim());
			}
		}
		return visiableElements;
	}

	/**
	 * Get All elements inner text
	 *
	 * @param by
	 * @return
	 */
	public List<String> lisfOfAllElementsInnerTextUsingJS(By by) {

		// Store all elements
		List<WebElement> elementList = driver.findElements(by);
		List<String> visiableElements = new ArrayList<String>();

		// Iterate through list
		for (int i = 0; i < elementList.size(); i++) {
			visiableElements.add(getInnerTextUsingJS(elementList.get(i)));
		}

		// Return list
		return visiableElements;
	}

	/**
	 * Halt the program for given time
	 *
	 * @param millis
	 */
	public void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Enter the text in text box
	 *
	 * @param w
	 * @param text
	 */
	public void type(WebElement w, String text) {
		try {
			w.click();
			w.clear();
			sleep(200);
			w.sendKeys(text);
			sleep(1800);
		}
		/*catch (ElementNotFoundException e) {
			e.printStackTrace();
			Assert.fail();
		}*/ catch (NoSuchElementException e) {
			e.printStackTrace();
			Assert.fail();

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();

		}
	}

	/**
	 * Get text using JS
	 *
	 * @param w
	 * @return Return Inner HTML
	 */
	public String getInnerTextUsingJS(WebElement w) {

		// Create JS
		JavascriptExecutor js = (JavascriptExecutor) driver;

		// Get text
		String text = js.executeScript("return arguments[0].innerHTML", w).toString();

		// Return text
		return text;
	}

	/**
	 * Wait for load js in page
	 *
	 * @return WebDriver
	 */
	public void waitForJSJQuery(WebDriver driver) {

		JavascriptExecutor js = (JavascriptExecutor) driver;

		int limit = (30 * 1000) / 100;

		for (int i = 0; i < limit; i++) {
			try {
				// System.out.println(i);
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}

			// To check page ready state.
			if ((Boolean) js.executeScript("return jQuery.active == 0") && js.executeScript("return document.readyState").equals("complete")) {
				break;
			}
		}
	}

	/**
	 * Verify String to sorted or not
	 *
	 * @param list
	 * @param isAscending
	 * @return Return boolean value
	 */
	public boolean isStringSorted(List<String> list, boolean isAscending) {

		// isSorted
		boolean sorted = true;

		// Iterate
		for (int i = 1; i < list.size(); i++) {

			if (isAscending) {
				if (list.get(i - 1).compareTo(list.get(i)) >= 1) {
					sorted = false;
					break;
				}
			} else {
				if (list.get(i - 1).compareTo(list.get(i)) < 0) {
					sorted = false;
					break;
				}
			}
		}

		// Return sorted value
		return sorted;
	}

	/**
	 * Verify isElement display
	 *
	 * @param WebElement
	 */
	public boolean isDisplayElement(WebElement element) {
		boolean status = false;

		try {
			element.isDisplayed();
			status = true;

		} catch (Exception e) {
			System.out.println("Element is not present : " + element);
		}
		return status;

	}

	/**
	 * Verify String to sorted or not
	 *
	 * @param list
	 * @param isAscending
	 * @return Return boolean value
	 */
	public boolean isNumberSorted(List<String> list, boolean isAscending) {

		// isSorted
		boolean sorted = true;

		// Iterate
		for (int i = 1; i < list.size(); i++) {

			Integer temp1 = Integer.parseInt(list.get(i));
			Integer temp2 = Integer.parseInt(list.get(i - 1));

			if (isAscending) {
				if (temp2.compareTo(temp1) >= 1) {
					sorted = false;
					break;
				}
			} else {
				if (temp2.compareTo(temp1) < 0) {
					sorted = false;
					break;
				}
			}
		}

		// Return sorted value
		return sorted;
	}

	public void isDisplayThenClick(WebElement w) {
		try {
			if (w.isDisplayed()) w.click();
		} catch (Exception e) {
			logger.info("Element is not Present");
		}
	}

	public void isDisplayThenType(WebElement w, String value) {
		try {
			if (w.isDisplayed()) w.sendKeys(value);
		} catch (Exception e) {
			logger.info("Element is not Present");
		}
	}

	/*public static boolean isUAT(DriverHelper browser) {
		if (browser.getCurrentURL().contains("uat")) {
			return true;
		} else return false;
	}*/

	public static String getRecentDateinString(int DateDiff,String format){
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, DateDiff);
		dt = c.getTime();
		SimpleDateFormat formattedDate = new SimpleDateFormat(format);
		return (formattedDate.format(dt));
	}



	public static int getRandomIndex(int maxSize){
		return (int) Math.floor(Math.random() * Math.floor(maxSize));
	}

	public static boolean isElementPresent(WebElement element, WebDriver driver) {
		try{
			element.getTagName();
			return true;
		}
		catch(Exception e){
			return false;
		}
	}

	public static String getUniqueEmailId(String prefix){
		String email = prefix + "_" + getRecentDateinString(0,"YYYYMMDDHHMMSS") + "@email.com";
		return  email;
	}

	public static String getRandomString() {
		String random = "Lorem ipsum dolor sit amet, libero vel diam orci bibendum vel, proin ante ultrices adipisci pede lorem, dui odio vitae arcu tellus sodales, blandit error consectetuer, et lectus sem tempus officia pellentesque pede. Repellat sit placerat luctus praesent risus, interdum lorem duis augue at volutpat, hendrerit curabitur dicta nec, et mauris enim, tempor amet. Cursus faucibus, mauris commodo sem sed, donec erat nec ultricies neque montes pretium. Parturient a, a dignissim laoreet sit imperdiet id. Quis justo arcu tristique amet mollis mus. Non mi malesuada ridiculus, dui elementum, nisl eget, neque commodo euismod animi quis penatibus, neque sed";
		return random;
	}

	public static void forceWait(int seconds){
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


}

