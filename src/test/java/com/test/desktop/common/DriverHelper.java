package com.test.desktop.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import com.infinico.selenum.common.ApplicationConstants;

public class DriverHelper {

	public static WebDriver driver;

	/*
	 * @BeforeSuite(alwaysRun = true) public void setUp() {
	 * System.setProperty("webdriver.gecko.driver",
	 * "D:\\Sravan\\Automation\\eclipse\\SW\\geckodriver.exe"); driver = new
	 * FirefoxDriver(); }
	 * 
	 * public static WebDriver getDriver() { if ( driver == null) {
	 * System.setProperty("webdriver.gecko.driver",
	 * "D:\\Sravan\\Automation\\eclipse\\SW\\geckodriver.exe"); driver = new
	 * FirefoxDriver(); } return driver; }
	 */

	@BeforeSuite(alwaysRun = true)
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", ApplicationConstants.CHROME_DRIVER);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		// System.out.println("first");
	}

	public static WebDriver getDriver() {
		if (driver == null) {
			System.setProperty("webdriver.chrome.driver", ApplicationConstants.CHROME_DRIVER);
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			// System.out.println("Chrome");
		}
		return driver;
	}

	/*
	 * public static WebDriver getDriver(String browser) { if (driver == null &&
	 * browser.equalsIgnoreCase("chrome")) {
	 * System.setProperty("webdriver.chrome.driver",
	 * "D:\\Sravan\\Automation\\eclipse\\SW\\chromedriver.exe"); driver = new
	 * ChromeDriver(); driver.manage().window().maximize();
	 * System.out.println("Chrome"); return driver; } else if(driver == null &&
	 * browser.equalsIgnoreCase("firefox")) { driver = new FirefoxDriver();
	 * driver.manage().window().maximize(); System.out.println("Firefox"); return
	 * driver; } return driver; }
	 */

	@AfterSuite(alwaysRun = true)
	public void tearDown() throws Exception {
		driver.close();
		driver.quit();
	}

}
