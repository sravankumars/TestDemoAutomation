package com.test.desktop.common;


public enum Url {
    PRODUCT_SECTION("/gp/product"),
    BASEURL("http://www.facebook.com"),
    SIGNOUT("/gp/flex/sign-out.html");

    String url;

    Url(String url){
        this.url = url;
    }

    public String getURL() {
        return url;
    }
}
