package com.test.desktop.common;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;


public class Report
{
	public static void head()
		{
			Reporter.log("<HTML><BODY><table border='3' width='100%'><tr><th style='text-align:center' width='5%' bgcolor='#C0C0C0'>S.NO</th><th style='text-align:center' width='15%' bgcolor='#C0C0C0'>Test Module</th><th style='text-align:center' width='70%' bgcolor='#C0C0C0'>Test Description</th><th style='text-align:center' width='20%' bgcolor='#C0C0C0'>Test Data</th><th style='text-align:center' width='10%' bgcolor='#C0C0C0'>Status</th><th style='text-align:center' width='10%' bgcolor='#C0C0C0'>Screenshot</th></tr>");

		}

	public static void body(int i, String tm, String td, String tdata, String status,String screenshot)
		{
			if (status == "pass" & screenshot != "")
				{
				Reporter.log("<tr><td style='text-align:center'>" + i + "</td><td style='text-align:center'>" + tm + "</td><td style='text-align:center'>" + td + "</td><td>" + tdata+ "</td><td  bgcolor='#669900' style='text-align:center'></td><td style='text-align:center'><a href="+screenshot+">" +screenshot+ "</a></td></tr>");
				}
			else if  (status == "pass" & screenshot == ""){
				Reporter.log("<tr><td style='text-align:center'>" + i + "</td><td style='text-align:center'>" + tm + "</td><td style='text-align:center'>" + td + "</td><td>" + tdata + "</td><td  bgcolor='#669900' style='text-align:center'></td><td>"+"  -  "+"</td></tr>");
				
			}
			else if (status == "fail" & screenshot != "")
				{
				Reporter.log("<tr><td style='text-align:center'>" + i + "</td><td style='text-align:center'>" + tm + "</td><td style='text-align:center'>" + td + "</td><td>" + tdata+ "</td><td  bgcolor='#FF0000' style='text-align:center'></td><td style='text-align:center'><a href="+screenshot+">" +screenshot+ "</a></td></tr>");
				}
			else
				Reporter.log("<tr><td style='text-align:center'>" + i + "</td><td style='text-align:center'>" + tm + "</td><td style='text-align:center'>" + td + "</td><td>" + tdata + "</td><td  bgcolor='#FF0000' style='text-align:center'></td>td>"+"  -  "+"</td></tr>");

		}
	
	
}
