package com.test.desktop.signup;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.LocalFileDetector;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.infinico.selenum.common.CommonUtil;
import com.infinico.selenum.common.Loggers;
import com.test.desktop.common.BasePO;
import com.test.desktop.common.DriverHelper;
import com.test.desktop.common.Url;
import com.test.desktop.login.UserLogin;
import com.test.desktop.pageobject.SitePageObject;

public class SignUp extends BasePO {

	//@Parameters("browser")
	@BeforeClass
	public void setUp(){
		driver = DriverHelper.getDriver();
	}

	@Test
	public void navigateToHomePage() {
		String url = Url.BASEURL.getURL();
		System.out.println("Navigating to Infinico: " + url);
		navigateToURL(url);
		sleep(1000);
	}

	@Test(priority=1)
	public void SignUp(){

		//Entering Fname
		waitForElementToBeVisible(SitePageObject.fname);
		sendKeys(SitePageObject.fname, SitePageObject.name);
		System.out.println("SIGNUP_PAGE: Entering fname: " + SitePageObject.name);
		sleep(1000);

		//Entering Lname
		waitForElementToBeVisible(SitePageObject.lname);
		sendKeys(SitePageObject.lname, SitePageObject.name);
		System.out.println("SIGNUP_PAGE: Entering lname: " + SitePageObject.name);
		sleep(1000);

		//Entering Email
		waitForElementToBeVisible(SitePageObject.email);
		sendKeys(SitePageObject.email, SitePageObject.emailId);
		System.out.println("SIGNUP_PAGE: Entering Email: " + SitePageObject.emailId);
		sleep(1000);

		//Re-Entering Email
		waitForElementToBeVisible(SitePageObject.email1);
		sendKeys(SitePageObject.email1, SitePageObject.emailId);
		System.out.println("SIGNUP_PAGE: Re-Entering Email: " + SitePageObject.emailId);
		sleep(1000);

		//Entering Password
		waitForElementToBeVisible(SitePageObject.password1);
		sendKeys(SitePageObject.password1, SitePageObject.name);
		System.out.println("SIGNUP_PAGE: Entering Password: " + SitePageObject.name);
		sleep(1000);
	} 

	@Test(priority=2)
	public void Verify(){
		waitForElementToBeVisible(SitePageObject.verify);
		checkMessage(driver.findElement(SitePageObject.verify), "Create an account");
		System.out.println("Succefully verified "+ driver.findElement(SitePageObject.verify).getText());
	}
	
	@Test(priority=3)
	public void getTitle(){
	System.out.println("Title is: " + getPageTitle());
	}
	
	@Test(priority=4)
	public void emailGetting() throws IOException{
		createSheet();
		
		
	System.out.println("email is: " + getPageTitle());
	}
	       
		
		
	}
	
	
/*
	@Test
	public void f() {
		System.out.println("sss");
		  UserLogin orderActions = new UserLogin();
		 //logger.debug("Hello World!");
		 logger.info("Test Started \n" + orderActions.emailId);

		 logger.info("Test Started \n" + orderActions.name);

		 driver.navigate().to("http://google.com");

	}*/

